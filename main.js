import App from './App'
import locale from './locale/index.js'



// #ifndef VUE3
import Vue from 'vue'
import {
	createI18n
} from 'vue-i18n'
import VueI18n from 'vue-i18n' // v8.x
import './uni.promisify.adaptor'
Vue.use(VueI18n)
Vue.config.productionTip = false
const i18n = createI18n({
	locale: uni.getLocale(),
	messages: locale
})
App.mpType = 'app'
const app = new Vue({
	i18n,
	...App
})

app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
import {
	createI18n
} from 'vue-i18n'// v9.x
import * as Pinia from 'pinia';
export function createApp() {
	const i18n = createI18n({
		locale: uni.getLocale(),
		messages: locale
	})
	const app = createSSRApp(App)
	app.use(i18n)
	app.use(Pinia.createPinia());
	return {
		app,
		Pinia
	}
}
// #endif