const typeDicts = {
	// 心率
	'0': {
		text: 'data.HeartRate'
	},
	// 血压,
	'1': {
		text: 'data.BloodPressure'
	},
	// 血氧
	'2': {
		text: 'data.BloodOxygen'
	},
	// 体温
	'3': {
		text: 'data.BodyTemperature'
	},
	// 低电量
	'4': {
		text: 'data.LowBattery'
	},
	// sos求救
	'5': {
		text: 'data.Sos'
	},
}

export function getAlarmTypeName(type){
	return typeDicts[type]?.text
}

export default typeDicts