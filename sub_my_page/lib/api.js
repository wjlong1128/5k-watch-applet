import request from '@/lib/utils/request.js'

export function getAlarmMsgApi(){
	return request('/MainFeatures/Alarm/getAlarmMsg',{},{method: 'GET',loading: true})
}


/**
 * 发送消息 {"msgData":"测试消息","sendUser":"发送人"}
 * @param {Object} data
 */
export function sendMsgApi(data){
	return request('/watch/send/down/msg',data,{method: 'POST',loading: true})
}

