import {
	defineStore
} from 'pinia'
import {
	ref
} from 'vue'

const useBluetoothStore = defineStore('bluetoothStore', () => {

	// 是否存在蓝牙权限
	const isAuth = ref(true)
	// 手机是否开启了蓝牙
	const isEnableBluetooth = ref(false)
	// 提示信息 国际化key
	const errTips = ref('bt.authTips')
	// 设备
	const deviceList = ref([]);
	// 是否在扫描
	const isDiscovering = ref(false)

	// 是否注册过某些方法
	const isRegisterMethod = ref(false)
	
	// 蓝牙模块是否在活动
	const isActive = ref(false);
	
	const restartLoading = ref(false);
	
	const isConnection = ref(false)
	const currentDevice = ref(null)
	/**
	 * 找到设备的处理
	 */
	function registerDeviceFound(){
		uni.onBluetoothDeviceFound((rd)=>{
			rd.devices.forEach(d=>{
//&& d.name.startsWith("LARK_")				
				if(d.name){
					const filterArr = deviceList.value.filter(e => e.deviceId == d.deviceId)
					if (!filterArr || filterArr.length <= 0) {
						deviceList.value.push(d)
					}
				}
			})
		})
	}
	
	const closeBluetooth = ()=>{
		// 关闭扫描
		uni.stopBluetoothDevicesDiscovery({
			success() {
				deviceList.value = []
			}
		})
		// 移除搜索到新设备的事件的全部监听函数
		uni.offBluetoothDeviceFound()
		uni.offBluetoothAdapterStateChange()
		uni.closeBluetoothAdapter()
		// #ifdef MP-WEIXIN
		uni.offBLECharacteristicValueChange();
		uni.offBLEConnectionStateChange()
		uni.offBLEMTUChange()
		// #endif
		isActive.value = false;
		deviceList.value = []
		isRegisterMethod.value = false;
		if(isAuth.value){
			errTips.value = 'bt.enableBt'
		}else{
			errTips.value = 'bt.authTips'
		}
		isAuth.value = true;
		isEnableBluetooth.value = false;
		isDiscovering.value = false;
		isConnection.value = false;
		currentDevice.value = null;

	}
	
	function listenerAdapterStateChange(){
		// available 蓝牙是否开启 discovering 是否在寻找设备
		uni.onBluetoothAdapterStateChange(({available,discovering})=>{
			
			// if(available && !isEnableBluetooth.value && !isActive.value){
			// 	 loadluetooth();
			// 	 return;
			// }
			
			// 只在蓝牙模块启动成功记录状态
			if(isActive.value){
				isEnableBluetooth.value = available;
				// isDiscovering.value = discovering;
			}
			
			
			// 代表关闭了蓝牙，要关闭蓝牙模块
			if(isActive.value && !available){
				errTips.value = 'bt.enableBt';
				closeBluetooth()
			}
			
		})
	}
	
	
	
	/**
	 * 蓝牙模块加载失败
	 */
	function loadFail(isShow, res,rejectFun) {
		console.log('error',res);
		isShow && uni.hideLoading()
		if (res.errno === 103 || res.errMsg == 'openBluetoothAdapter:fail auth deny' || res.errMsg ==
			'openBluetoothAdapter:fail:auth denied') {
			errTips.value = 'bt.authTips'
			isAuth.value = false;
			rejectFun()
			return;
		}
		// 授权成功，那就是蓝牙的错
		isAuth.value = true;
		switch (res.errCode) {
			case 10001: {
				isEnableBluetooth.value = false;
				errTips.value = 'bt.enableBt'
				break
			}
			default: {
				break;
			}
		}
		
		rejectFun(res)
	}

	/**
	 * 蓝牙模块加载成功
	 */
	function loadSuccess(isShow,resolveFun) {
		isActive.value = true;
		isShow && uni.hideLoading();
		isAuth.value = true;
		isEnableBluetooth.value = true;
		resolveFun()
	}


	/**
	 * 加载蓝牙模块
	 */
	const loadBluetooth = (isShow = false) => {
		return new Promise((resolve,reject)=>{
			if(isActive.value){
				resolve()
				return;
			}
			isShow && uni.showLoading();
			
			uni.openBluetoothAdapter({
				mode: 'central',
				success() {
					if(!isRegisterMethod.value){
						registerDeviceFound();
						listenerAdapterStateChange()
						isRegisterMethod.value = true;
					}
					loadSuccess(isShow,resolve);
				},
				fail(res) {
					loadFail(isShow, res,reject)
				}
			})
		})
		
		
		
	}
	
	const authBluetooth = ()=>{
		uni.openSetting({
			success(authRes) {
				if (authRes.authSetting['scope.bluetooth']) {
					isAuth.value = true;
					loadBluetooth().then(()=>{
						startScan();
					})
				}
			}
		})
	}
	
	const startScan = ()=> {
		return new Promise((resolve,reject)=>{
			uni.startBluetoothDevicesDiscovery({
				allowDuplicatesKey: false,
				interval: 0,
				success(res) {
					// uni.showLoading({
					// 	title: "搜索中"
					// })
					// isSearch.value = true;
					// isScan.value = true;
					console.log('开启扫描',res);
					// cb && cb()
					isDiscovering.value = true;
					resolve(res);

				},
				fail(e) {
					console.log('开启扫描失败！', e);
					isDiscovering.value = false;
					reject(e)
				},
				
			})
		})
	}
	
	
	const stopScan = ()=>{
		return new Promise((resolve,reject)=>{
			if(!isDiscovering.value){
				resolve()
				return;
			}
			isDiscovering.value = false;
			uni.stopBluetoothDevicesDiscovery({
				fail(e){
					reject(e)
				},
				success(res) {
					resolve(res)
				}
			})
		})
	}
	
	
	// 过时！
	const restartScan = ()=>{
		restartLoading.value = true;
		stopScan(()=>{
			deviceList.value = []
			uni.getBluetoothDevices({
				success(res) {
					res?.devices?.forEach(e=>{
						if(e.name && e.name != '未知设备'){
							deviceList.value.push(e)
						}
						
					})
				},
				complete(){
					startScan(()=>{
						restartLoading.value = false;
					})
				}
			})
		})
	}
	
	const getDeviceServices = (deviceId)=>{
		return new Promise((resolve,reject)=>{
			uni.getBLEDeviceServices({
				deviceId,
				success(res) {
					console.log('getBLEDeviceServices ok',res);
					resolve(res)
				},
				fail(e) {
					console.log('getBLEDeviceServices error',e)
					reject(e)
				}
			})
		})
	}
	
	const getDeviceCharacteristics = (deviceId,serviceId)=>{
		return new Promise((resolve,reject)=>{
			setTimeout(()=>{
				uni.getBLEDeviceCharacteristics({
					deviceId,
					serviceId,
					success(res) {
						console.log('getDeviceCharacteristics, ok',deviceId,serviceId,res );
						resolve(res)
					},
					fail(e) {
						if(e.errCode == 10005){ // 10005没有特征值
							resolve({...e,characteristics:[]})
							return;
						}
						console.log('getDeviceCharacteristics, error',deviceId,serviceId,e );
						reject(e)
					}
				})
			},500)
		})
	}
	
	const registerNotifyCharacteristicValueChange = (deviceId,serviceId,characteristic)=>{
		return new Promise((resolve,reject)=>{
			if(characteristic.properties.notify || characteristic.properties.indicate){
				uni.notifyBLECharacteristicValueChange({
					deviceId,
					serviceId,
					characteristicId:characteristic.uuid,
					state:true,
					success(r) {
						console.log('registerNotifyCharacteristicValueChange ok',r,deviceId,serviceId,characteristic);
						resolve({result: r,characteristic})
					},
					fail(e) {
						console.log('registerNotifyCharacteristicValueChange error',e,deviceId,serviceId,characteristic);
						reject(e)
					}
				})
			}else{
				resolve({})
			}
		
		})
	}
	
	/**
	 * 获取已经连接设备的所有服务及其对应的特征值
	 */
	const getDeviceServicesAndCharacteristics = async (deviceId)=>{

		try{
			const {services} = await getDeviceServices(deviceId)
			for(let i = 0; i < services.length; i++){
				const s = services[i]
				// console.log('ssss:',s,i);
				
					const {characteristics} = await getDeviceCharacteristics(deviceId,s.uuid);
					for(let j = 0; j < characteristics.length ; j++){
					  await registerNotifyCharacteristicValueChange(deviceId,s.uuid,characteristics[j])
					}
					s.characteristics = characteristics;
			
			}
			uni.onBLECharacteristicValueChange(({deviceId: did,serviceId,characteristicId,value})=>{
				console.log('onBLECharacteristicValueChange',did,serviceId,characteristicId,value);
			})
			return services;
		}catch(e){
			throw e;
		}
	    
	}
	
	
	//  const getDeviceServicesAndCharacteristics = (deviceId) => {
	//   return new Promise((resolve, reject) => {
	//     getDeviceServices(deviceId)
	//       .then(({ services }) => {
	//         let promiseChain = Promise.resolve(); // 初始化一个已解决的Promise
	
	//         services.forEach((service) => {
	//           promiseChain = promiseChain // 累积Promise，确保按顺序执行
	//             .then(() => getDeviceCharacteristics(deviceId, service.uuid))
	//             .then(({ characteristics }) => {
	//               // 并行注册所有特征的通知
	//               const registerPromises = characteristics.map((characteristic) => 
	//                 registerNotifyCharacteristicValueChange(deviceId, service.uuid, characteristic)
	//               );
	//               return Promise.all(registerPromises);
	//             })
	//             .then((arr) => {
	// 			  let characteristics = arr.map(arrItem=>arrItem.characteristic)
	//               service.characteristics = characteristics; // 特征注册完毕，附加到服务对象
	//             })
	//             .catch(reject); // 任何步骤出错，直接传递给外层Promise的reject
	//         });
	
	//         // 当所有服务和特性处理完成后，解析最终结果
	//         promiseChain.then(() => resolve(services), reject);
	//       })
	//       .catch(reject); // getDeviceServices出错时，直接拒绝外层Promise
	//   });
	// }
	
	
	const connectSuccess = (connectionResult,device)=>{
		return new Promise((resolve,reject)=>{
			getDeviceServicesAndCharacteristics(device.deviceId).then(services=>{
				isConnection.value = true;
				currentDevice.value = device;
				// 监听连接状态改变
				uni.onBLEConnectionStateChange(({deviceId,connected})=>{
					console.log('onBLEConnectionStateChange',deviceId,connected);
					if(deviceId == currentDevice.value?.deviceId){
						isConnection.value = connected
						// 如果连接为false, 但是还是处于蓝牙活动状态，关闭蓝牙适配器
						if(isActive.value && !connected){
							closeBluetooth()
						}
					}
				})
				// console.log(JSON.stringify(services));
				resolve({result: connectionResult,services})
			}).catch((e)=>{
				uni.closeBLEConnection({
					deviceId: device.deviceId,
					complete() {
						isConnection.value = false;
						currentDevice.value = null;
						reject(e)
					}
				})
			})
		})
	}
	/**
	 * 连接设备
	 */
	const connect = (device)=>{
		return new Promise((resolve,reject)=>{
			uni.createBLEConnection({
				deviceId:device.deviceId,
				success(res) {
					// isConnection.value = true;
					// currentDevice.value = device;
					// // 监听连接状态改变
					// uni.onBLEConnectionStateChange(({deviceId,connected})=>{
					// 	console.log('onBLEConnectionStateChange',deviceId,connected);
					// 	if(deviceId == currentDevice.value?.deviceId){
					// 		isConnection.value = connected
					// 		// 如果连接为false, 但是还是处于蓝牙活动状态，关闭蓝牙适配器
					// 		if(isActive.value && !connected){
					// 			closeBluetooth()
					// 		}
					// 	}
					// })
					
					// getDeviceServicesAndCharacteristics(device.deviceId).then(services=>{
						
					// 	stopScan().finally(()=>{
							
					// 		resolve(res)
							
					// 	})
						
					// }).catch(reject)
					
					connectSuccess(res,device).then(resolve).catch(reject)
					
				},
				fail(res) {
					console.log('连接失败',device,res);
					if(res.errCode == -1){
						// 重复连接
						// isConnection.value = true;
						// currentDevice.value = device;
						// resolve(res)
						connectSuccess(res,device).then(resolve).catch(reject)
					}else{
						isConnection.value = false;
						currentDevice.value = null;
						reject(res)
					}
					
				}
			})
		})
	}
	
	// 关闭连接
	const closeConnection = ()=>{
		return new Promise((resolve,reject)=>{
			uni.closeBLEConnection({
				deviceId: currentDevice.value?.deviceId,
				success(r) {
					console.log('关闭连接成功',r);
				},
				fail(e) {
					console.log('关闭连接失败',e);
				},
				complete() {
					closeBluetooth()
					resolve()
				},
			})
		})
	}

	return {
		isAuth,
		isEnableBluetooth,
		errTips,
		isActive,
		isDiscovering,
		deviceList,
		restartLoading,
		isConnection,
		currentDevice,
		loadBluetooth,
		authBluetooth,
		startScan,
		stopScan,
		restartScan,
		closeBluetooth,
		connect,
		closeConnection,
		getDeviceServicesAndCharacteristics
	}
})

export default useBluetoothStore