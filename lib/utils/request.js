import {
	useUserStore
} from '../../store/user.js'
import {
	logOutApi
} from '../api/user.js';
import {
	storeKeys
} from '../constants.js';

const defauls = {
	method: "POST",
	loading: true
}

export function logoutAfter(){
	uni.removeStorageSync(storeKeys.langKey)
	uni.removeStorageSync(storeKeys.wechatInfo)
	uni.removeStorageSync(storeKeys.token)
	uni.removeStorageSync(storeKeys.openId)
	uni.removeStorageSync(storeKeys.imei)
}

// export const BASE_URL = 'https://dev.sx5k.com'
// export const BASE_URL = 'http://192.168.1.21:6519'
export const BASE_URL = 'https://prod.sx5k.com'
// 全局请求封装
export default (path, data = {}, config = defauls, reqconfig = {}) => {
	const token = uni.getStorageSync(storeKeys.token);
	const Authorization = token ? `Bearer ${token}` : "";

	if (config.loading) {
		uni.showLoading({
			title: uni.getLocale().startsWith('zh') ? '加载中' : "loading",
			mask: true
		});
	};

	return new Promise((resolve, reject) => {
		uni.request({
			...reqconfig,
			header: {
				Authorization
			},
			url: BASE_URL + path,
			method: config.method,
			data,
			async success(response) {
				if (response.data.code === 3000 || response.data.code === 403 || response.data.code === 1009) { //Token失效
					// logout()
					// const userStore = useUserStore()
					// await userStore.logout()
					// ================================
					await logOutApi()
					logoutAfter()
					uni.reLaunch({
						url: `/pages/my/my?msg=${uni.getLocale().startsWith('zh') ? '登录状态已过期或失效，请重新登录' :
							"Login status has expired or expired, please log in again"}`,
					})

					// ================================
					// uni.showToast({
					// 	icon: "none",
					// 	duration: 4000,
					// 	title: uni.getLocale().startsWith('zh') ? '登录状态已过期或失效，请重新登录' :
					// 		"Login status has expired or expired, please log in again"
					// });



					reject(response.data.msg)
				}

				// 心率为空
				else if (response.data.code === 1037) {
					resolve(response.data);
				}
				// 手表绑定失败
				else if (response.data.code === 1038) {
					setTimeout(() => {
						uni.showToast({
							icon: "none",
							duration: 4000,
							title: uni.getLocale().startsWith('zh') ?
								'绑定手表失败，请扫描正确的二维码' :
								"Binding watch failed, please scan the correct QR code"
						});
					}, 500)
					reject(response.data);
				}
				// 登录
				else if (response.data.code === 201) {
					// uni.setStorageSync('sessionid', response.header['Set-Cookie'].split(',')[0]);
					// console.log(response.header['Set-Cookie'].split(',')[0],'登录id');
					resolve(response.data);
				} else if (response.data.code == 500) {
					setTimeout(() => {
						uni.showToast({
							icon: "none",
							duration: 5000,
							title: response.data.msg === '绑定失败,二维码有误' &&
								uni.getLocale().startsWith('zh') ? response.data
								.msg :
								"Binding watch failed, please scan the correct QR code"
						});
					}, 500)
					reject(response.data.msg)
					// console.log('请求不是2000',response);
				} else if (response.data.code !== 200) {
					uni.showToast({
						icon: "none",
						duration: 4000,
						title: response.data.msg
					});
					reject(response.data.msg)
					// console.log('请求不是2000',response);
				}
				resolve(response.data);
			},
			fail(err) {
				uni.showToast({
					icon: "none",
					title: '服务响应失败'
				});
				console.error('服务器响应失败', err);
				reject(err);
			},
			complete() {
				if(config.loading){
					uni.hideLoading();
				}
			}
		});
	});
};