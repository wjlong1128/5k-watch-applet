export function getNumberArray(start, end) {
  const result = [];
  
  for (let i = start; i <= end; i++) {
    result.push(i);
  }
  
  return result;
}


export function getStartDate(year,month = 1, day = 1) {
  // 创建一个新的 Date 对象，月份是从 0 开始计数的，所以这里月份为 0 表示一月
  month = month - 1
  return new Date(year, month, day);
}

export function formatTimestampToStr(timestamp) {
  let date = new Date(timestamp);
  let year = date.getFullYear();
  let month = String(date.getMonth() + 1).padStart(2, '0');
  let day = String(date.getDate()).padStart(2, '0');
  let hours = String(date.getHours()).padStart(2, '0');
  let minutes = String(date.getMinutes()).padStart(2, '0');
  let seconds = String(date.getSeconds()).padStart(2, '0');

  return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}


export function isNumber(input) {
  // 判断是否为数字
  if (!isNaN(input)) {
    // 判断是否为浮点数
    if (parseFloat(input) === Number(input)) {
      return true; // 是浮点数
    } else {
      return false; // 是整数
    }
  } else {
    return false; // 不是数字
  }
}