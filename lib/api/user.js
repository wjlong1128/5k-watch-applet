import request from '../utils/request.js'

/**
 * 登录请求获取token
 * @param {object} data:{code:获取登录的code,phoneCode:电话代码} 
 */ 
export function loginApi(data) {
	return request('/watch/login',data)
}


/**
 * 获取个人信息
 */ 
export function getUserInfoApi(data) {
	return request('/personalCenter/getPersonalMsg',{},{method: "GET",loading: true})
}


/**
 * 退出登录
 */ 
export function logOutApi() {
	return request('/watch/logout',{},{method: "POST",loading: false})
}


/**
 * 修改个人信息
 * @param {object} data:{sex:性别0男1女，height:身高(cm)，weight:体重(kg),birthday:日期} 修改信息
 * 
 */ 
export function updateUserInfoApi(data) {
	return request(`/personalCenter/modifyPersonalMsg`,data,{method: "POST",loading: true})
}



/**
 * 获取当前手表标识码
 */ 
export function getDeviceImeiApi() {
	return request('/personalCenter/getWatchImei',{},{method: "GET",loading: true})
}
/**
 * 绑定手表
 * @param {object} imei 手表标识码
 */ 
export function bindDeviceApi(imei) {
	return request(`/personalCenter/bindWatch`,{imei},{method: "POST",loading: true})
}

/**
 * 解除绑定
*/ 
export function unBindApi() {
	return request(`/personalCenter/unWatchImei`)
}

/**
 * 获取设备信息
 * @param {String} imei 设备码
*/ 
export function getDeviceInfoApi(imei) {
	return request(`/MainFeatures/device/info/${imei}`,{},{method: "GET",loading: true})
}

/**
 * 获取设备电量和和网络信号
 * @param {String} imei 设备码
*/ 
export function getDevicePowerApi(imei) {
	return request(`/MainFeatures/device/power/${imei}`,{},{method: "GET",loading: false})
}



// 获取手机号
export function getMyPhone(code) {
	return request(`/personalCenter/setPhoneNumber`,{code})
}

/**
 * 获取设备位置
 */
export function getDeviceLoactionApi(){
	return request('/MainFeatures/device/location',{},{method: 'GET',loading: true})
}
