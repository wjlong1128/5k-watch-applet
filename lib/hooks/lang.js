import {
	getCurrentInstance
} from "vue"
import { storeKeys } from "../constants"
export default function useChangeLang(callback) {
	const {
		proxy
	} = getCurrentInstance()
	const changeLang = (lang) => {
		// const locale = uni.getLocale()
		// const lang = locale == 'zh-Hans' ? 'en' : 'zh-Hans'
		uni.setLocale(lang)
		proxy.$i18n.locale = lang
		uni.setStorageSync(storeKeys.langKey, lang)
		callback && callback()
	}
	
	return {
		changeLang
	}

}