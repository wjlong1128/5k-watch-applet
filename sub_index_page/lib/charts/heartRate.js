import {
	getHeartRateChartApi,
} from "../api"
import {
	extractTimeFromTimestamp
} from "../utils"


// const heartRate = {
// 	// type: 'line',
// 	type: 'area',
// 	api: getHeartRateChartApi,
// 	opts: {
// 		 color: ["#1890FF","#91CB74","#FAC858","#EE6666","#73C0DE","#3CA272","#FC8452","#9A60B4","#ea7ccc"],
// 		// padding: [15, 15, 0, 15],
// 		enableScroll: true,
// 		// dataLabel: false,
// 		dataPointShapeType: 'solid',
// 		legend: {
// 			show: false
// 		},
// 		xAxis: {
// 			disableGrid: true,
// 			labelCount: 3,
// 			itemCount: 6,
// 			scrollShow: true

// 		},
// 		yAxis: {
// 			gridType: "solid",
// 			disabled: false,
// 			disableGrid: true,

// 			data: [{
// 				min: 40,
// 				calibration: true,
// 			}]
// 		},
// 		extra: {
// 			area: {
// 				type: "curve",
// 				opacity: 0.2,
// 				// addLine: true,
// 				// width: 2,
// 				// gradient: true,
// 				// activeType: "hollow"
// 			}
// 			// tooltip: {
// 			// 	showBox: false
// 			// }
// 		},
// 	},
// 	async req(t, start, end, dateType, imei) {
// 		const {
// 			data
// 		} = await this.api(start, end, dateType, imei)
// 		return this.convert(t, dateType, data)
// 	},
// 	convert(t, dateType, data) {
// 		const keys = []
// 		const values = []
// 		data?.forEach(item => {
// 			if (dateType == 'day') {
// 				keys.push(extractTimeFromTimestamp(item['v']))
// 			} else {
// 				keys.push((item['v']))
// 			}
// 			values.push(item['k'])
// 		})
// 		const isEmpty = keys.length == 0 || values.length == 0
// 		return {
// 			isEmpty,
// 			chartData: this.charts(t, keys, values),
// 			data
// 		}
// 	},
// 	charts(t, keys, values) {
// 		return {
// 			//模拟服务器返回数据，如果数据格式和标准格式不同，需自行按下面的格式拼接
// 			categories: keys || [],
// 			series: [{
// 				name: t("data.HeartRate") + 'bpm',
// 				data: values || []
// 			}]
// 		}
// 	}
// }



const heartRate = {
	// type: 'line',
	type: 'line',
	api: getHeartRateChartApi,
	opts: {
		color: ["#1890FF", "#91CB74", "#FAC858", "#EE6666", "#73C0DE", "#3CA272", "#FC8452", "#9A60B4", "#ea7ccc"],
		padding: [15, 15, 0, 15],
		dataLabel: false,
		dataPointShape: true,
		dataPointShapeType: 'solid',
		enableScroll: false,
		legend: {},
		xAxis: {
			disableGrid: true,
			labelCount: 4,
			itemCount: 0, // x轴显示数据个数
		},
		yAxis: {
		 // disabled:true,
			// gridType: "solid",
			gridType: "dash",
			dashLength: 2,
			data: [{
				min: 30,
				// max: 150
				axisLineColor: '#fff',
			}]
		},
		extra: {
			line: {
				type: "curve",
				width: 2,
				activeType: "hollow",
				linearType: "custom",
				onShadow: true,
				animation: "vertical"
			}
		}
	},
	async req(t, start, end, dateType, imei) {
		const {
			data
		} = await this.api(start, end, dateType, imei)
		return this.convert(t, dateType, data)
	},
	convert(t, dateType, data) {
		const keys = []
		const values = []
		data?.list?.forEach(item => {
			if (dateType == 'day') {
				keys.push(extractTimeFromTimestamp(item['v']))
			} else {
				keys.push((item['v']))
			}
			values.push(item['k'])
		})
		const isEmpty = keys.length == 0 || values.length == 0
		return {
			isEmpty,
			chartData: this.charts(t, keys, values),
			data
		}
	},
	charts(t, keys, values) {
		this.opts.xAxis.itemCount = keys?.length || 0
		return {
			//模拟服务器返回数据，如果数据格式和标准格式不同，需自行按下面的格式拼接
			categories: keys || [],
			series: [{
				name: t("data.HeartRate"),
				linearColor: [
					[
						0,
						"#1890FF"
					],
					[
						0.25,
						"#00B5FF"
					],
					[
						0.5,
						"#00D1ED"
					],
					[
						0.75,
						"#00E6BB"
					],
					[
						1,
						"#90F489"
					]
				],
				setShadow: [
					3,
					8,
					10,
					"#1890FF"
				],
				data: values || []
			}]
		}
	}
}



export default heartRate