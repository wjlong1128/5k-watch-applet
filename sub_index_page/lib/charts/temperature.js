import {
	getTemperatureChartApi,
} from "../api"
import {
	extractTimeFromTimestamp
} from "../utils"

const temperature = {
	// type: 'line',
	type: 'column',
	api: getTemperatureChartApi,
	opts: {
		color: [ "#73C0DE","#EE6666", "#3CA272",  "#9A60B4", "#ea7ccc","#FC8452","#1890FF", "#91CB74", "#FAC858"],
		padding: [15, 15, 0, 15],
		dataLabel: false,
		dataPointShape: true,
		dataPointShapeType: 'solid',
		enableScroll: false,
		legend: {},
		xAxis: {
			disableGrid: true,
			labelCount: 4,
			itemCount: 0, // x轴显示数据个数
		},
		yAxis: {
			// disabled:true,
			// gridType: "solid",
			gridType: "dash",
			dashLength: 2,
			data: [{
				min: 0,
				// max: 150
				axisLineColor: '#fff',
			}]
		},
		extra: {
			column: {
			            type: "group",
			            width: 20,
			            activeBgColor: "#000000",
			            activeBgOpacity: 0.08,
						animation: "vertical"
			          }
			// line: {
			// 	type: "curve",
			// 	width: 2,
			// 	activeType: "hollow",
			// 	linearType: "custom",
			// 	onShadow: true,
				
			// }
		}
	},
	async req(t, start, end, dateType, imei) {
		const {
			data
		} = await this.api(start, end, dateType, imei)
		return this.convert(t, dateType, data)
	},
	convert(t, dateType, data) {
		const keys = []
		const values = []
		data?.list?.forEach(item => {
			if (dateType == 'day') {
				keys.push(extractTimeFromTimestamp(item['v']))
			} else {
				keys.push((item['v']))
			}
			values.push(Number(item['k'].toFixed(2)))
		})
		const isEmpty = keys.length == 0 || values.length == 0
		return {
			isEmpty,
			chartData: this.charts(t, keys, values),
			data
		}
	},
	charts(t, keys, values) {
		this.opts.xAxis.itemCount = keys?.length || 0
		return {
			//模拟服务器返回数据，如果数据格式和标准格式不同，需自行按下面的格式拼接
			categories: keys || [],
			series: [
				{
					name: t("Temp"),
					data: values || []
				},
				
			]
		}
	}
}



export default temperature