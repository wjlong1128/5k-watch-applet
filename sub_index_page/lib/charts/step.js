import {
	getMotionChartApi,
} from "../api"
import {
	extractTimeFromTimestamp
} from "../utils"

const bloodOxygen = {
	// type: 'line',
	type: 'column',
	api: getMotionChartApi,
	opts: {
		color: ["#FAC858", "#EE6666", "#FAC858", "#EE6666", "#73C0DE", "#3CA272", "#FC8452", "#9A60B4", "#ea7ccc"],
		padding: [15, 15, 0, 15],
		dataLabel: false,
		dataPointShape: true,
		dataPointShapeType: 'solid',
		enableScroll: false,
		legend: {},
		xAxis: {
			disableGrid: true,
			labelCount: 4,
			itemCount: 0, // x轴显示数据个数
		},
		yAxis: {
			// disabled:true,
			// gridType: "solid",
			gridType: "dash",
			dashLength: 2,
			data: [{
				min: 0,
				// max: 150
				axisLineColor: '#fff',
			}]
		},
		extra: {
			column: {
				type: "group",
				width: 30,
				activeBgColor: "#000000",
				activeBgOpacity: 0.08,
				linearType: "custom",
				seriesGap: 5,
				linearOpacity: 0.5,
				barBorderCircle: true,
				customColor: [
					"#FA7D8D",
					"#EB88E2"
				]
			}
		}
	},
	async req(t, start, end, dateType, imei) {
		const {
			data
		} = await this.api(start, end, dateType, imei)
		return this.convert(t, dateType, data)
	},
	convert(t, dateType, data) {
		const keys = []
		const steps = []
		const calories = []
		data?.list?.forEach(item => {
			// if (dateType == 'day') {
			// 	keys.push(extractTimeFromTimestamp(item['v']))
			// } else {
			keys.push((item['v']))
			// }
			steps.push(item['s'])
			calories.push(item['d'])
		})
		// console.log('步数', data);
		const isEmpty = keys.length == 0 || steps.length == 0
		return {
			isEmpty,
			chartData: this.charts(t, keys, steps, calories),
			data
		}
	},
	charts(t, keys, steps, calories) {
		this.opts.xAxis.itemCount = keys?.length || 0
		return {
			//模拟服务器返回数据，如果数据格式和标准格式不同，需自行按下面的格式拼接
			categories: keys || [],
			series: [{
					name: t("Step"),
					data: steps || []
				},
				{
					name: t("data.countTotalCalories"),
					data: calories || []
				}
			]
		}
	}
}



export default bloodOxygen