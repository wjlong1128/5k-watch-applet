import {
	formatTimestamp
} from "../../utils"

/**
 * calendarType: 日历选择器类型,设置为空字符串就是不显示日历选择按钮
 * dateType: 代表时间类型，接口入参
 * text: 国际化参数
 * uniPickerField: uniPicker定制时间选择类型
 * showText: 国际化展示本日、本周、本月、本年
 * formatStr: 格式化时间显示到页面的方法
 */

const dateTypeDict = [{
		calendarType: 'date',
		dateType: 'day',
		text: 'day',
		uniPickerField: 'day',
		showText: 'data.showDay',
		formatStr(startTime, endTime) {
			return formatTimestamp(startTime, 'yyyy-MM-dd')
		}
	},
	{
		calendarType: 'week',
		dateType: 'week',
		text: 'week',
		uniPickerField: 'day',
		showText: 'data.showWeek',
		formatStr(startTime, endTime) {
			return formatTimestamp(startTime, 'yyyy-MM-dd') + '~' + formatTimestamp(endTime, 'yyyy-MM-dd')
		}
	},
	{
		calendarType: 'month',
		dateType: 'month',
		text: 'month',
		uniPickerField: 'month',
		showText: 'data.showMonth',
		formatStr(startTime, endTime) {
			return formatTimestamp(startTime, 'yyyy-MM');
		}
	},
	{
		calendarType: '',
		dateType: 'year',
		text: 'year',
		uniPickerField: 'year',
		showText: 'data.showYear',
		formatStr(startTime, endTime) {
			return formatTimestamp(startTime, 'yyyy')
		}
	},
]

export function getShowTextByDateType(t, dateType) {
	const e = dateTypeDict.filter(item => item.dateType == dateType)?.[0] || null
	if (e) {
		return t(e.showText)
	}
	return null
}

export default dateTypeDict