import request from '@/lib/utils/request.js'


/*
说明： 所有的chart接口都存在这四个参数start,end,dateType,imei
start: 开始时间字符串
end: 结束时间字符串
dateType: 时间类型字符串 day,week,month,year
imei: 设备号
*/

/**
 * 心率
 */
export function getHeartRateChartApi(start,end,dateType,imei){
	return request('/applet/watch/features/heartRate',{start,end,dateType,imei},{method: 'POST',loading: true})
}

/**
 * 血压
 */
export function getBloodPressureChartApi(start,end,dateType,imei){
	return request('/applet/watch/features/bloodPressure',{start,end,dateType,imei},{method: 'POST',loading: true})
}

/**
 * 体温
 */
export function getTemperatureChartApi(start,end,dateType,imei){
	return request('/applet/watch/features/temperature',{start,end,dateType,imei},{method: 'POST',loading: true})
}

/**
 * 血氧
 */
export function getBloodOxyChartApi(start,end,dateType,imei){
	return request('/applet/watch/features/bloodOxy ',{start,end,dateType,imei},{method: 'POST',loading: true})
}

/**
 * 血糖
 */
export function getBloodSugarChartApi(start,end,dateType,imei){
	return request('/applet/watch/features/bloodSugar ',{start,end,dateType,imei},{method: 'POST',loading: true})
}

/**
 * 提交血糖校准
 * @param {Number} timeFood 分钟
 * @param {Number} sugar 血糖值
 * @param {Number} diabetes 是否是糖尿病 1是 0不是
*/ 

export function submitBloodGlucoseCalibration(data) {
	return request('/calibration/sugarV2',data,{method: "POST",loading: true})
}



/**
 * 运动（步数）
 */
export function getMotionChartApi(start,end,dateType,imei){
	return request('/applet/watch/features/motion ',{start,end,dateType,imei},{method: 'POST',loading: true})
}
