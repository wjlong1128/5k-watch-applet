import {
	defineStore
} from 'pinia'
import {
	computed,
	ref
} from 'vue'
import {
	getDeviceInfoApi,getDevicePowerApi,
	getUserInfoApi, logOutApi
} from '../lib/api/user'
import {
	storeKeys
} from '../lib/constants'

import {useI18n} from 'vue-i18n'

export const useUserStore = defineStore('user', () => {
	const {t} = useI18n()
	/*
		avatarUrl: null
		birthday: "2014-05-11 00:00:00"
		gender: 1
		height: 167
		imei: "863967059002465"
		mobile: "137****0315"
		nickname: null
		openId: "oFDRb5akxvKtO87_Srenf1hJI29o"
		weight: 12
	 */
	const userInfo = ref(null) // 用户信息
	const wechatInfo = ref(null) // 拉取授权后的info 
	const imeiRef = ref(null)
	const deviceInfo = ref({
		signal: 0,
		battery: 0,
		deviceStatus: 1,
	})
	// const imei = computed(()=>{
	// 	const imei = uni.getStorageSync(storeKeys.imei)
		
	// })
	const setDeviceInfo = (deviceData)=>{
		deviceInfo.value.signal = parseInt(deviceData?.signal || 0)
		deviceInfo.value.battery = parseInt(deviceData?.battery || 0)
		deviceInfo.value.deviceStatus = parseInt(deviceData?.status || 1)
	}
	
	const refreshUserInfo = () => {
		if(!uni.getStorageSync(storeKeys.token)){
			userInfo.value = null;
			wechatInfo.value = null;
		}
		return {
			userInfo: userInfo.value, 
			wechatInfo: wechatInfo.value,
		}
	}
	
	const getImei = async ()=>{
		const imei = uni.getStorageSync(storeKeys.imei)
		if(imei) {
			return imei;
		}
		return null
	}



	const setWechatInfo = (v) => {
		wechatInfo.value = v;
		uni.setStorageSync(storeKeys.wechatInfo, JSON.stringify(v))
	}

	const getUserInfo = async () => {
		const token = uni.getStorageSync(storeKeys.token)
		if (token) {
			const {
				data
			} = await getUserInfoApi()
			
			userInfo.value = data;
			wechatInfo.value = uni.getStorageSync(storeKeys.wechatInfo)
			const imei = data.imei;
			imeiRef.value = imei
			if(imei){
				uni.setStorageSync(storeKeys.imei,imei)
			}else{
				uni.removeStorageSync(storeKeys.imei)
			}
			// const res = await getDeviceInfoApi(imei)
			// console.log('getDeviceInfoApi',res);
		}
	}

	const getDeviceStatus = async ()=>{
		const imei = uni.getStorageSync(storeKeys.imei)
		if (!imei) {
			uni.showToast({
				title: t('data.UnboundDevice'),
				icon:'fail'
			})
			return
		}
		const res = await getDevicePowerApi(imei)
		// 手表电量和信号数据为空
		if (!res.data) {
			uni.showToast({
				title:t('me.deviceInfoNull'),
				icon:'none',
				duration:4000
			})
			return
		}
		// setDeviceInfo(res.data)
	}

	const logout = async () => {
		await logOutApi()
		userInfo.value = null;
		wechatInfo.value = null;
		clearDeviceInfo()
		uni.removeStorageSync(storeKeys.langKey)
		uni.removeStorageSync(storeKeys.wechatInfo)
		uni.removeStorageSync(storeKeys.token)
		uni.removeStorageSync(storeKeys.openId)
		uni.removeStorageSync(storeKeys.imei)
		return new Promise((resolve, reject) => {
			uni.reLaunch({
				   url:'/pages/index/index',
				   success:() => {
						resolve()
				   },
				   fail() {
						reject('跳转登录失败')
				   }
			})
		})
	}

	const clearDeviceInfo = ()=>{
		deviceInfo.value = {
			signal: 0,
			battery: 0,
			deviceStatus: 1,
		}
	}

	const clearCurUser = ()=>{
			clearDeviceInfo()
			const token = uni.getStorageSync(storeKeys.token)
			if(!token){
				userInfo.value = null;
				wechatInfo.value =  null;
			}
		}
	
	

	return {
		imeiRef,
		userInfo,
		deviceInfo,
		refreshUserInfo,
		getUserInfo,
		setWechatInfo,
		setDeviceInfo,
		getDeviceStatus,
		getImei,
		logout,
		clearCurUser,
		clearDeviceInfo
	}
})