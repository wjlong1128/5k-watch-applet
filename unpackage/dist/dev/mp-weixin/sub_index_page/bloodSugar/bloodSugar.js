"use strict";
const common_vendor = require("../../common/vendor.js");
const lib_constants = require("../../lib/constants.js");
const sub_index_page_lib_utils = require("../lib/utils.js");
const sub_index_page_lib_charts_bloodSugar = require("../lib/charts/bloodSugar.js");
if (!Array) {
  const _easycom_qiun_data_charts2 = common_vendor.resolveComponent("qiun-data-charts");
  _easycom_qiun_data_charts2();
}
const _easycom_qiun_data_charts = () => "../../uni_modules/qiun-data-charts/components/qiun-data-charts/qiun-data-charts.js";
if (!Math) {
  (DateTypePacker + _easycom_qiun_data_charts + DataList)();
}
const DateTypePacker = () => "../lib/components/DateTypePacker/DateTypePacker.js";
const DataList = () => "../lib/components/DataList/DataList.js";
const _sfc_main = {
  __name: "bloodSugar",
  setup(__props) {
    const {
      t
    } = common_vendor.useI18n();
    const type = common_vendor.ref(sub_index_page_lib_charts_bloodSugar.bloodSugar.type);
    const chartData = common_vendor.ref(sub_index_page_lib_charts_bloodSugar.bloodSugar.charts(t));
    const isEmpty = common_vendor.ref(true);
    const opts = common_vendor.ref(sub_index_page_lib_charts_bloodSugar.bloodSugar.opts);
    const changeTitle = () => {
      common_vendor.index.setNavigationBarTitle({
        title: t("data.BloodSugar")
      });
    };
    const enableAnimation = common_vendor.ref(true);
    common_vendor.onLoad(() => {
      changeTitle();
      if (common_vendor.index.getSystemInfoSync().osName == "android") {
        enableAnimation.value = false;
      }
    });
    const dataUnit = common_vendor.ref("mmol");
    const dataRef = common_vendor.ref({
      "max": {
        // 最大
        text: t("MaxGlu"),
        value: 0,
        showUnit: true
      },
      "average": {
        // 平均
        text: t("AvgGlu"),
        value: 0,
        showUnit: true
      },
      "min": {
        // 最小
        text: t("MinGlu"),
        value: 0,
        showUnit: true
      },
      "size": {
        // 累计次数
        hide: false,
        text: t("reportCount"),
        value: 0,
        showUnit: false,
        unit: t("data.count.frequency")
        // 有这个也显示单位，没有并且showUnit false就不显示单位
      }
    });
    const showTitle = common_vendor.ref("");
    const dateChange = async (e) => {
      var _a;
      const start = sub_index_page_lib_utils.formatTimestampToString(e.start);
      const end = sub_index_page_lib_utils.formatTimestampToString(e.end);
      const rep = await sub_index_page_lib_charts_bloodSugar.bloodSugar.req(t, start, end, e.dateType, common_vendor.index.getStorageSync(lib_constants.storeKeys.imei));
      chartData.value = rep.chartData;
      const keys = Object.keys(dataRef.value);
      for (let k of keys) {
        dataRef.value[k].value = ((_a = rep.data) == null ? void 0 : _a[k]) || 0;
      }
      dataRef.value["size"].hide = e.dateType !== "day";
      isEmpty.value = rep.isEmpty;
      showTitle.value = e.showText;
    };
    const dataList = common_vendor.computed(() => {
      return Object.values(dataRef.value);
    });
    const calibrateBloodSugar = () => {
      common_vendor.index.navigateTo({
        url: "/sub_index_page/calibrateBloodSugar/calibrateBloodSugar"
      });
    };
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.o(dateChange),
        b: !isEmpty.value
      }, !isEmpty.value ? {
        c: common_vendor.p({
          type: type.value,
          opts: opts.value,
          canvas2d: true,
          animation: enableAnimation.value,
          chartData: chartData.value,
          ontouch: true,
          inScrollView: true
        })
      } : {}, {
        d: isEmpty.value
      }, isEmpty.value ? {
        e: common_vendor.t(_ctx.$t("data.empty"))
      } : {}, {
        f: common_vendor.t(showTitle.value),
        g: common_vendor.t(_ctx.$t("GluCalibration")),
        h: common_vendor.o(calibrateBloodSugar),
        i: common_vendor.p({
          unit: dataUnit.value,
          dataList: dataList.value
        }),
        j: common_vendor.t(_ctx.$t("data.desc"))
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-425b70ff"]]);
wx.createPage(MiniProgramPage);
