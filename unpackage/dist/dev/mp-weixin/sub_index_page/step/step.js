"use strict";
const common_vendor = require("../../common/vendor.js");
const lib_constants = require("../../lib/constants.js");
const sub_index_page_lib_utils = require("../lib/utils.js");
const sub_index_page_lib_charts_step = require("../lib/charts/step.js");
if (!Array) {
  const _easycom_qiun_data_charts2 = common_vendor.resolveComponent("qiun-data-charts");
  const _easycom_wd_circle2 = common_vendor.resolveComponent("wd-circle");
  (_easycom_qiun_data_charts2 + _easycom_wd_circle2)();
}
const _easycom_qiun_data_charts = () => "../../uni_modules/qiun-data-charts/components/qiun-data-charts/qiun-data-charts.js";
const _easycom_wd_circle = () => "../../uni_modules/wot-design-uni/components/wd-circle/wd-circle.js";
if (!Math) {
  (DateTypePacker + _easycom_qiun_data_charts + _easycom_wd_circle + DataList)();
}
const DateTypePacker = () => "../lib/components/DateTypePacker/DateTypePacker.js";
const DataList = () => "../lib/components/DataList/DataList.js";
const _sfc_main = {
  __name: "step",
  setup(__props) {
    const {
      t
    } = common_vendor.useI18n();
    const type = common_vendor.ref(sub_index_page_lib_charts_step.bloodOxygen.type);
    const chartData = common_vendor.ref(sub_index_page_lib_charts_step.bloodOxygen.charts(t));
    const isEmpty = common_vendor.ref(true);
    const opts = common_vendor.ref(sub_index_page_lib_charts_step.bloodOxygen.opts);
    const changeTitle = () => {
      common_vendor.index.setNavigationBarTitle({
        title: t("data.NumberOfSteps")
      });
    };
    const enableAnimation = common_vendor.ref(true);
    common_vendor.onLoad(() => {
      changeTitle();
      if (common_vendor.index.getSystemInfoSync().osName == "android") {
        enableAnimation.value = false;
      }
    });
    const dataUnit = common_vendor.ref(t("Km"));
    const dataRef = common_vendor.ref({
      "stepDistance": {
        // 总路程
        text: t("data.countTotalDistance"),
        value: 0,
        showUnit: true
      },
      "sumStep": {
        // 总步数
        text: t("data.countTotalSteps"),
        value: 0,
        showUnit: false,
        unit: t("Step.msg")
      },
      "stepDuration": {
        //  累计时长
        text: t("data.countTotalDuration"),
        value: 0,
        showUnit: true,
        unit: t("Minutes")
      },
      "sumCalories": {
        // 卡路里
        // hide: true,
        text: t("data.countTotalCalories"),
        value: 0,
        showUnit: false,
        unit: t("kcal")
        // 有这个也显示单位，没有并且showUnit false就不显示单位
      }
    });
    const progress = common_vendor.computed(() => {
      const sumStep = Math.min(7e3, dataRef.value.sumStep.value);
      return Number((sumStep / 7e3 * 100).toFixed(2));
    });
    const dateType = common_vendor.ref("day");
    const showTitle = common_vendor.ref("");
    const dateChange = async (e) => {
      var _a;
      const start = sub_index_page_lib_utils.formatTimestampToString(e.start);
      const end = sub_index_page_lib_utils.formatTimestampToString(e.end);
      const rep = await sub_index_page_lib_charts_step.bloodOxygen.req(t, start, end, e.dateType, common_vendor.index.getStorageSync(lib_constants.storeKeys.imei));
      chartData.value = rep.chartData;
      const keys = Object.keys(dataRef.value);
      for (let k of keys) {
        dataRef.value[k].value = ((_a = rep.data) == null ? void 0 : _a[k]) || 0;
      }
      dateType.value = e.dateType;
      isEmpty.value = rep.isEmpty;
      showTitle.value = e.showText;
    };
    const dataList = common_vendor.computed(() => {
      return Object.values(dataRef.value);
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.o(dateChange),
        b: !isEmpty.value && dateType.value != "day"
      }, !isEmpty.value && dateType.value != "day" ? {
        c: common_vendor.p({
          type: type.value,
          opts: opts.value,
          canvas2d: true,
          animation: enableAnimation.value,
          chartData: chartData.value,
          ontouch: true,
          inScrollView: true
        })
      } : {}, {
        d: !isEmpty.value && dateType.value == "day"
      }, !isEmpty.value && dateType.value == "day" ? {
        e: common_vendor.o(($event) => progress.value = $event),
        f: common_vendor.p({
          layerColor: "#f2f2f2",
          strokeWidth: 30,
          text: `${_ctx.$t("Step.compet")}：${progress.value}%`,
          size: 200,
          modelValue: progress.value
        }),
        g: common_vendor.t(_ctx.$t("Step.today")),
        h: common_vendor.t(dataRef.value.sumStep.value),
        i: common_vendor.t(_ctx.$t("Step.msg"))
      } : {}, {
        j: isEmpty.value
      }, isEmpty.value ? {
        k: common_vendor.t(_ctx.$t("data.empty"))
      } : {}, {
        l: common_vendor.t(showTitle.value),
        m: common_vendor.p({
          unit: dataUnit.value,
          dataList: dataList.value
        }),
        n: common_vendor.t(_ctx.$t("data.desc"))
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-99429234"]]);
wx.createPage(MiniProgramPage);
