"use strict";
const common_vendor = require("../../common/vendor.js");
const lib_constants = require("../../lib/constants.js");
const sub_index_page_lib_utils = require("../lib/utils.js");
const sub_index_page_lib_charts_heartRate = require("../lib/charts/heartRate.js");
if (!Array) {
  const _easycom_qiun_data_charts2 = common_vendor.resolveComponent("qiun-data-charts");
  _easycom_qiun_data_charts2();
}
const _easycom_qiun_data_charts = () => "../../uni_modules/qiun-data-charts/components/qiun-data-charts/qiun-data-charts.js";
if (!Math) {
  (DateTypePacker + _easycom_qiun_data_charts + DataList)();
}
const DateTypePacker = () => "../lib/components/DateTypePacker/DateTypePacker.js";
const DataList = () => "../lib/components/DataList/DataList.js";
const _sfc_main = {
  __name: "heartRate",
  setup(__props) {
    const {
      t
    } = common_vendor.useI18n();
    const type = common_vendor.ref(sub_index_page_lib_charts_heartRate.heartRate.type);
    const chartData = common_vendor.ref(sub_index_page_lib_charts_heartRate.heartRate.charts(t));
    const isEmpty = common_vendor.ref(true);
    const opts = common_vendor.ref(sub_index_page_lib_charts_heartRate.heartRate.opts);
    const changeTitle = () => {
      common_vendor.index.setNavigationBarTitle({
        title: t("data.HeartRate")
      });
    };
    const enableAnimation = common_vendor.ref(true);
    common_vendor.onLoad(() => {
      changeTitle();
      if (common_vendor.index.getSystemInfoSync().osName == "android") {
        enableAnimation.value = false;
      }
    });
    const dataUnit = common_vendor.ref("bpm");
    const dataRef = common_vendor.ref({
      "max": {
        // 最大
        text: t("MaxHr"),
        value: 0,
        showUnit: true
      },
      "average": {
        // 平均
        text: t("AvgHr"),
        value: 0,
        showUnit: true
      },
      "min": {
        // 最小
        text: t("MinHr"),
        value: 0,
        showUnit: true
      },
      "size": {
        // 累计次数
        hide: false,
        text: t("reportCount"),
        value: 0,
        showUnit: false,
        unit: t("data.count.frequency")
      }
    });
    const dataList = common_vendor.computed(() => {
      return Object.values(dataRef.value);
    });
    const showTitle = common_vendor.ref("");
    const dateChange = async (e) => {
      var _a;
      const start = sub_index_page_lib_utils.formatTimestampToString(e.start);
      const end = sub_index_page_lib_utils.formatTimestampToString(e.end);
      const rep = await sub_index_page_lib_charts_heartRate.heartRate.req(t, start, end, e.dateType, common_vendor.index.getStorageSync(lib_constants.storeKeys.imei));
      chartData.value = rep.chartData;
      const keys = Object.keys(dataRef.value);
      for (let k of keys) {
        dataRef.value[k].value = ((_a = rep.data) == null ? void 0 : _a[k]) || 0;
      }
      dataRef.value["size"].hide = e.dateType !== "day";
      isEmpty.value = rep.isEmpty;
      showTitle.value = e.showText;
    };
    const calendarStatus = common_vendor.ref(false);
    const closeCalendar = () => {
    };
    const openCalendar = () => {
    };
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.o(openCalendar),
        b: common_vendor.o(closeCalendar),
        c: common_vendor.o(dateChange),
        d: !isEmpty.value && !calendarStatus.value
      }, !isEmpty.value && !calendarStatus.value ? {
        e: common_vendor.p({
          type: type.value,
          opts: opts.value,
          canvas2d: true,
          animation: enableAnimation.value,
          chartData: chartData.value,
          ontouch: true
        })
      } : {}, {
        f: isEmpty.value || calendarStatus.value
      }, isEmpty.value || calendarStatus.value ? {
        g: common_vendor.t(_ctx.$t("data.empty"))
      } : {}, {
        h: common_vendor.t(showTitle.value),
        i: common_vendor.p({
          unit: dataUnit.value,
          dataList: dataList.value
        }),
        j: common_vendor.t(_ctx.$t("data.desc"))
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-3ad6dcfa"]]);
wx.createPage(MiniProgramPage);
