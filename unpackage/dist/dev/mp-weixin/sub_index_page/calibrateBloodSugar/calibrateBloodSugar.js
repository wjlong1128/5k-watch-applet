"use strict";
const common_vendor = require("../../common/vendor.js");
const uni_modules_wotDesignUni_components_wdToast_index = require("../../uni_modules/wot-design-uni/components/wd-toast/index.js");
require("../../uni_modules/wot-design-uni/locale/index.js");
const sub_index_page_lib_api = require("../lib/api.js");
const lib_utils_utils = require("../../lib/utils/utils.js");
if (!Array) {
  const _easycom_wd_toast2 = common_vendor.resolveComponent("wd-toast");
  const _easycom_wd_input2 = common_vendor.resolveComponent("wd-input");
  const _easycom_wd_radio2 = common_vendor.resolveComponent("wd-radio");
  const _easycom_wd_radio_group2 = common_vendor.resolveComponent("wd-radio-group");
  const _easycom_wd_cell_group2 = common_vendor.resolveComponent("wd-cell-group");
  const _easycom_wd_button2 = common_vendor.resolveComponent("wd-button");
  const _easycom_wd_form2 = common_vendor.resolveComponent("wd-form");
  const _easycom_wd_table_col2 = common_vendor.resolveComponent("wd-table-col");
  const _easycom_wd_table2 = common_vendor.resolveComponent("wd-table");
  (_easycom_wd_toast2 + _easycom_wd_input2 + _easycom_wd_radio2 + _easycom_wd_radio_group2 + _easycom_wd_cell_group2 + _easycom_wd_button2 + _easycom_wd_form2 + _easycom_wd_table_col2 + _easycom_wd_table2)();
}
const _easycom_wd_toast = () => "../../uni_modules/wot-design-uni/components/wd-toast/wd-toast.js";
const _easycom_wd_input = () => "../../uni_modules/wot-design-uni/components/wd-input/wd-input.js";
const _easycom_wd_radio = () => "../../uni_modules/wot-design-uni/components/wd-radio/wd-radio.js";
const _easycom_wd_radio_group = () => "../../uni_modules/wot-design-uni/components/wd-radio-group/wd-radio-group.js";
const _easycom_wd_cell_group = () => "../../uni_modules/wot-design-uni/components/wd-cell-group/wd-cell-group.js";
const _easycom_wd_button = () => "../../uni_modules/wot-design-uni/components/wd-button/wd-button.js";
const _easycom_wd_form = () => "../../uni_modules/wot-design-uni/components/wd-form/wd-form.js";
const _easycom_wd_table_col = () => "../../uni_modules/wot-design-uni/components/wd-table-col/wd-table-col.js";
const _easycom_wd_table = () => "../../uni_modules/wot-design-uni/components/wd-table/wd-table.js";
if (!Math) {
  (_easycom_wd_toast + _easycom_wd_input + _easycom_wd_radio + _easycom_wd_radio_group + _easycom_wd_cell_group + _easycom_wd_button + _easycom_wd_form + _easycom_wd_table_col + _easycom_wd_table)();
}
const _sfc_main = {
  __name: "calibrateBloodSugar",
  setup(__props) {
    const {
      success: showSuccess,
      close: closeToast
    } = uni_modules_wotDesignUni_components_wdToast_index.useToast();
    const {
      t
    } = common_vendor.useI18n();
    const dataList = common_vendor.ref([]);
    const clearItem = (row, index) => {
      dataList.value.splice(index, 1);
    };
    const submitBtnDisable = common_vendor.computed(() => {
      return !(dataList.value.length > 0);
    });
    const addBtnDisable = common_vendor.computed(() => {
      return dataList.value.length >= 3;
    });
    const model = common_vendor.reactive({
      timeFood: void 0,
      // 饭后时间
      sugar: void 0,
      // 血糖值
      diabetes: 0
      // 是否糖尿病 1是 0不是
    });
    const submitHandle = async () => {
      if (dataList.value.length == 0)
        return;
      const req = dataList.value.map((e) => ({
        ...e,
        diabetes: model.diabetes
      }));
      await sub_index_page_lib_api.submitBloodGlucoseCalibration(req);
      dataList.value = [];
      showSuccess({
        msg: t("universal.Submission")
      });
    };
    const formRef = common_vendor.ref();
    const validatorTimeFood = (val) => {
      return val && lib_utils_utils.isNumber(val) && val >= 0;
    };
    const validatorSugarValue = (val) => {
      return val && lib_utils_utils.isNumber(val) && val > 0 && val >= 3.9 && val <= 33;
    };
    function handleAdd() {
      formRef.value.validate().then(async ({
        valid,
        errors
      }) => {
        if (valid) {
          dataList.value.push({
            sugar: model.sugar,
            timeFood: model.timeFood
          });
          model.sugar = void 0;
          model.timeFood = void 0;
        }
      }).catch((error) => {
        console.log(error, "error");
      });
    }
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.o(($event) => model.timeFood = $event),
        b: common_vendor.p({
          label: _ctx.$t("AfterMealTime") + `(${_ctx.$t("Minutes")})`,
          ["label-width"]: "150px",
          prop: "timeFood",
          clearable: true,
          placeholder: _ctx.$t("PleaseEnterAfterMealTime"),
          type: "number",
          rules: [{
            required: true,
            validator: validatorTimeFood,
            message: _ctx.$t("PleaseEnterAfterMealTime")
          }],
          modelValue: model.timeFood
        }),
        c: common_vendor.o(($event) => model.sugar = $event),
        d: common_vendor.p({
          label: _ctx.$t("BloodSugarValue") + "(mmol/L)",
          ["label-width"]: "150px",
          type: "digit",
          prop: "sugar",
          clearable: true,
          placeholder: _ctx.$t("PleaseEnterBloodSugarValue"),
          rules: [{
            required: true,
            validator: validatorSugarValue,
            message: _ctx.$t("PleaseEnterBloodSugarValue")
          }],
          modelValue: model.sugar
        }),
        e: common_vendor.t(_ctx.$t("DiabetesStatus")),
        f: common_vendor.t(_ctx.$t("Yes")),
        g: common_vendor.p({
          value: 1
        }),
        h: common_vendor.t(_ctx.$t("No")),
        i: common_vendor.p({
          value: 0
        }),
        j: common_vendor.o(($event) => model.diabetes = $event),
        k: common_vendor.p({
          shape: "button",
          modelValue: model.diabetes
        }),
        l: common_vendor.p({
          border: true
        }),
        m: common_vendor.t(_ctx.$t("CalibratingMsgHint")),
        n: common_vendor.t(_ctx.$t("data.add")),
        o: common_vendor.o(handleAdd),
        p: common_vendor.p({
          type: "primary",
          disabled: addBtnDisable.value,
          block: true
        }),
        q: common_vendor.sr(formRef, "198f0787-1", {
          "k": "formRef"
        }),
        r: common_vendor.p({
          model
        }),
        s: !submitBtnDisable.value
      }, !submitBtnDisable.value ? {
        t: common_vendor.t(`${_ctx.$t("AddTips").replace("0", dataList.value.length)}`),
        v: common_vendor.t(_ctx.$t("universal.submit")),
        w: common_vendor.o(submitHandle),
        x: common_vendor.p({
          round: false,
          type: "success"
        })
      } : {}, {
        y: common_vendor.f(dataList.value, (item, index, i0) => {
          return {
            a: common_vendor.w(({
              row
            }, s1, i1) => {
              return {
                a: common_vendor.t(row.timeFood + " " + _ctx.$t("Minutes")),
                b: i1,
                c: s1
              };
            }, {
              name: "value",
              path: "y[" + i0 + "].a",
              vueId: "198f0787-11-" + i0 + "," + ("198f0787-10-" + i0)
            }),
            b: "198f0787-11-" + i0 + "," + ("198f0787-10-" + i0),
            c: common_vendor.w(({
              row
            }, s1, i1) => {
              return {
                a: common_vendor.t(row.sugar + " mmol/L"),
                b: i1,
                c: s1
              };
            }, {
              name: "value",
              path: "y[" + i0 + "].c",
              vueId: "198f0787-12-" + i0 + "," + ("198f0787-10-" + i0)
            }),
            d: "198f0787-12-" + i0 + "," + ("198f0787-10-" + i0),
            e: common_vendor.w(({
              row
            }, s1, i1) => {
              return {
                a: common_vendor.o(($event) => clearItem(row, index), index),
                b: "198f0787-14-" + i0 + "-" + i1 + "," + ("198f0787-13-" + i0),
                c: i1,
                d: s1
              };
            }, {
              name: "value",
              path: "y[" + i0 + "].e",
              vueId: "198f0787-13-" + i0 + "," + ("198f0787-10-" + i0)
            }),
            f: "198f0787-13-" + i0 + "," + ("198f0787-10-" + i0),
            g: "198f0787-10-" + i0,
            h: common_vendor.p({
              data: [item]
            }),
            i: index
          };
        }),
        z: common_vendor.p({
          align: "center",
          width: "33%",
          prop: "timeFood",
          label: _ctx.$t("AfterMealTime")
        }),
        A: common_vendor.p({
          align: "center",
          width: "33%",
          prop: "sugar",
          label: _ctx.$t("BloodSugarValue")
        }),
        B: common_vendor.t(_ctx.$t("data.delete")),
        C: common_vendor.p({
          round: false,
          size: "small",
          type: "error"
        }),
        D: common_vendor.p({
          align: "center",
          prop: "timeFood",
          label: _ctx.$t("data.delete"),
          width: "33%"
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-198f0787"]]);
wx.createPage(MiniProgramPage);
