"use strict";
function extractTimeFromTimestamp(timestamp) {
  const regex = /\d{2}:\d{2}/;
  const result = timestamp.match(regex)[0];
  return result;
}
function getStartAndEndTimestamps(timestamp, dateType, step = 0) {
  const date = new Date(timestamp);
  let startTimestamp, endTimestamp;
  switch (dateType) {
    case "day":
      date.setDate(date.getDate() + step);
      startTimestamp = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0).getTime();
      endTimestamp = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59, 999).getTime();
      break;
    case "week":
      const dayOfWeek = date.getDay();
      const diffToMonday = (dayOfWeek === 0 ? -6 : 1) - dayOfWeek;
      date.setDate(date.getDate() + step * 7);
      const startOfWeek = new Date(date);
      startOfWeek.setDate(date.getDate() + diffToMonday);
      startOfWeek.setHours(0, 0, 0, 0);
      startTimestamp = startOfWeek.getTime();
      const endOfWeek = new Date(startOfWeek);
      endOfWeek.setDate(startOfWeek.getDate() + 6);
      endOfWeek.setHours(23, 59, 59, 999);
      endTimestamp = endOfWeek.getTime();
      break;
    case "month":
      date.setMonth(date.getMonth() + step);
      startTimestamp = new Date(date.getFullYear(), date.getMonth(), 1, 0, 0, 0, 0).getTime();
      endTimestamp = new Date(date.getFullYear(), date.getMonth() + 1, 0, 23, 59, 59, 999).getTime();
      break;
    case "year":
      date.setFullYear(date.getFullYear() + step);
      startTimestamp = new Date(date.getFullYear(), 0, 1, 0, 0, 0, 0).getTime();
      endTimestamp = new Date(date.getFullYear(), 11, 31, 23, 59, 59, 999).getTime();
      break;
    default:
      throw new Error("Invalid dateType. Use 'day', 'week', 'month', or 'year'.");
  }
  return { startTimestamp, endTimestamp };
}
function formatTimestampToString(timestamp) {
  const date = new Date(timestamp);
  const year = date.getFullYear();
  const month = (date.getMonth() + 1).toString().padStart(2, "0");
  const day = date.getDate().toString().padStart(2, "0");
  const hours = date.getHours().toString().padStart(2, "0");
  const minutes = date.getMinutes().toString().padStart(2, "0");
  const seconds = date.getSeconds().toString().padStart(2, "0");
  return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}
function formatTimestamp(timestamp, format) {
  const date = new Date(timestamp);
  const replacements = {
    "yyyy": date.getFullYear(),
    "MM": (date.getMonth() + 1).toString().padStart(2, "0"),
    "dd": date.getDate().toString().padStart(2, "0"),
    "HH": date.getHours().toString().padStart(2, "0"),
    "mm": date.getMinutes().toString().padStart(2, "0"),
    "ss": date.getSeconds().toString().padStart(2, "0")
  };
  return format.replace(/yyyy|MM|dd|HH|mm|ss/g, (match) => replacements[match]);
}
function getYearStartTimestampOffset(offset) {
  let currentYear = (/* @__PURE__ */ new Date()).getFullYear();
  let targetYear = currentYear + offset;
  let startOfYear = new Date(targetYear, 0, 1, 0, 0, 0, 0);
  let timestamp = startOfYear.getTime();
  return timestamp;
}
function getStartAndEndTimestampsByPicker(dateType, dateString) {
  let startTimestamp, endTimestamp;
  switch (dateType) {
    case "year":
      const year = parseInt(dateString, 10);
      startTimestamp = new Date(year, 0, 1, 0, 0, 0, 0).getTime();
      endTimestamp = new Date(year, 11, 31, 23, 59, 59, 999).getTime();
      break;
    case "month":
      const [yearMonth, month] = dateString.split("-").map(Number);
      startTimestamp = new Date(yearMonth, month - 1, 1, 0, 0, 0, 0).getTime();
      endTimestamp = new Date(yearMonth, month, 0, 23, 59, 59, 999).getTime();
      break;
    case "week":
      const date = new Date(dateString);
      const dayOfWeek = date.getDay();
      const diffToMonday = (dayOfWeek === 0 ? -6 : 1) - dayOfWeek;
      const startOfWeek = new Date(date);
      startOfWeek.setDate(date.getDate() + diffToMonday);
      startOfWeek.setHours(0, 0, 0, 0);
      startTimestamp = startOfWeek.getTime();
      const endOfWeek = new Date(startOfWeek);
      endOfWeek.setDate(startOfWeek.getDate() + 6);
      endOfWeek.setHours(23, 59, 59, 999);
      endTimestamp = endOfWeek.getTime();
      break;
    case "day":
      const dayDate = new Date(dateString);
      startTimestamp = new Date(dayDate.getFullYear(), dayDate.getMonth(), dayDate.getDate(), 0, 0, 0, 0).getTime();
      endTimestamp = new Date(dayDate.getFullYear(), dayDate.getMonth(), dayDate.getDate(), 23, 59, 59, 999).getTime();
      break;
    default:
      throw new Error("Invalid dateType. Use 'year', 'month', 'week', or 'day'.");
  }
  return { startTimestamp, endTimestamp };
}
exports.extractTimeFromTimestamp = extractTimeFromTimestamp;
exports.formatTimestamp = formatTimestamp;
exports.formatTimestampToString = formatTimestampToString;
exports.getStartAndEndTimestamps = getStartAndEndTimestamps;
exports.getStartAndEndTimestampsByPicker = getStartAndEndTimestampsByPicker;
exports.getYearStartTimestampOffset = getYearStartTimestampOffset;
