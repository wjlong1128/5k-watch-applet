"use strict";
const sub_index_page_lib_api = require("../api.js");
const sub_index_page_lib_utils = require("../utils.js");
const bloodOxygen = {
  // type: 'line',
  type: "column",
  api: sub_index_page_lib_api.getBloodOxyChartApi,
  opts: {
    color: ["#01dcd2", "#01dcd2"],
    padding: [15, 15, 0, 15],
    dataLabel: false,
    dataPointShape: true,
    dataPointShapeType: "solid",
    enableScroll: false,
    legend: {},
    xAxis: {
      disableGrid: true,
      labelCount: 4,
      itemCount: 0
      // x轴显示数据个数
    },
    yAxis: {
      // disabled:true,
      // gridType: "solid",
      gridType: "dash",
      dashLength: 2,
      data: [{
        min: 0,
        // max: 150
        axisLineColor: "#fff"
      }]
    },
    extra: {
      column: {
        type: "group",
        width: 30,
        activeBgColor: "#000000",
        activeBgOpacity: 0.08,
        linearType: "custom",
        seriesGap: 5,
        linearOpacity: 0.5,
        barBorderCircle: true,
        customColor: [
          "#01dcd2"
        ]
      }
    }
  },
  async req(t, start, end, dateType, imei) {
    const {
      data
    } = await this.api(start, end, dateType, imei);
    return this.convert(t, dateType, data);
  },
  convert(t, dateType, data) {
    var _a;
    const keys = [];
    const values = [];
    (_a = data == null ? void 0 : data.list) == null ? void 0 : _a.forEach((item) => {
      if (dateType == "day") {
        keys.push(sub_index_page_lib_utils.extractTimeFromTimestamp(item["v"]));
      } else {
        keys.push(item["v"]);
      }
      values.push(item["k"]);
    });
    const isEmpty = keys.length == 0 || values.length == 0;
    return {
      isEmpty,
      chartData: this.charts(t, keys, values),
      data
    };
  },
  charts(t, keys, values) {
    this.opts.xAxis.itemCount = (keys == null ? void 0 : keys.length) || 0;
    return {
      //模拟服务器返回数据，如果数据格式和标准格式不同，需自行按下面的格式拼接
      categories: keys || [],
      series: [
        {
          name: t("SpO2"),
          data: values || []
        }
      ]
    };
  }
};
exports.bloodOxygen = bloodOxygen;
