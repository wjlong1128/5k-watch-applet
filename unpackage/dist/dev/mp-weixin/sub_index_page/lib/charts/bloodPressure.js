"use strict";
const sub_index_page_lib_api = require("../api.js");
const sub_index_page_lib_utils = require("../utils.js");
const bloodPressure = {
  // type: 'line',
  type: "line",
  api: sub_index_page_lib_api.getBloodPressureChartApi,
  opts: {
    color: ["#FC8452", "#1890FF", "#91CB74", "#FAC858", "#EE6666", "#73C0DE", "#3CA272", "#9A60B4", "#ea7ccc"],
    padding: [15, 15, 0, 15],
    dataLabel: false,
    dataPointShape: true,
    dataPointShapeType: "solid",
    enableScroll: false,
    legend: {},
    xAxis: {
      disableGrid: true,
      labelCount: 4,
      itemCount: 0
      // x轴显示数据个数
    },
    yAxis: {
      // disabled:true,
      // gridType: "solid",
      gridType: "dash",
      dashLength: 2,
      data: [{
        min: 30,
        // max: 150
        axisLineColor: "#fff"
      }]
    },
    extra: {
      line: {
        type: "curve",
        width: 2,
        activeType: "hollow",
        linearType: "custom",
        onShadow: true,
        animation: "vertical"
      }
    }
  },
  async req(t, start, end, dateType, imei) {
    const {
      data
    } = await this.api(start, end, dateType, imei);
    return this.convert(t, dateType, data);
  },
  convert(t, dateType, data) {
    var _a;
    const keys = [];
    const hs = [];
    const ls = [];
    (_a = data == null ? void 0 : data.list) == null ? void 0 : _a.forEach((item) => {
      if (dateType == "day") {
        keys.push(sub_index_page_lib_utils.extractTimeFromTimestamp(item["v"]));
      } else {
        keys.push(item["v"]);
      }
      hs.push(item["s"]);
      ls.push(item["d"]);
    });
    const isEmpty = keys.length == 0 || hs.length == 0;
    return {
      isEmpty,
      chartData: this.charts(t, keys, hs, ls),
      data
    };
  },
  charts(t, keys, hs, ls) {
    this.opts.xAxis.itemCount = (keys == null ? void 0 : keys.length) || 0;
    return {
      //模拟服务器返回数据，如果数据格式和标准格式不同，需自行按下面的格式拼接
      categories: keys || [],
      series: [
        {
          name: t("HP"),
          linearColor: [
            [
              0,
              "#FAC858"
            ],
            [
              0.33,
              "#FFC371"
            ],
            [
              0.66,
              "#FFC2B2"
            ],
            [
              1,
              "#FA7D8D"
            ]
          ],
          setShadow: [
            3,
            8,
            10,
            "#FC8452"
          ],
          data: hs || []
        },
        {
          name: t("LP"),
          linearColor: [
            [
              0,
              "#1890FF"
            ],
            [
              0.25,
              "#00B5FF"
            ],
            [
              0.5,
              "#00D1ED"
            ],
            [
              0.75,
              "#00E6BB"
            ],
            [
              1,
              "#90F489"
            ]
          ],
          setShadow: [
            3,
            8,
            10,
            "#1890FF"
          ],
          data: ls || []
        }
      ]
    };
  }
};
exports.bloodPressure = bloodPressure;
