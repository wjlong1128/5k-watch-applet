"use strict";
const lib_utils_request = require("../../lib/utils/request.js");
function getHeartRateChartApi(start, end, dateType, imei) {
  return lib_utils_request.request("/applet/watch/features/heartRate", { start, end, dateType, imei }, { method: "POST", loading: true });
}
function getBloodPressureChartApi(start, end, dateType, imei) {
  return lib_utils_request.request("/applet/watch/features/bloodPressure", { start, end, dateType, imei }, { method: "POST", loading: true });
}
function getTemperatureChartApi(start, end, dateType, imei) {
  return lib_utils_request.request("/applet/watch/features/temperature", { start, end, dateType, imei }, { method: "POST", loading: true });
}
function getBloodOxyChartApi(start, end, dateType, imei) {
  return lib_utils_request.request("/applet/watch/features/bloodOxy ", { start, end, dateType, imei }, { method: "POST", loading: true });
}
function getBloodSugarChartApi(start, end, dateType, imei) {
  return lib_utils_request.request("/applet/watch/features/bloodSugar ", { start, end, dateType, imei }, { method: "POST", loading: true });
}
function submitBloodGlucoseCalibration(data) {
  return lib_utils_request.request("/calibration/sugarV2", data, { method: "POST", loading: true });
}
function getMotionChartApi(start, end, dateType, imei) {
  return lib_utils_request.request("/applet/watch/features/motion ", { start, end, dateType, imei }, { method: "POST", loading: true });
}
exports.getBloodOxyChartApi = getBloodOxyChartApi;
exports.getBloodPressureChartApi = getBloodPressureChartApi;
exports.getBloodSugarChartApi = getBloodSugarChartApi;
exports.getHeartRateChartApi = getHeartRateChartApi;
exports.getMotionChartApi = getMotionChartApi;
exports.getTemperatureChartApi = getTemperatureChartApi;
exports.submitBloodGlucoseCalibration = submitBloodGlucoseCalibration;
