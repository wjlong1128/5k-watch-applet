"use strict";
const sub_index_page_lib_utils = require("../../utils.js");
const dateTypeDict = [
  {
    calendarType: "date",
    dateType: "day",
    text: "day",
    uniPickerField: "day",
    showText: "data.showDay",
    formatStr(startTime, endTime) {
      return sub_index_page_lib_utils.formatTimestamp(startTime, "yyyy-MM-dd");
    }
  },
  {
    calendarType: "week",
    dateType: "week",
    text: "week",
    uniPickerField: "day",
    showText: "data.showWeek",
    formatStr(startTime, endTime) {
      return sub_index_page_lib_utils.formatTimestamp(startTime, "yyyy-MM-dd") + "~" + sub_index_page_lib_utils.formatTimestamp(endTime, "yyyy-MM-dd");
    }
  },
  {
    calendarType: "month",
    dateType: "month",
    text: "month",
    uniPickerField: "month",
    showText: "data.showMonth",
    formatStr(startTime, endTime) {
      return sub_index_page_lib_utils.formatTimestamp(startTime, "yyyy-MM");
    }
  },
  {
    calendarType: "",
    dateType: "year",
    text: "year",
    uniPickerField: "year",
    showText: "data.showYear",
    formatStr(startTime, endTime) {
      return sub_index_page_lib_utils.formatTimestamp(startTime, "yyyy");
    }
  }
];
exports.dateTypeDict = dateTypeDict;
