"use strict";
const common_vendor = require("../../../../common/vendor.js");
const sub_index_page_lib_utils = require("../../utils.js");
const sub_index_page_lib_components_DateTypePacker_dateTypeDict = require("./dateTypeDict.js");
if (!Array) {
  const _easycom_wd_icon2 = common_vendor.resolveComponent("wd-icon");
  const _easycom_wd_calendar2 = common_vendor.resolveComponent("wd-calendar");
  (_easycom_wd_icon2 + _easycom_wd_calendar2)();
}
const _easycom_wd_icon = () => "../../../../uni_modules/wot-design-uni/components/wd-icon/wd-icon.js";
const _easycom_wd_calendar = () => "../../../../uni_modules/wot-design-uni/components/wd-calendar/wd-calendar.js";
if (!Math) {
  (HeaderTab + _easycom_wd_icon + _easycom_wd_calendar)();
}
const HeaderTab = () => "../../../../components/HeaderTab/HeaderTab.js";
const _sfc_main = {
  __name: "DateTypePacker",
  emits: ["dateChange", "openCalendar", "closeCalendar"],
  setup(__props, { emit: __emit }) {
    const {
      t
    } = common_vendor.useI18n();
    const emits = __emit;
    const curDict = common_vendor.ref(sub_index_page_lib_components_DateTypePacker_dateTypeDict.dateTypeDict[0]);
    const uniPickDate = common_vendor.ref(sub_index_page_lib_utils.formatTimestamp((/* @__PURE__ */ new Date()).getTime(), "yyyy-MM-dd"));
    const tabList = common_vendor.ref(sub_index_page_lib_components_DateTypePacker_dateTypeDict.dateTypeDict);
    const tabListBind = common_vendor.computed(() => {
      return tabList.value.map((item) => {
        item.localText = t(item.text);
        return {
          name: item.localText,
          key: item.dateType
        };
      });
    });
    const tab = common_vendor.ref(sub_index_page_lib_components_DateTypePacker_dateTypeDict.dateTypeDict[0].dateType);
    const time = common_vendor.ref((/* @__PURE__ */ new Date()).getTime());
    const {
      startTimestamp,
      endTimestamp
    } = sub_index_page_lib_utils.getStartAndEndTimestamps((/* @__PURE__ */ new Date()).getTime(), sub_index_page_lib_components_DateTypePacker_dateTypeDict.dateTypeDict[0].dateType);
    const startTime = common_vendor.ref(startTimestamp);
    const endTime = common_vendor.ref(endTimestamp);
    const timeStr = common_vendor.computed(() => {
      for (let i = 0; i < sub_index_page_lib_components_DateTypePacker_dateTypeDict.dateTypeDict.length; i++) {
        if (sub_index_page_lib_components_DateTypePacker_dateTypeDict.dateTypeDict[i].dateType == tab.value) {
          return sub_index_page_lib_components_DateTypePacker_dateTypeDict.dateTypeDict[i].formatStr(startTime.value, endTime.value);
        }
      }
      return "none!";
    });
    const calendarShow = common_vendor.ref(false);
    const wdCalendarRef = common_vendor.ref(null);
    const calendar = common_vendor.ref((/* @__PURE__ */ new Date()).getTime());
    const openCalendar = () => {
      var _a;
      calendarShow.value = true;
      (_a = wdCalendarRef.value) == null ? void 0 : _a.open();
      emits("openCalendar");
    };
    const closeCalendar = () => {
      if (calendarShow.value) {
        emits("closeCalendar");
        calendarShow.value = false;
      }
    };
    const handleConfirm = ({
      value
    }) => {
      console.log(value);
      const {
        startTimestamp: s,
        endTimestamp: e
      } = sub_index_page_lib_utils.getStartAndEndTimestamps(value, tab.value);
      if (s == startTime.value && e == endTime.value) {
        return;
      }
      startTime.value = s;
      endTime.value = e;
      time.value = s;
      emits("dateChange", {
        start: startTime.value,
        end: endTime.value,
        dateType: tab.value,
        showText: t(curDict.value.showText)
      });
    };
    common_vendor.watch(time, (newTime) => {
      uniPickDate.value = sub_index_page_lib_utils.formatTimestamp(newTime, "yyyy-MM-dd");
      calendar.value = newTime;
    });
    const handleTabClick = ({
      key: dateType
    }) => {
      time.value = (/* @__PURE__ */ new Date()).getTime();
      const {
        startTimestamp: s,
        endTimestamp: e
      } = sub_index_page_lib_utils.getStartAndEndTimestamps(time.value, dateType);
      startTime.value = s;
      endTime.value = e;
      time.value = s;
      curDict.value = sub_index_page_lib_components_DateTypePacker_dateTypeDict.dateTypeDict.find((dict) => dict.dateType == dateType);
      emits("dateChange", {
        start: startTime.value,
        end: endTime.value,
        dateType,
        showText: t(curDict.value.showText)
      });
    };
    common_vendor.onLoad(async () => {
      await common_vendor.nextTick$1();
      emits("dateChange", {
        start: startTime.value,
        end: endTime.value,
        dateType: tab.value,
        showText: t(curDict.value.showText)
      });
    });
    const prevTime = () => {
      const {
        startTimestamp: s,
        endTimestamp: e
      } = sub_index_page_lib_utils.getStartAndEndTimestamps(time.value, tab.value, -1);
      startTime.value = s;
      endTime.value = e;
      time.value = s;
      emits("dateChange", {
        start: startTime.value,
        end: endTime.value,
        dateType: tab.value,
        showText: t(curDict.value.showText)
      });
    };
    const nextTime = () => {
      const {
        startTimestamp: s,
        endTimestamp: e
      } = sub_index_page_lib_utils.getStartAndEndTimestamps(time.value, tab.value, 1);
      startTime.value = s;
      endTime.value = e;
      time.value = s;
      emits("dateChange", {
        start: startTime.value,
        end: endTime.value,
        dateType: tab.value,
        showText: t(curDict.value.showText)
      });
    };
    const bindDateChange = ({
      detail
    }) => {
      const {
        startTimestamp: s,
        endTimestamp: e
      } = sub_index_page_lib_utils.getStartAndEndTimestampsByPicker(tab.value, detail.value);
      if (s == startTime.value && e == endTime.value) {
        return;
      }
      startTime.value = s;
      endTime.value = e;
      time.value = s;
      emits("dateChange", {
        start: startTime.value,
        end: endTime.value,
        dateType: tab.value,
        showText: t(curDict.value.showText)
      });
    };
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.o(handleTabClick),
        b: common_vendor.o(($event) => tab.value = $event),
        c: common_vendor.p({
          modelKey: "key",
          tabs: tabListBind.value,
          modelValue: tab.value
        }),
        d: common_vendor.p({
          color: "#676767",
          name: "arrow-left",
          size: "22px"
        }),
        e: common_vendor.o(prevTime),
        f: curDict.value.dateType == "year"
      }, curDict.value.dateType == "year" ? {
        g: common_vendor.t(timeStr.value),
        h: curDict.value.uniPickerField,
        i: uniPickDate.value,
        j: common_vendor.o(bindDateChange)
      } : {
        k: common_vendor.t(timeStr.value),
        l: common_vendor.o(openCalendar)
      }, {
        m: common_vendor.p({
          color: "#676767",
          name: "arrow-right",
          size: "22px"
        }),
        n: common_vendor.o(nextTime),
        o: common_vendor.sr(wdCalendarRef, "882cf70d-3", {
          "k": "wdCalendarRef"
        }),
        p: common_vendor.o(handleConfirm),
        q: common_vendor.o(closeCalendar),
        r: common_vendor.o(($event) => calendar.value = $event),
        s: common_vendor.p({
          ["min-date"]: common_vendor.unref(sub_index_page_lib_utils.getYearStartTimestampOffset)(-1),
          ["max-date"]: (/* @__PURE__ */ new Date()).getTime(),
          type: curDict.value.calendarType,
          ["first-day-of-week"]: 1,
          ["z-index"]: 999,
          ["use-default-slot"]: true,
          modelValue: calendar.value
        })
      });
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-882cf70d"]]);
wx.createComponent(Component);
