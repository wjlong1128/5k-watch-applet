"use strict";
const common_vendor = require("../../../../common/vendor.js");
const _sfc_main = {
  __name: "DataList",
  props: {
    dataList: Array,
    unit: String
  },
  setup(__props) {
    return (_ctx, _cache) => {
      return {
        a: common_vendor.f(_ctx.$props.dataList, (item, index, i0) => {
          return common_vendor.e({
            a: !(item == null ? void 0 : item.hide)
          }, !(item == null ? void 0 : item.hide) ? common_vendor.e({
            b: common_vendor.t(item.value),
            c: item.showUnit || item.unit
          }, item.showUnit || item.unit ? {
            d: common_vendor.t((item == null ? void 0 : item.unit) || _ctx.$props.unit)
          } : {}, {
            e: common_vendor.t(item.text)
          }) : {}, {
            f: index
          });
        })
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-7b674098"]]);
wx.createComponent(Component);
