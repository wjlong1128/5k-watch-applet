"use strict";
const common_vendor = require("../../../../common/vendor.js");
const uni_modules_wotDesignUni_components_common_util = require("../common/util.js");
const uni_modules_wotDesignUni_components_composables_useChildren = require("../composables/useChildren.js");
const uni_modules_wotDesignUni_components_wdForm_types = require("./types.js");
const __default__ = {
  name: "wd-form",
  options: {
    addGlobalClass: true,
    virtualHost: true,
    styleIsolation: "shared"
  }
};
const _sfc_main = /* @__PURE__ */ common_vendor.defineComponent({
  ...__default__,
  props: uni_modules_wotDesignUni_components_wdForm_types.formProps,
  setup(__props, { expose: __expose }) {
    const props = __props;
    const { children, linkChildren } = uni_modules_wotDesignUni_components_composables_useChildren.useChildren(uni_modules_wotDesignUni_components_wdForm_types.FORM_KEY);
    let errorMessages = common_vendor.reactive({});
    linkChildren({ props, errorMessages });
    common_vendor.watch(
      () => props.model,
      () => {
        if (props.resetOnChange) {
          clearMessage();
        }
      },
      { immediate: true, deep: true }
    );
    async function validate(prop) {
      const errors = [];
      let valid = true;
      const promises = [];
      const formRules = getMergeRules();
      const rulesToValidate = prop ? { [prop]: formRules[prop] } : formRules;
      for (const prop2 in rulesToValidate) {
        const rules = rulesToValidate[prop2];
        const value = uni_modules_wotDesignUni_components_common_util.getPropByPath(props.model, prop2);
        if (rules && rules.length > 0) {
          for (const rule of rules) {
            if (rule.required && (!uni_modules_wotDesignUni_components_common_util.isDef(value) || value === "")) {
              errors.push({
                prop: prop2,
                message: rule.message
              });
              valid = false;
              break;
            }
            if (rule.pattern && !rule.pattern.test(value)) {
              errors.push({
                prop: prop2,
                message: rule.message
              });
              valid = false;
              break;
            }
            const { validator, ...ruleWithoutValidator } = rule;
            if (validator) {
              const result = validator(value, ruleWithoutValidator);
              if (uni_modules_wotDesignUni_components_common_util.isPromise(result)) {
                promises.push(
                  result.then((res) => {
                    if (typeof res === "string") {
                      errors.push({
                        prop: prop2,
                        message: res
                      });
                      valid = false;
                    } else if (typeof res === "boolean" && !res) {
                      errors.push({
                        prop: prop2,
                        message: rule.message
                      });
                      valid = false;
                    }
                  }).catch((error) => {
                    errors.push({
                      prop: prop2,
                      message: error || rule.message
                    });
                    valid = false;
                  })
                );
              } else {
                if (!result) {
                  errors.push({
                    prop: prop2,
                    message: rule.message
                  });
                  valid = false;
                }
              }
            }
          }
        }
      }
      await Promise.all(promises);
      errors.forEach((error) => {
        showMessage(error);
      });
      if (valid) {
        if (prop) {
          clearMessage(prop);
        } else {
          clearMessage();
        }
      }
      return {
        valid,
        errors
      };
    }
    function getMergeRules() {
      const mergedRules = uni_modules_wotDesignUni_components_common_util.deepClone(props.rules);
      children.forEach((item) => {
        if (uni_modules_wotDesignUni_components_common_util.isDef(item.prop) && uni_modules_wotDesignUni_components_common_util.isDef(item.rules) && item.rules.length) {
          if (mergedRules[item.prop]) {
            mergedRules[item.prop] = [...mergedRules[item.prop], ...item.rules];
          } else {
            mergedRules[item.prop] = item.rules;
          }
        }
      });
      return mergedRules;
    }
    function showMessage(errorMsg) {
      if (errorMsg.message) {
        errorMessages[errorMsg.prop] = errorMsg.message;
      }
    }
    function clearMessage(prop) {
      if (prop) {
        errorMessages[prop] = "";
      } else {
        Object.keys(errorMessages).forEach((key) => {
          errorMessages[key] = "";
        });
      }
    }
    function reset() {
      clearMessage();
    }
    __expose({ validate, reset });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.n(`wd-form ${_ctx.customClass}`),
        b: common_vendor.s(_ctx.customStyle)
      };
    };
  }
});
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-6504e7d0"]]);
wx.createComponent(Component);
