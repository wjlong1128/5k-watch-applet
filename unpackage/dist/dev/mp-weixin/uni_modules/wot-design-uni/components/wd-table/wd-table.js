"use strict";
const common_vendor = require("../../../../common/vendor.js");
const uni_modules_wotDesignUni_components_common_util = require("../common/util.js");
const uni_modules_wotDesignUni_components_wdTable_types = require("./types.js");
const uni_modules_wotDesignUni_components_composables_useTranslate = require("../composables/useTranslate.js");
if (!Array) {
  const _easycom_wd_sort_button2 = common_vendor.resolveComponent("wd-sort-button");
  _easycom_wd_sort_button2();
}
const _easycom_wd_sort_button = () => "../wd-sort-button/wd-sort-button.js";
if (!Math) {
  (_easycom_wd_sort_button + WdTableCol)();
}
const WdTableCol = () => "../wd-table-col/wd-table-col.js";
const __default__ = {
  name: "wd-table",
  options: {
    addGlobalClass: true,
    virtualHost: true,
    styleIsolation: "shared"
  }
};
const _sfc_main = /* @__PURE__ */ common_vendor.defineComponent({
  ...__default__,
  props: uni_modules_wotDesignUni_components_wdTable_types.tableProps,
  emits: ["sort-method", "row-click"],
  setup(__props, { emit: __emit }) {
    const { translate } = uni_modules_wotDesignUni_components_composables_useTranslate.useTranslate("tableCol");
    const props = __props;
    const emit = __emit;
    const reactiveState = common_vendor.reactive({
      data: props.data,
      stripe: props.stripe,
      border: props.border,
      height: props.height,
      rowHeight: props.rowHeight,
      showHeader: props.showHeader,
      ellipsis: props.ellipsis,
      scrollLeft: 0,
      columns: [],
      setRowClick,
      setColumns
    });
    const indexUUID = uni_modules_wotDesignUni_components_common_util.uuid();
    const indexColumn = common_vendor.ref({
      prop: indexUUID,
      label: translate("indexLabel"),
      width: "100rpx",
      sortable: false,
      fixed: false,
      align: "left",
      ...uni_modules_wotDesignUni_components_common_util.isObj(props.index) ? props.index : {}
    });
    const scroll = uni_modules_wotDesignUni_components_common_util.debounce(handleScroll, 100, { leading: false });
    common_vendor.provide("wdTable", reactiveState);
    common_vendor.watch(
      () => props.data,
      (newValue) => {
        reactiveState.data = newValue;
      },
      { deep: true }
    );
    common_vendor.watch(
      () => props.stripe,
      (newValue) => {
        reactiveState.stripe = newValue;
      },
      { deep: true }
    );
    common_vendor.watch(
      () => props.border,
      (newValue) => {
        reactiveState.border = newValue;
      },
      { deep: true }
    );
    common_vendor.watch(
      () => props.height,
      (newValue) => {
        reactiveState.height = newValue;
      },
      { deep: true }
    );
    common_vendor.watch(
      () => props.rowHeight,
      (newValue) => {
        reactiveState.rowHeight = newValue;
      },
      { deep: true }
    );
    common_vendor.watch(
      () => props.showHeader,
      (newValue) => {
        reactiveState.showHeader = newValue;
      },
      { deep: true }
    );
    common_vendor.watch(
      () => props.ellipsis,
      (newValue) => {
        reactiveState.ellipsis = newValue;
      },
      { deep: true }
    );
    const tableStyle = common_vendor.computed(() => {
      const style = {};
      if (uni_modules_wotDesignUni_components_common_util.isDef(props.height)) {
        style["max-height"] = uni_modules_wotDesignUni_components_common_util.addUnit(props.height);
      }
      return `${uni_modules_wotDesignUni_components_common_util.objToStyle(style)};${props.customStyle}`;
    });
    const realWidthStyle = common_vendor.computed(() => {
      const style = {
        display: "flex"
      };
      let width = "";
      reactiveState.columns.forEach((column) => {
        width = width ? `${width} + ${uni_modules_wotDesignUni_components_common_util.addUnit(column.width)}` : uni_modules_wotDesignUni_components_common_util.addUnit(column.width);
      });
      style["width"] = `calc(${width})`;
      return uni_modules_wotDesignUni_components_common_util.objToStyle(style);
    });
    const bodyStyle = common_vendor.computed(() => {
      const style = {};
      if (uni_modules_wotDesignUni_components_common_util.isDef(props.height)) {
        style["height"] = uni_modules_wotDesignUni_components_common_util.isDef(props.rowHeight) ? `calc(${props.data.length} * ${uni_modules_wotDesignUni_components_common_util.addUnit(props.rowHeight)})` : `calc(${props.data.length} * 50px)`;
      }
      return `${uni_modules_wotDesignUni_components_common_util.objToStyle(style)};`;
    });
    function isLastFixed(column) {
      let isLastFixed2 = false;
      if (column.fixed && uni_modules_wotDesignUni_components_common_util.isDef(reactiveState.columns)) {
        const columns = reactiveState.columns.filter((column2) => {
          return column2.fixed;
        });
        if (columns.length && columns[columns.length - 1].prop === column.prop) {
          isLastFixed2 = true;
        }
      }
      return isLastFixed2;
    }
    function setColumns(column) {
      if (column.prop === indexUUID) {
        reactiveState.columns = uni_modules_wotDesignUni_components_common_util.deepClone([column, ...reactiveState.columns]);
      } else {
        reactiveState.columns = uni_modules_wotDesignUni_components_common_util.deepClone([...reactiveState.columns, column]);
      }
    }
    function headerCellStyle(columnIndex) {
      const style = {};
      if (uni_modules_wotDesignUni_components_common_util.isDef(reactiveState.columns[columnIndex].width)) {
        style["width"] = uni_modules_wotDesignUni_components_common_util.addUnit(reactiveState.columns[columnIndex].width);
      }
      if (reactiveState.columns[columnIndex].fixed) {
        if (columnIndex > 0) {
          let left = "";
          reactiveState.columns.forEach((column, index) => {
            if (index < columnIndex) {
              left = left ? `${left} + ${uni_modules_wotDesignUni_components_common_util.addUnit(column.width)}` : uni_modules_wotDesignUni_components_common_util.addUnit(column.width);
            }
          });
          style["left"] = `calc(${left})`;
        } else {
          style["left"] = 0;
        }
      }
      return uni_modules_wotDesignUni_components_common_util.objToStyle(style);
    }
    function handleSortChange(value, index) {
      reactiveState.columns[index].sortDirection = value;
      reactiveState.columns.forEach((col, i) => {
        if (index != i) {
          col.sortDirection = 0;
        }
      });
      emit("sort-method", reactiveState.columns[index]);
    }
    function handleScroll(event) {
      if (!props.showHeader) {
        return;
      }
      reactiveState.scrollLeft = event.detail.scrollLeft;
    }
    function setRowClick(index) {
      emit("row-click", { rowIndex: index });
    }
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: _ctx.showHeader
      }, _ctx.showHeader ? {
        b: common_vendor.f(reactiveState.columns, (column, index, i0) => {
          return common_vendor.e({
            a: column.sortable
          }, column.sortable ? {
            b: common_vendor.o(({
              value
            }) => handleSortChange(value, index), index),
            c: "323200c2-0-" + i0,
            d: common_vendor.o(($event) => column.sortDirection = $event, index),
            e: common_vendor.p({
              ["allow-reset"]: true,
              line: false,
              title: column.label,
              modelValue: column.sortDirection
            })
          } : {
            f: common_vendor.t(column.label),
            g: common_vendor.n(`wd-table__value ${_ctx.ellipsis ? "is-ellipsis" : ""}`)
          }, {
            h: common_vendor.n(`wd-table__cell ${_ctx.border ? "is-border" : ""} ${column.fixed ? "is-fixed" : ""} ${_ctx.stripe ? "is-stripe" : ""} is-${column.align} ${isLastFixed(column) && reactiveState.scrollLeft ? "is-shadow" : ""}`),
            i: common_vendor.s(headerCellStyle(index)),
            j: index
          });
        }),
        c: common_vendor.s(realWidthStyle.value),
        d: reactiveState.scrollLeft,
        e: common_vendor.o(
          //@ts-ignore
          (...args) => common_vendor.unref(scroll) && common_vendor.unref(scroll)(...args)
        )
      } : {}, {
        f: _ctx.index !== false
      }, _ctx.index !== false ? {
        g: common_vendor.w(({
          index
        }, s0, i0) => {
          return {
            a: common_vendor.t(index + 1),
            b: i0,
            c: s0
          };
        }, {
          name: "value",
          path: "g",
          vueId: "323200c2-1"
        }),
        h: common_vendor.p({
          prop: indexColumn.value.prop,
          label: indexColumn.value.label,
          width: indexColumn.value.width,
          sortable: indexColumn.value.sortable,
          fixed: indexColumn.value.fixed,
          align: indexColumn.value.align
        })
      } : {}, {
        i: common_vendor.s(realWidthStyle.value),
        j: common_vendor.s(bodyStyle.value),
        k: common_vendor.o(
          //@ts-ignore
          (...args) => common_vendor.unref(scroll) && common_vendor.unref(scroll)(...args)
        ),
        l: reactiveState.scrollLeft,
        m: common_vendor.n(`wd-table ${_ctx.border ? "is-border" : ""} ${_ctx.customClass}`),
        n: common_vendor.s(tableStyle.value)
      });
    };
  }
});
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-323200c2"]]);
wx.createComponent(Component);
