"use strict";
const common_vendor = require("../../../../common/vendor.js");
const uni_modules_wotDesignUni_components_common_util = require("../common/util.js");
const uni_modules_wotDesignUni_components_composables_useParent = require("../composables/useParent.js");
const uni_modules_wotDesignUni_components_wdTabs_types = require("../wd-tabs/types.js");
const uni_modules_wotDesignUni_components_wdTab_types = require("./types.js");
const __default__ = {
  name: "wd-tab",
  options: {
    addGlobalClass: true,
    virtualHost: true,
    styleIsolation: "shared"
  }
};
const _sfc_main = /* @__PURE__ */ common_vendor.defineComponent({
  ...__default__,
  props: uni_modules_wotDesignUni_components_wdTab_types.tabProps,
  setup(__props, { expose: __expose }) {
    const props = __props;
    const painted = common_vendor.ref(false);
    const isShow = common_vendor.ref(false);
    const { proxy } = common_vendor.getCurrentInstance();
    const { parent: tabs, index } = uni_modules_wotDesignUni_components_composables_useParent.useParent(uni_modules_wotDesignUni_components_wdTabs_types.TABS_KEY);
    const activeIndex = common_vendor.computed(() => {
      return uni_modules_wotDesignUni_components_common_util.isDef(tabs) ? tabs.state.activeIndex : 0;
    });
    common_vendor.watch(
      () => props.name,
      (newValue) => {
        if (uni_modules_wotDesignUni_components_common_util.isDef(newValue) && !uni_modules_wotDesignUni_components_common_util.isNumber(newValue) && !uni_modules_wotDesignUni_components_common_util.isString(newValue)) {
          console.error("[wot design] error(wd-tab): the type of name should be number or string");
          return;
        }
        if (tabs) {
          checkName(proxy);
        }
      },
      {
        deep: true,
        immediate: true
      }
    );
    common_vendor.watch(
      () => activeIndex.value,
      (newValue) => {
        if (newValue === index.value) {
          setShow(true, true);
        } else {
          setShow(painted.value, false);
        }
      },
      { deep: true, immediate: true }
    );
    function checkName(self) {
      const { name: myName } = props;
      if (myName === void 0 || myName === null || myName === "") {
        return;
      }
      tabs && tabs.children.forEach((child) => {
        if (child.$.uid !== self.$.uid && child.name === myName) {
          console.error(`The tab's bound value: ${myName} has been used`);
        }
      });
    }
    function setShow(setPainted, setIsShow) {
      painted.value = setPainted;
      isShow.value = setIsShow;
    }
    __expose({
      setShow,
      painted
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: painted.value
      }, painted.value ? {
        b: common_vendor.s(isShow.value ? "" : "display: none;")
      } : {}, {
        c: common_vendor.n(`wd-tab ${_ctx.customClass}`),
        d: common_vendor.s(_ctx.customStyle)
      });
    };
  }
});
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-0ac60957"]]);
wx.createComponent(Component);
