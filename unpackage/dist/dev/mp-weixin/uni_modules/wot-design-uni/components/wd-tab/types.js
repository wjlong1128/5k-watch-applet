"use strict";
const uni_modules_wotDesignUni_components_common_props = require("../common/props.js");
const tabProps = {
  ...uni_modules_wotDesignUni_components_common_props.baseProps,
  /**
   * 唯一标识符
   */
  name: uni_modules_wotDesignUni_components_common_props.numericProp,
  /**
   * tab的标题
   */
  title: String,
  /**
   *  是否禁用，无法点击
   */
  disabled: uni_modules_wotDesignUni_components_common_props.makeBooleanProp(false)
};
exports.tabProps = tabProps;
