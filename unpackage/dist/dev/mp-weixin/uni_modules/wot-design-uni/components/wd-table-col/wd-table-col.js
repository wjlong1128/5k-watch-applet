"use strict";
const common_vendor = require("../../../../common/vendor.js");
const uni_modules_wotDesignUni_components_common_util = require("../common/util.js");
const uni_modules_wotDesignUni_components_wdTableCol_types = require("./types.js");
const __default__ = {
  name: "wd-table-col",
  options: {
    addGlobalClass: true,
    virtualHost: true,
    styleIsolation: "shared"
  }
};
const _sfc_main = /* @__PURE__ */ common_vendor.defineComponent({
  ...__default__,
  props: uni_modules_wotDesignUni_components_wdTableCol_types.tableColumnProps,
  setup(__props) {
    const props = __props;
    const parent = common_vendor.inject("wdTable", { data: [] });
    const stripe = common_vendor.computed(() => {
      return parent.stripe || false;
    });
    const border = common_vendor.computed(() => {
      return parent.border || false;
    });
    const ellipsis = common_vendor.computed(() => {
      return parent.ellipsis || false;
    });
    const isLastFixed = common_vendor.computed(() => {
      let isLastFixed2 = false;
      if (props.fixed && uni_modules_wotDesignUni_components_common_util.isDef(parent.columns)) {
        const columns = parent.columns.filter((column2) => {
          return column2.fixed;
        });
        if (columns.length && columns[columns.length - 1].prop === props.prop) {
          isLastFixed2 = true;
        }
      }
      return isLastFixed2;
    });
    const columnStyle = common_vendor.computed(() => {
      const style = {};
      if (uni_modules_wotDesignUni_components_common_util.isDef(props.width)) {
        style["width"] = uni_modules_wotDesignUni_components_common_util.addUnit(props.width);
      }
      if (props.fixed) {
        const columnIndex = parent.columns.findIndex((column2) => {
          return column2.prop === props.prop;
        });
        if (columnIndex > 0) {
          let left = "";
          parent.columns.forEach((column2, index) => {
            if (index < columnIndex) {
              left = left ? `${left} + ${uni_modules_wotDesignUni_components_common_util.addUnit(column2.width)}` : uni_modules_wotDesignUni_components_common_util.addUnit(column2.width);
            }
          });
          style["left"] = `calc(${left})`;
        } else {
          style["left"] = 0;
        }
      }
      return uni_modules_wotDesignUni_components_common_util.objToStyle(style);
    });
    const cellStyle = common_vendor.computed(() => {
      const rowHeight = parent.rowHeight || "80rpx";
      const style = {};
      if (uni_modules_wotDesignUni_components_common_util.isDef(rowHeight)) {
        style["height"] = uni_modules_wotDesignUni_components_common_util.addUnit(rowHeight);
      }
      if (props.fixed) {
        const columnIndex = parent.columns.findIndex((column2) => {
          return column2.prop === props.prop;
        });
        if (columnIndex > 0) {
          let left = "";
          parent.columns.forEach((column2, index) => {
            if (index < columnIndex) {
              left = left ? `${left} + ${uni_modules_wotDesignUni_components_common_util.addUnit(column2.width)}` : uni_modules_wotDesignUni_components_common_util.addUnit(column2.width);
            }
          });
          style["left"] = `calc(${left})`;
        } else {
          style["left"] = 0;
        }
      }
      return uni_modules_wotDesignUni_components_common_util.objToStyle(style);
    });
    const scope = common_vendor.computed(() => {
      return (index) => {
        return parent.data[index] || {};
      };
    });
    const column = common_vendor.computed(() => {
      let column2 = parent.data.map((item) => {
        return item[props.prop];
      });
      return column2;
    });
    common_vendor.onMounted(() => {
      parent.setColumns && parent.setColumns({
        prop: props.prop,
        label: props.label,
        width: props.width,
        sortable: props.sortable,
        fixed: props.fixed,
        align: props.align,
        sortDirection: 0
      });
    });
    function handleRowClick(index) {
      parent.setRowClick && parent.setRowClick(index);
    }
    return (_ctx, _cache) => {
      return {
        a: common_vendor.f(column.value, (row, index, i0) => {
          return common_vendor.e(_ctx.$slots.value ? {
            a: "value-" + i0,
            b: common_vendor.r("value", {
              row: scope.value(index),
              index
            }, i0)
          } : {
            c: common_vendor.t(row),
            d: common_vendor.n(`wd-table__value ${ellipsis.value ? "is-ellipsis" : ""}`)
          }, {
            e: common_vendor.n(`wd-table__cell ${stripe.value && common_vendor.unref(uni_modules_wotDesignUni_components_common_util.isOdd)(index) ? "is-stripe" : ""} ${border.value ? "is-border" : ""} is-${_ctx.align}`),
            f: index,
            g: common_vendor.o(($event) => handleRowClick(index), index)
          });
        }),
        b: _ctx.$slots.value,
        c: common_vendor.s(cellStyle.value),
        d: common_vendor.n(`wd-table-col ${_ctx.fixed ? "wd-table-col--fixed" : ""} ${isLastFixed.value && common_vendor.unref(uni_modules_wotDesignUni_components_common_util.isDef)(common_vendor.unref(parent).scrollLeft) && common_vendor.unref(parent).scrollLeft ? "is-shadow" : ""}`),
        e: common_vendor.s(columnStyle.value)
      };
    };
  }
});
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-00c812c4"]]);
wx.createComponent(Component);
