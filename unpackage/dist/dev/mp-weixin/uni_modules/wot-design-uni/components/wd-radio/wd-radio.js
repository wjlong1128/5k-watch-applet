"use strict";
const common_vendor = require("../../../../common/vendor.js");
const uni_modules_wotDesignUni_components_composables_useParent = require("../composables/useParent.js");
const uni_modules_wotDesignUni_components_wdRadioGroup_types = require("../wd-radio-group/types.js");
const uni_modules_wotDesignUni_components_wdRadio_types = require("./types.js");
if (!Array) {
  const _easycom_wd_icon2 = common_vendor.resolveComponent("wd-icon");
  _easycom_wd_icon2();
}
const _easycom_wd_icon = () => "../wd-icon/wd-icon.js";
if (!Math) {
  _easycom_wd_icon();
}
const __default__ = {
  name: "wd-radio",
  options: {
    virtualHost: true,
    addGlobalClass: true,
    styleIsolation: "shared"
  }
};
const _sfc_main = /* @__PURE__ */ common_vendor.defineComponent({
  ...__default__,
  props: uni_modules_wotDesignUni_components_wdRadio_types.radioProps,
  setup(__props) {
    const props = __props;
    const { parent: radioGroup } = uni_modules_wotDesignUni_components_composables_useParent.useParent(uni_modules_wotDesignUni_components_wdRadioGroup_types.RADIO_GROUP_KEY);
    const isChecked = common_vendor.computed(() => {
      if (radioGroup) {
        return props.value === radioGroup.props.modelValue;
      } else {
        return false;
      }
    });
    const innerShape = common_vendor.computed(() => {
      if (!props.shape && radioGroup && radioGroup.props.shape) {
        return radioGroup.props.shape;
      } else {
        return props.shape;
      }
    });
    const innerCheckedColor = common_vendor.computed(() => {
      if (!props.checkedColor && radioGroup && radioGroup.props.checkedColor) {
        return radioGroup.props.checkedColor;
      } else {
        return props.checkedColor;
      }
    });
    const innerDisabled = common_vendor.computed(() => {
      if ((props.disabled === null || props.disabled === void 0) && radioGroup && radioGroup.props.disabled) {
        return radioGroup.props.disabled;
      } else {
        return props.disabled;
      }
    });
    const innerInline = common_vendor.computed(() => {
      if ((props.inline === null || props.inline === void 0) && radioGroup && radioGroup.props.inline) {
        return radioGroup.props.inline;
      } else {
        return props.inline;
      }
    });
    const innerSize = common_vendor.computed(() => {
      if (!props.size && radioGroup && radioGroup.props.size) {
        return radioGroup.props.size;
      } else {
        return props.size;
      }
    });
    const innerCell = common_vendor.computed(() => {
      if ((props.cell === null || props.cell === void 0) && radioGroup && radioGroup.props.cell) {
        return radioGroup.props.cell;
      } else {
        return props.cell;
      }
    });
    common_vendor.watch(
      () => props.shape,
      (newValue) => {
        const type = ["check", "dot", "button"];
        if (!newValue || type.indexOf(newValue) === -1)
          console.error(`shape must be one of ${type.toString()}`);
      }
    );
    function handleClick() {
      const { value } = props;
      if (!innerDisabled.value && radioGroup && value !== null && value !== void 0) {
        radioGroup.updateValue(value);
      }
    }
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.s(`${_ctx.maxWidth ? "max-width:" + _ctx.maxWidth : ""};  ${isChecked.value && innerShape.value === "button" && !innerDisabled.value ? "color :" + innerCheckedColor.value : ""}`),
        b: innerShape.value === "check"
      }, innerShape.value === "check" ? {
        c: common_vendor.s(isChecked.value && !_ctx.disabled ? "color: " + innerCheckedColor.value : ""),
        d: common_vendor.p({
          name: "check"
        })
      } : {}, {
        e: common_vendor.s(isChecked.value && !_ctx.disabled ? "color: " + innerCheckedColor.value : ""),
        f: common_vendor.n(`wd-radio ${innerCell.value ? "is-cell-radio" : ""} ${innerCell.value && innerShape.value == "button" ? "is-button-radio" : ""} ${innerSize.value ? "is-" + innerSize.value : ""} ${innerInline.value ? "is-inline" : ""} ${isChecked.value ? "is-checked" : ""} ${innerShape.value !== "check" ? "is-" + innerShape.value : ""} ${innerDisabled.value ? "is-disabled" : ""} ${_ctx.customClass}`),
        g: common_vendor.s(_ctx.customStyle),
        h: common_vendor.o(handleClick)
      });
    };
  }
});
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-a54631cc"]]);
wx.createComponent(Component);
