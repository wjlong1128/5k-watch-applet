"use strict";
const common_vendor = require("../../../../common/vendor.js");
const uni_modules_wotDesignUni_components_common_util = require("../common/util.js");
const uni_modules_wotDesignUni_components_composables_useTouch = require("../composables/useTouch.js");
const uni_modules_wotDesignUni_components_wdTabs_types = require("./types.js");
const uni_modules_wotDesignUni_components_composables_useChildren = require("../composables/useChildren.js");
const uni_modules_wotDesignUni_components_composables_useTranslate = require("../composables/useTranslate.js");
if (!Array) {
  const _easycom_wd_icon2 = common_vendor.resolveComponent("wd-icon");
  const _easycom_wd_sticky2 = common_vendor.resolveComponent("wd-sticky");
  const _easycom_wd_sticky_box2 = common_vendor.resolveComponent("wd-sticky-box");
  (_easycom_wd_icon2 + _easycom_wd_sticky2 + _easycom_wd_sticky_box2)();
}
const _easycom_wd_icon = () => "../wd-icon/wd-icon.js";
const _easycom_wd_sticky = () => "../wd-sticky/wd-sticky.js";
const _easycom_wd_sticky_box = () => "../wd-sticky-box/wd-sticky-box.js";
if (!Math) {
  (_easycom_wd_icon + _easycom_wd_sticky + _easycom_wd_sticky_box)();
}
const __default__ = {
  name: "wd-tabs",
  options: {
    addGlobalClass: true,
    virtualHost: true,
    styleIsolation: "shared"
  }
};
const _sfc_main = /* @__PURE__ */ common_vendor.defineComponent({
  ...__default__,
  props: uni_modules_wotDesignUni_components_wdTabs_types.tabsProps,
  emits: ["change", "disabled", "click", "update:modelValue"],
  setup(__props, { expose: __expose, emit: __emit }) {
    const $item = ".wd-tabs__nav-item";
    const $container = ".wd-tabs__nav-container";
    const props = __props;
    const emit = __emit;
    const { translate } = uni_modules_wotDesignUni_components_composables_useTranslate.useTranslate("tabs");
    const state = common_vendor.reactive({ activeIndex: 0 });
    const lineStyle = common_vendor.ref("");
    const mapShow = common_vendor.ref(false);
    const scrollLeft = common_vendor.ref(0);
    const animating = common_vendor.ref(false);
    const inited = common_vendor.ref(false);
    const { children, linkChildren } = uni_modules_wotDesignUni_components_composables_useChildren.useChildren(uni_modules_wotDesignUni_components_wdTabs_types.TABS_KEY);
    linkChildren({ state });
    const { proxy } = common_vendor.getCurrentInstance();
    const touch = uni_modules_wotDesignUni_components_composables_useTouch.useTouch();
    const items = common_vendor.computed(() => {
      return children.map((child, index) => {
        return { disabled: child.disabled, title: child.title, name: uni_modules_wotDesignUni_components_common_util.isDef(child.name) ? child.name : index };
      });
    });
    const bodyStyle = common_vendor.computed(() => {
      if (!props.animated) {
        return "";
      }
      return uni_modules_wotDesignUni_components_common_util.objToStyle({
        left: -100 * state.activeIndex + "%",
        "transition-duration": props.duration + "ms",
        "-webkit-transition-duration": props.duration + "ms"
      });
    });
    const setActive = uni_modules_wotDesignUni_components_common_util.debounce(
      function(value = 0, init = false, setScroll = true) {
        if (items.value.length === 0)
          return;
        value = getActiveIndex(value);
        if (items.value[value].disabled)
          return;
        state.activeIndex = value;
        if (setScroll) {
          updateLineStyle(init === false);
          scrollIntoView();
        }
        setActiveTab();
      },
      100,
      { leading: false }
    );
    common_vendor.watch(
      () => props.modelValue,
      (newValue) => {
        if (!uni_modules_wotDesignUni_components_common_util.isNumber(newValue) && !uni_modules_wotDesignUni_components_common_util.isString(newValue)) {
          console.error("[wot design] error(wd-tabs): the type of value should be number or string");
        }
        if (newValue === "" || !uni_modules_wotDesignUni_components_common_util.isDef(newValue)) {
          console.error("[wot design] error(wd-tabs): tabs's value cannot be '' null or undefined");
        }
        if (typeof newValue === "number" && newValue < 0) {
          console.error("[wot design] error(wd-tabs): tabs's value cannot be less than zero");
        }
      },
      {
        immediate: true,
        deep: true
      }
    );
    common_vendor.watch(
      () => props.modelValue,
      (newValue) => {
        const index = getActiveIndex(newValue);
        setActive(newValue, false, index !== state.activeIndex);
      },
      {
        immediate: false,
        deep: true
      }
    );
    common_vendor.watch(
      () => children.length,
      () => {
        if (inited.value) {
          common_vendor.nextTick$1(() => {
            setActive(props.modelValue);
          });
        }
      }
    );
    common_vendor.watch(
      () => props.slidableNum,
      (newValue) => {
        uni_modules_wotDesignUni_components_common_util.checkNumRange(newValue, "slidableNum");
      }
    );
    common_vendor.watch(
      () => props.mapNum,
      (newValue) => {
        uni_modules_wotDesignUni_components_common_util.checkNumRange(newValue, "mapNum");
      }
    );
    common_vendor.onMounted(() => {
      inited.value = true;
      common_vendor.nextTick$1(() => {
        setActive(props.modelValue, true);
      });
    });
    function toggleMap() {
      if (mapShow.value) {
        animating.value = false;
        setTimeout(() => {
          mapShow.value = false;
        }, 300);
      } else {
        mapShow.value = true;
        setTimeout(() => {
          animating.value = true;
        }, 100);
      }
    }
    function updateLineStyle(animation = true) {
      if (!inited.value)
        return;
      const { lineWidth, lineHeight } = props;
      uni_modules_wotDesignUni_components_common_util.getRect($item, true, proxy).then((rects) => {
        const rect = rects[state.activeIndex];
        const width = lineWidth;
        let left = rects.slice(0, state.activeIndex).reduce((prev, curr) => prev + Number(curr.width), 0);
        left += (Number(rect.width) - width) / 2;
        const transition = animation ? "transition: width 300ms ease, transform 300ms ease;" : "";
        const lineStyleTemp = `
            height: ${lineHeight}px;
            width: ${width}px;
            transform: translateX(${left}px);
            ${transition}
          `;
        if (lineStyle.value !== lineStyleTemp) {
          lineStyle.value = lineStyleTemp;
        }
      });
    }
    function setActiveTab() {
      if (!inited.value)
        return;
      if (items.value[state.activeIndex].name !== props.modelValue) {
        emit("change", {
          index: state.activeIndex,
          name: items.value[state.activeIndex].name
        });
        emit("update:modelValue", items.value[state.activeIndex].name);
      }
    }
    function scrollIntoView() {
      if (!inited.value)
        return;
      Promise.all([uni_modules_wotDesignUni_components_common_util.getRect($item, true, proxy), uni_modules_wotDesignUni_components_common_util.getRect($container, false, proxy)]).then(([navItemsRects, navRect]) => {
        const selectItem = navItemsRects[state.activeIndex];
        const offsetLeft = navItemsRects.slice(0, state.activeIndex).reduce((prev, curr) => prev + curr.width, 0);
        const left = offsetLeft - (navRect.width - Number(selectItem.width)) / 2;
        if (left === scrollLeft.value) {
          scrollLeft.value = left + Math.random() / 1e4;
        } else {
          scrollLeft.value = left;
        }
      });
    }
    function handleSelect(index) {
      if (index === void 0)
        return;
      const { name, disabled } = items.value[index];
      if (disabled) {
        emit("disabled", {
          index,
          name
        });
        return;
      }
      mapShow.value && toggleMap();
      setActive(index);
      emit("click", {
        index,
        name
      });
    }
    function onTouchStart(event) {
      if (!props.swipeable)
        return;
      touch.touchStart(event);
    }
    function onTouchMove(event) {
      if (!props.swipeable)
        return;
      touch.touchMove(event);
    }
    function onTouchEnd() {
      if (!props.swipeable)
        return;
      const { direction, deltaX, offsetX } = touch;
      const minSwipeDistance = 50;
      if (direction.value === "horizontal" && offsetX.value >= minSwipeDistance) {
        if (deltaX.value > 0 && state.activeIndex !== 0) {
          setActive(state.activeIndex - 1);
        } else if (deltaX.value < 0 && state.activeIndex !== items.value.length - 1) {
          setActive(state.activeIndex + 1);
          setActive(state.activeIndex + 1);
        }
      }
    }
    function getActiveIndex(value) {
      if (uni_modules_wotDesignUni_components_common_util.isNumber(value) && value >= items.value.length) {
        console.error("[wot design] warning(wd-tabs): the type of tabs' value is Number shouldn't be less than its children");
        value = 0;
      }
      if (uni_modules_wotDesignUni_components_common_util.isString(value)) {
        const index = items.value.findIndex((item) => item.name === value);
        value = index === -1 ? 0 : index;
      }
      return value;
    }
    __expose({
      setActive,
      scrollIntoView,
      updateLineStyle,
      children
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: _ctx.sticky
      }, _ctx.sticky ? common_vendor.e({
        b: common_vendor.f(items.value, (item, index, i0) => {
          return {
            a: common_vendor.t(item.title),
            b: common_vendor.o(($event) => handleSelect(index), index),
            c: index,
            d: common_vendor.n(`wd-tabs__nav-item  ${state.activeIndex === index ? "is-active" : ""} ${item.disabled ? "is-disabled" : ""}`),
            e: common_vendor.s(state.activeIndex === index ? _ctx.color ? "color:" + _ctx.color : "" : _ctx.inactiveColor ? "color:" + _ctx.inactiveColor : "")
          };
        }),
        c: common_vendor.s(lineStyle.value),
        d: _ctx.slidableNum < items.value.length,
        e: scrollLeft.value,
        f: _ctx.mapNum < items.value.length && _ctx.mapNum !== 0
      }, _ctx.mapNum < items.value.length && _ctx.mapNum !== 0 ? {
        g: common_vendor.p({
          name: "arrow-down"
        }),
        h: common_vendor.n(`wd-tabs__map-arrow  ${animating.value ? "is-open" : ""}`),
        i: common_vendor.n(`wd-tabs__map-btn  ${animating.value ? "is-open" : ""}`),
        j: common_vendor.o(toggleMap),
        k: common_vendor.t(common_vendor.unref(translate)("all")),
        l: common_vendor.s(`${mapShow.value ? "" : "display:none;"}  ${animating.value ? "opacity:1;" : ""}`),
        m: common_vendor.f(items.value, (item, index, i0) => {
          return {
            a: common_vendor.t(item.title),
            b: common_vendor.n(`wd-tabs__map-nav-btn ${state.activeIndex === index ? "is-active" : ""}  ${item.disabled ? "is-disabled" : ""}`),
            c: common_vendor.s(state.activeIndex === index ? _ctx.color ? "color:" + _ctx.color + ";border-color:" + _ctx.color : "" : _ctx.inactiveColor ? "color:" + _ctx.inactiveColor : ""),
            d: index,
            e: common_vendor.o(($event) => handleSelect(index), index)
          };
        }),
        n: common_vendor.n(`wd-tabs__map-body  ${animating.value ? "is-open" : ""}`),
        o: common_vendor.s(mapShow.value ? "" : "display:none")
      } : {}, {
        p: common_vendor.p({
          ["offset-top"]: _ctx.offsetTop
        }),
        q: common_vendor.n(_ctx.animated ? "is-animated" : ""),
        r: common_vendor.s(bodyStyle.value),
        s: common_vendor.o(onTouchStart),
        t: common_vendor.o(onTouchMove),
        v: common_vendor.o(onTouchEnd),
        w: common_vendor.o(onTouchEnd),
        x: common_vendor.s(`${mapShow.value ? "" : "display:none;"} ${animating.value ? "opacity:1;" : ""}`),
        y: common_vendor.o(toggleMap),
        z: common_vendor.n(`wd-tabs ${_ctx.customClass} ${_ctx.slidableNum < items.value.length ? "is-slide" : ""} ${_ctx.mapNum < items.value.length && _ctx.mapNum !== 0 ? "is-map" : ""}`),
        A: common_vendor.s(_ctx.customStyle)
      }) : common_vendor.e({
        B: common_vendor.f(items.value, (item, index, i0) => {
          return {
            a: common_vendor.t(item.title),
            b: common_vendor.o(($event) => handleSelect(index), index),
            c: index,
            d: common_vendor.n(`wd-tabs__nav-item ${state.activeIndex === index ? "is-active" : ""} ${item.disabled ? "is-disabled" : ""}`),
            e: common_vendor.s(state.activeIndex === index ? _ctx.color ? "color:" + _ctx.color : "" : _ctx.inactiveColor ? "color:" + _ctx.inactiveColor : "")
          };
        }),
        C: common_vendor.s(lineStyle.value),
        D: _ctx.slidableNum < items.value.length,
        E: scrollLeft.value,
        F: _ctx.mapNum < items.value.length && _ctx.mapNum !== 0
      }, _ctx.mapNum < items.value.length && _ctx.mapNum !== 0 ? {
        G: common_vendor.p({
          name: "arrow-down"
        }),
        H: common_vendor.n(`wd-tabs__map-arrow ${animating.value ? "is-open" : ""}`),
        I: common_vendor.o(toggleMap),
        J: common_vendor.t(common_vendor.unref(translate)("all")),
        K: common_vendor.s(`${mapShow.value ? "" : "display:none;"}  ${animating.value ? "opacity:1;" : ""}`),
        L: common_vendor.f(items.value, (item, index, i0) => {
          return {
            a: common_vendor.t(item.title),
            b: common_vendor.n(`wd-tabs__map-nav-btn ${state.activeIndex === index ? "is-active" : ""}  ${item.disabled ? "is-disabled" : ""}`),
            c: index,
            d: common_vendor.o(($event) => handleSelect(index), index)
          };
        }),
        M: common_vendor.n(`wd-tabs__map-body ${animating.value ? "is-open" : ""}`),
        N: common_vendor.s(mapShow.value ? "" : "display:none")
      } : {}, {
        O: common_vendor.n(_ctx.animated ? "is-animated" : ""),
        P: common_vendor.s(bodyStyle.value),
        Q: common_vendor.o(onTouchStart),
        R: common_vendor.o(onTouchMove),
        S: common_vendor.o(onTouchEnd),
        T: common_vendor.o(onTouchEnd),
        U: common_vendor.s(`${mapShow.value ? "" : "display:none;"}  ${animating.value ? "opacity:1" : ""}`),
        V: common_vendor.o(toggleMap),
        W: common_vendor.n(`wd-tabs  ${_ctx.customClass} ${_ctx.slidableNum < items.value.length ? "is-slide" : ""} ${_ctx.mapNum < items.value.length && _ctx.mapNum !== 0 ? "is-map" : ""}`)
      }));
    };
  }
});
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-4388d15d"]]);
wx.createComponent(Component);
