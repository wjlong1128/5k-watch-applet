"use strict";
const common_vendor = require("../../../../common/vendor.js");
const uni_modules_wotDesignUni_components_common_util = require("../common/util.js");
const uni_modules_wotDesignUni_components_composables_useCell = require("../composables/useCell.js");
const uni_modules_wotDesignUni_components_wdForm_types = require("../wd-form/types.js");
const uni_modules_wotDesignUni_components_composables_useParent = require("../composables/useParent.js");
const uni_modules_wotDesignUni_components_composables_useTranslate = require("../composables/useTranslate.js");
const uni_modules_wotDesignUni_components_wdInput_types = require("./types.js");
if (!Array) {
  const _easycom_wd_icon2 = common_vendor.resolveComponent("wd-icon");
  _easycom_wd_icon2();
}
const _easycom_wd_icon = () => "../wd-icon/wd-icon.js";
if (!Math) {
  _easycom_wd_icon();
}
const __default__ = {
  name: "wd-input",
  options: {
    virtualHost: true,
    addGlobalClass: true,
    styleIsolation: "shared"
  }
};
const _sfc_main = /* @__PURE__ */ common_vendor.defineComponent({
  ...__default__,
  props: uni_modules_wotDesignUni_components_wdInput_types.inputProps,
  emits: [
    "update:modelValue",
    "clear",
    "change",
    "blur",
    "focus",
    "input",
    "keyboardheightchange",
    "confirm",
    "linechange",
    "clicksuffixicon",
    "clickprefixicon",
    "click"
  ],
  setup(__props, { emit: __emit }) {
    const props = __props;
    const emit = __emit;
    const { translate } = uni_modules_wotDesignUni_components_composables_useTranslate.useTranslate("input");
    const showClear = common_vendor.ref(false);
    const showWordCount = common_vendor.ref(false);
    const isPwdVisible = common_vendor.ref(false);
    const clearing = common_vendor.ref(false);
    const isFocus = common_vendor.ref(false);
    const inputValue = common_vendor.ref("");
    const cell = uni_modules_wotDesignUni_components_composables_useCell.useCell();
    common_vendor.watch(
      () => props.focus,
      (newValue) => {
        isFocus.value = newValue;
      },
      { immediate: true, deep: true }
    );
    common_vendor.watch(
      () => props.modelValue,
      (newValue) => {
        const { disabled, readonly, clearable } = props;
        if (newValue === void 0) {
          newValue = "";
          console.warn("[wot-design] warning(wd-input): value can not be undefined.");
        }
        inputValue.value = newValue;
        showClear.value = Boolean(clearable && !disabled && !readonly && newValue);
      },
      { immediate: true, deep: true }
    );
    const { parent: form } = uni_modules_wotDesignUni_components_composables_useParent.useParent(uni_modules_wotDesignUni_components_wdForm_types.FORM_KEY);
    const errorMessage = common_vendor.computed(() => {
      if (form && props.prop && form.errorMessages && form.errorMessages[props.prop]) {
        return form.errorMessages[props.prop];
      } else {
        return "";
      }
    });
    const isRequired = common_vendor.computed(() => {
      let formRequired = false;
      if (form && form.props.rules) {
        const rules = form.props.rules;
        for (const key in rules) {
          if (Object.prototype.hasOwnProperty.call(rules, key) && key === props.prop && Array.isArray(rules[key])) {
            formRequired = rules[key].some((rule) => rule.required);
          }
        }
      }
      return props.required || props.rules.some((rule) => rule.required) || formRequired;
    });
    const rootClass = common_vendor.computed(() => {
      return `wd-input  ${props.label || props.useLabelSlot ? "is-cell" : ""} ${props.center ? "is-center" : ""} ${cell.border.value ? "is-border" : ""} ${props.size ? "is-" + props.size : ""} ${props.error ? "is-error" : ""} ${props.disabled ? "is-disabled" : ""}  ${inputValue.value && String(inputValue.value).length > 0 ? "is-not-empty" : ""}  ${props.noBorder ? "is-no-border" : ""} ${props.customClass}`;
    });
    const labelClass = common_vendor.computed(() => {
      return `wd-input__label ${props.customLabelClass} ${isRequired.value ? "is-required" : ""}`;
    });
    const inputPlaceholderClass = common_vendor.computed(() => {
      return `wd-input__placeholder  ${props.placeholderClass}`;
    });
    const labelStyle = common_vendor.computed(() => {
      return props.labelWidth ? uni_modules_wotDesignUni_components_common_util.objToStyle({
        "min-width": props.labelWidth,
        "max-width": props.labelWidth
      }) : "";
    });
    common_vendor.onBeforeMount(() => {
      initState();
    });
    function initState() {
      const { disabled, readonly, clearable, maxlength, showWordLimit } = props;
      let newVal = "";
      if (showWordLimit && maxlength && inputValue.value.toString().length > maxlength) {
        newVal = inputValue.value.toString().substring(0, maxlength);
      }
      showClear.value = Boolean(!disabled && !readonly && clearable && inputValue.value);
      showWordCount.value = Boolean(!disabled && !readonly && maxlength && showWordLimit);
      inputValue.value = newVal || inputValue.value;
      emit("update:modelValue", inputValue.value);
    }
    function togglePwdVisible() {
      isPwdVisible.value = !isPwdVisible.value;
    }
    function clear() {
      inputValue.value = "";
      uni_modules_wotDesignUni_components_common_util.requestAnimationFrame().then(() => uni_modules_wotDesignUni_components_common_util.requestAnimationFrame()).then(() => uni_modules_wotDesignUni_components_common_util.requestAnimationFrame()).then(() => {
        isFocus.value = true;
        emit("change", {
          value: ""
        });
        emit("update:modelValue", inputValue.value);
        emit("clear");
      });
    }
    function handleBlur() {
      isFocus.value = false;
      emit("change", {
        value: inputValue.value
      });
      emit("update:modelValue", inputValue.value);
      emit("blur", {
        value: inputValue.value
      });
    }
    function handleFocus({ detail }) {
      if (clearing.value) {
        clearing.value = false;
        return;
      }
      isFocus.value = true;
      emit("focus", detail);
    }
    function handleInput() {
      emit("update:modelValue", inputValue.value);
      emit("input", inputValue.value);
    }
    function handleKeyboardheightchange(event) {
      emit("keyboardheightchange", event.detail);
    }
    function handleConfirm({ detail }) {
      emit("confirm", detail);
    }
    function onClickSuffixIcon() {
      emit("clicksuffixicon");
    }
    function onClickPrefixIcon() {
      emit("clickprefixicon");
    }
    function handleClick(event) {
      emit("click", event);
    }
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: _ctx.label || _ctx.useLabelSlot
      }, _ctx.label || _ctx.useLabelSlot ? common_vendor.e({
        b: _ctx.prefixIcon || _ctx.usePrefixSlot
      }, _ctx.prefixIcon || _ctx.usePrefixSlot ? common_vendor.e({
        c: _ctx.prefixIcon && !_ctx.usePrefixSlot
      }, _ctx.prefixIcon && !_ctx.usePrefixSlot ? {
        d: common_vendor.o(onClickPrefixIcon),
        e: common_vendor.p({
          ["custom-class"]: "wd-input__icon",
          name: _ctx.prefixIcon
        })
      } : {}) : {}, {
        f: _ctx.label
      }, _ctx.label ? {
        g: common_vendor.t(_ctx.label)
      } : {}, {
        h: common_vendor.n(labelClass.value),
        i: common_vendor.s(labelStyle.value)
      }) : {}, {
        j: (_ctx.prefixIcon || _ctx.usePrefixSlot) && !_ctx.label
      }, (_ctx.prefixIcon || _ctx.usePrefixSlot) && !_ctx.label ? common_vendor.e({
        k: _ctx.prefixIcon
      }, _ctx.prefixIcon ? {
        l: common_vendor.o(onClickPrefixIcon),
        m: common_vendor.p({
          ["custom-class"]: "wd-input__icon",
          name: _ctx.prefixIcon
        })
      } : {}) : {}, {
        n: common_vendor.n(_ctx.prefixIcon ? "wd-input__inner--prefix" : ""),
        o: common_vendor.n(showWordCount.value ? "wd-input__inner--count" : ""),
        p: common_vendor.n(_ctx.alignRight ? "is-align-right" : ""),
        q: common_vendor.n(_ctx.customInputClass),
        r: _ctx.type,
        s: _ctx.showPassword && !isPwdVisible.value,
        t: _ctx.placeholder || common_vendor.unref(translate)("placeholder"),
        v: _ctx.disabled,
        w: _ctx.maxlength,
        x: isFocus.value,
        y: _ctx.confirmType,
        z: _ctx.confirmHold,
        A: _ctx.cursor,
        B: _ctx.cursorSpacing,
        C: _ctx.placeholderStyle,
        D: _ctx.selectionStart,
        E: _ctx.selectionEnd,
        F: _ctx.adjustPosition,
        G: _ctx.holdKeyboard,
        H: _ctx.alwaysEmbed,
        I: inputPlaceholderClass.value,
        J: common_vendor.o([($event) => inputValue.value = $event.detail.value, handleInput]),
        K: common_vendor.o(handleFocus),
        L: common_vendor.o(handleBlur),
        M: common_vendor.o(handleConfirm),
        N: common_vendor.o(handleKeyboardheightchange),
        O: inputValue.value,
        P: _ctx.readonly
      }, _ctx.readonly ? {} : {}, {
        Q: showClear.value || _ctx.showPassword || _ctx.suffixIcon || showWordCount.value || _ctx.useSuffixSlot
      }, showClear.value || _ctx.showPassword || _ctx.suffixIcon || showWordCount.value || _ctx.useSuffixSlot ? common_vendor.e({
        R: showClear.value
      }, showClear.value ? {
        S: common_vendor.o(clear),
        T: common_vendor.p({
          ["custom-class"]: "wd-input__clear",
          name: "error-fill"
        })
      } : {}, {
        U: _ctx.showPassword
      }, _ctx.showPassword ? {
        V: common_vendor.o(togglePwdVisible),
        W: common_vendor.p({
          ["custom-class"]: "wd-input__icon",
          name: isPwdVisible.value ? "view" : "eye-close"
        })
      } : {}, {
        X: showWordCount.value
      }, showWordCount.value ? {
        Y: common_vendor.t(String(inputValue.value).length),
        Z: common_vendor.n(inputValue.value && String(inputValue.value).length > 0 ? "wd-input__count-current" : ""),
        aa: common_vendor.n(String(inputValue.value).length > _ctx.maxlength ? "is-error" : ""),
        ab: common_vendor.t(_ctx.maxlength)
      } : {}, {
        ac: _ctx.suffixIcon
      }, _ctx.suffixIcon ? {
        ad: common_vendor.o(onClickSuffixIcon),
        ae: common_vendor.p({
          ["custom-class"]: "wd-input__icon",
          name: _ctx.suffixIcon
        })
      } : {}) : {}, {
        af: errorMessage.value
      }, errorMessage.value ? {
        ag: common_vendor.t(errorMessage.value)
      } : {}, {
        ah: common_vendor.n(rootClass.value),
        ai: common_vendor.s(_ctx.customStyle),
        aj: common_vendor.o(handleClick)
      });
    };
  }
});
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-4e0c9774"]]);
wx.createComponent(Component);
