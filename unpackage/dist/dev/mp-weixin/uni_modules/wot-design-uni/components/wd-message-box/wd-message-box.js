"use strict";
const common_vendor = require("../../../../common/vendor.js");
const uni_modules_wotDesignUni_components_wdMessageBox_types = require("./types.js");
const uni_modules_wotDesignUni_components_wdMessageBox_index = require("./index.js");
const uni_modules_wotDesignUni_components_common_util = require("../common/util.js");
const uni_modules_wotDesignUni_components_composables_useTranslate = require("../composables/useTranslate.js");
if (!Array) {
  const _easycom_wd_input2 = common_vendor.resolveComponent("wd-input");
  const _easycom_wd_button2 = common_vendor.resolveComponent("wd-button");
  const _easycom_wd_popup2 = common_vendor.resolveComponent("wd-popup");
  (_easycom_wd_input2 + _easycom_wd_button2 + _easycom_wd_popup2)();
}
const _easycom_wd_input = () => "../wd-input/wd-input.js";
const _easycom_wd_button = () => "../wd-button/wd-button.js";
const _easycom_wd_popup = () => "../wd-popup/wd-popup.js";
if (!Math) {
  (_easycom_wd_input + _easycom_wd_button + _easycom_wd_popup)();
}
const __default__ = {
  name: "wd-message-box",
  options: {
    virtualHost: true,
    addGlobalClass: true,
    styleIsolation: "shared"
  }
};
const _sfc_main = /* @__PURE__ */ common_vendor.defineComponent({
  ...__default__,
  props: uni_modules_wotDesignUni_components_wdMessageBox_types.messageBoxProps,
  setup(__props) {
    const props = __props;
    const { translate } = uni_modules_wotDesignUni_components_composables_useTranslate.useTranslate("message-box");
    const rootClass = common_vendor.computed(() => {
      return `wd-message-box__container ${props.customClass}`;
    });
    const bodyClass = common_vendor.computed(() => {
      return `wd-message-box__body ${!title.value ? "is-no-title" : ""} ${type.value === "prompt" ? "is-prompt" : ""}`;
    });
    const messageOptionKey = props.selector ? uni_modules_wotDesignUni_components_wdMessageBox_index.messageDefaultOptionKey + props.selector : uni_modules_wotDesignUni_components_wdMessageBox_index.messageDefaultOptionKey;
    const messageOption = common_vendor.inject(messageOptionKey, common_vendor.ref(uni_modules_wotDesignUni_components_wdMessageBox_index.defaultOptions));
    const msg = common_vendor.ref("");
    let onConfirm = null;
    let onCancel = null;
    let beforeConfirm = null;
    const show = common_vendor.ref(false);
    const title = common_vendor.ref("");
    const showCancelButton = common_vendor.ref(false);
    const closeOnClickModal = common_vendor.ref(true);
    const confirmButtonText = common_vendor.ref("");
    const cancelButtonText = common_vendor.ref("");
    const type = common_vendor.ref("alert");
    const inputType = common_vendor.ref("text");
    const inputValue = common_vendor.ref("");
    const inputPlaceholder = common_vendor.ref("");
    const inputPattern = common_vendor.ref();
    let inputValidate = null;
    const inputError = common_vendor.ref("");
    const showErr = common_vendor.ref(false);
    const zIndex = common_vendor.ref(99);
    const lazyRender = common_vendor.ref(true);
    common_vendor.watch(
      () => messageOption.value,
      (newVal) => {
        reset(newVal);
      },
      {
        deep: true,
        immediate: true
      }
    );
    common_vendor.watch(
      () => show.value,
      (newValue) => {
        resetErr(newValue);
      },
      {
        deep: true,
        immediate: true
      }
    );
    function toggleModal(action) {
      if (action === "modal" && !closeOnClickModal.value) {
        return;
      }
      if (type.value === "prompt" && action === "confirm" && !validate()) {
        return;
      }
      switch (action) {
        case "confirm":
          if (beforeConfirm) {
            beforeConfirm({
              resolve: (isPass) => {
                if (isPass) {
                  handleConfirm({
                    action,
                    value: inputValue.value
                  });
                }
              }
            });
          } else {
            handleConfirm({
              action,
              value: inputValue.value
            });
          }
          break;
        case "cancel":
          handleCancel({
            action
          });
          break;
        default:
          handleCancel({
            action: "modal"
          });
          break;
      }
    }
    function handleConfirm(result) {
      show.value = false;
      if (uni_modules_wotDesignUni_components_common_util.isFunction(onConfirm)) {
        onConfirm(result);
      }
    }
    function handleCancel(result) {
      show.value = false;
      if (uni_modules_wotDesignUni_components_common_util.isFunction(onCancel)) {
        onCancel(result);
      }
    }
    function validate() {
      if (inputPattern.value && !inputPattern.value.test(String(inputValue.value))) {
        showErr.value = true;
        return false;
      }
      if (typeof inputValidate === "function") {
        const validateResult = inputValidate(inputValue.value);
        if (!validateResult) {
          showErr.value = true;
          return false;
        }
      }
      showErr.value = false;
      return true;
    }
    function resetErr(val) {
      if (val === false) {
        showErr.value = false;
      }
    }
    function inputValChange(value) {
      if (value === "") {
        showErr.value = false;
        return;
      }
      inputValue.value = value;
    }
    function reset(option) {
      if (option) {
        title.value = uni_modules_wotDesignUni_components_common_util.isDef(option.title) ? option.title : "";
        showCancelButton.value = uni_modules_wotDesignUni_components_common_util.isDef(option.showCancelButton) ? option.showCancelButton : false;
        show.value = option.show;
        closeOnClickModal.value = option.closeOnClickModal;
        confirmButtonText.value = option.confirmButtonText;
        cancelButtonText.value = option.cancelButtonText;
        msg.value = option.msg;
        type.value = option.type;
        inputType.value = option.inputType;
        inputValue.value = option.inputValue;
        inputPlaceholder.value = option.inputPlaceholder;
        inputPattern.value = option.inputPattern;
        inputValidate = option.inputValidate;
        onConfirm = option.onConfirm;
        onCancel = option.onCancel;
        beforeConfirm = option.beforeConfirm;
        inputError.value = option.inputError;
        showErr.value = option.showErr;
        zIndex.value = option.zIndex;
        lazyRender.value = option.lazyRender;
      }
    }
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: title.value
      }, title.value ? {
        b: common_vendor.t(title.value)
      } : {}, {
        c: type.value === "prompt"
      }, type.value === "prompt" ? common_vendor.e({
        d: common_vendor.o(inputValChange),
        e: common_vendor.o(($event) => inputValue.value = $event),
        f: common_vendor.p({
          type: inputType.value,
          size: "large",
          placeholder: inputPlaceholder.value || "请输入",
          modelValue: inputValue.value
        }),
        g: showErr.value
      }, showErr.value ? {
        h: common_vendor.t(inputError.value || common_vendor.unref(translate)("inputNoValidate"))
      } : {}) : {}, {
        i: common_vendor.t(msg.value),
        j: common_vendor.n(bodyClass.value),
        k: showCancelButton.value
      }, showCancelButton.value ? {
        l: common_vendor.t(cancelButtonText.value || common_vendor.unref(translate)("cancel")),
        m: common_vendor.o(($event) => toggleModal("cancel")),
        n: common_vendor.p({
          type: "info",
          block: true,
          ["custom-style"]: "margin-right: 16px;"
        })
      } : {}, {
        o: common_vendor.t(confirmButtonText.value || common_vendor.unref(translate)("confirm")),
        p: common_vendor.o(($event) => toggleModal("confirm")),
        q: common_vendor.p({
          block: true
        }),
        r: common_vendor.n(`wd-message-box__actions ${showCancelButton.value ? "wd-message-box__flex" : "wd-message-box__block"}`),
        s: common_vendor.n(rootClass.value),
        t: common_vendor.o(($event) => toggleModal("modal")),
        v: common_vendor.o(($event) => show.value = $event),
        w: common_vendor.p({
          transition: "zoom-in",
          ["close-on-click-modal"]: closeOnClickModal.value,
          ["lazy-render"]: lazyRender.value,
          ["custom-class"]: "wd-message-box",
          ["z-index"]: zIndex.value,
          duration: 200,
          modelValue: show.value
        })
      });
    };
  }
});
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-c8139c88"]]);
wx.createComponent(Component);
