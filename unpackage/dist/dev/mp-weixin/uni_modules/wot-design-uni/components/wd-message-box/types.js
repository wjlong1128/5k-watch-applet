"use strict";
const uni_modules_wotDesignUni_components_common_props = require("../common/props.js");
const messageBoxProps = {
  ...uni_modules_wotDesignUni_components_common_props.baseProps,
  selector: String
};
exports.messageBoxProps = messageBoxProps;
