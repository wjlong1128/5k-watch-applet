"use strict";
const uni_modules_wotDesignUni_components_common_props = require("../common/props.js");
const circleProps = {
  ...uni_modules_wotDesignUni_components_common_props.baseProps,
  /**
   * 当前进度
   */
  modelValue: uni_modules_wotDesignUni_components_common_props.makeNumberProp(0),
  /**
   * 圆环直径，默认单位为 px
   */
  size: uni_modules_wotDesignUni_components_common_props.makeNumberProp(100),
  /**
   * 进度条颜色，传入对象格式可以定义渐变色
   */
  color: {
    type: [String, Object],
    default: "#4d80f0"
  },
  /**
   * 轨道颜色
   */
  layerColor: uni_modules_wotDesignUni_components_common_props.makeStringProp("#EBEEF5"),
  /**
   * 填充颜色
   */
  fill: String,
  /**
   * 动画速度（单位为 rate/s）
   */
  speed: uni_modules_wotDesignUni_components_common_props.makeNumberProp(50),
  /**
   * 文字
   */
  text: String,
  /**
   * 进度条宽度 单位px
   */
  strokeWidth: uni_modules_wotDesignUni_components_common_props.makeNumberProp(10),
  /**
   * 进度条端点的形状，可选值为 "butt" | "round" | "square"
   */
  strokeLinecap: uni_modules_wotDesignUni_components_common_props.makeStringProp("round"),
  /**
   * 是否顺时针增加
   */
  clockwise: uni_modules_wotDesignUni_components_common_props.makeBooleanProp(true)
};
exports.circleProps = circleProps;
