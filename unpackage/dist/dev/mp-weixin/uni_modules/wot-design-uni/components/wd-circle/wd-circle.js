"use strict";
const common_vendor = require("../../../../common/vendor.js");
const uni_modules_wotDesignUni_components_common_util = require("../common/util.js");
const uni_modules_wotDesignUni_components_wdCircle_types = require("./types.js");
const __default__ = {
  name: "wd-circle",
  options: {
    addGlobalClass: true,
    virtualHost: true,
    styleIsolation: "shared"
  }
};
const _sfc_main = /* @__PURE__ */ common_vendor.defineComponent({
  ...__default__,
  props: uni_modules_wotDesignUni_components_wdCircle_types.circleProps,
  setup(__props) {
    function format(rate) {
      return Math.min(Math.max(rate, 0), 100);
    }
    const PERIMETER = 2 * Math.PI;
    const BEGIN_ANGLE = -Math.PI / 2;
    const STEP = 1;
    const props = __props;
    const progressColor = common_vendor.ref("");
    const pixel = common_vendor.ref(1);
    const currentValue = common_vendor.ref(0);
    const interval = common_vendor.ref(null);
    const canvasId = common_vendor.ref(uni_modules_wotDesignUni_components_common_util.uuid());
    let ctx = null;
    const canvasSize = common_vendor.computed(() => {
      return props.size * pixel.value;
    });
    const style = common_vendor.computed(() => {
      const style2 = {
        width: uni_modules_wotDesignUni_components_common_util.addUnit(props.size),
        height: uni_modules_wotDesignUni_components_common_util.addUnit(props.size)
      };
      return `${uni_modules_wotDesignUni_components_common_util.objToStyle(style2)}; ${props.customStyle}`;
    });
    common_vendor.watch(
      () => props.modelValue,
      () => {
        reRender();
      },
      { immediate: true }
    );
    common_vendor.watch(
      () => props.size,
      () => {
        let timer = setTimeout(() => {
          drawCircle(currentValue.value);
          clearTimeout(timer);
        }, 50);
      },
      { immediate: false }
    );
    common_vendor.watch(
      () => props.color,
      () => {
        drawCircle(currentValue.value);
      },
      { immediate: false, deep: true }
    );
    common_vendor.watch(
      () => props.layerColor,
      () => {
        drawCircle(currentValue.value);
      },
      { immediate: false }
    );
    common_vendor.watch(
      () => props.strokeWidth,
      () => {
        drawCircle(currentValue.value);
      },
      { immediate: false }
    );
    common_vendor.watch(
      () => props.clockwise,
      () => {
        drawCircle(currentValue.value);
      },
      { immediate: false }
    );
    common_vendor.onMounted(() => {
      currentValue.value = props.modelValue;
      drawCircle(currentValue.value);
    });
    common_vendor.onUnmounted(() => {
      clearTimeInterval();
    });
    const { proxy } = common_vendor.getCurrentInstance();
    function getContext() {
      if (!ctx) {
        ctx = common_vendor.index.createCanvasContext(canvasId.value, proxy);
      }
      return Promise.resolve(ctx);
    }
    function presetCanvas(context, strokeStyle, beginAngle, endAngle, fill) {
      const canvasSize2 = props.size * pixel.value;
      let strokeWidth = props.strokeWidth * pixel.value;
      const position = canvasSize2 / 2;
      if (!fill) {
        strokeWidth = strokeWidth / 2;
      }
      const radius = position - strokeWidth / 2;
      context.strokeStyle = strokeStyle;
      context.setLineWidth(strokeWidth);
      context.setLineCap(props.strokeLinecap);
      context.beginPath();
      context.arc(position, position, radius, beginAngle, endAngle, !props.clockwise);
      context.stroke();
      if (fill) {
        context.setLineWidth(strokeWidth);
        context.setFillStyle(fill);
        context.fill();
      }
    }
    function renderLayerCircle(context) {
      presetCanvas(context, props.layerColor, 0, PERIMETER, props.fill);
    }
    function renderHoverCircle(context, formatValue) {
      const canvasSize2 = props.size * pixel.value;
      const progress = PERIMETER * (formatValue / 100);
      const endAngle = props.clockwise ? BEGIN_ANGLE + progress : 3 * Math.PI - (BEGIN_ANGLE + progress);
      if (uni_modules_wotDesignUni_components_common_util.isObj(props.color)) {
        const LinearColor = context.createLinearGradient(canvasSize2, 0, 0, 0);
        Object.keys(props.color).sort((a, b) => parseFloat(a) - parseFloat(b)).map((key) => LinearColor.addColorStop(parseFloat(key) / 100, props.color[key]));
        progressColor.value = LinearColor;
      } else {
        progressColor.value = props.color;
      }
      presetCanvas(context, progressColor.value, BEGIN_ANGLE, endAngle);
    }
    function renderDot(context) {
      const canvasSize2 = props.size * pixel.value;
      const strokeWidth = props.strokeWidth * pixel.value;
      const position = canvasSize2 / 2;
      if (uni_modules_wotDesignUni_components_common_util.isObj(props.color)) {
        const LinearColor = context.createLinearGradient(canvasSize2, 0, 0, 0);
        Object.keys(props.color).sort((a, b) => parseFloat(a) - parseFloat(b)).map((key) => LinearColor.addColorStop(parseFloat(key) / 100, props.color[key]));
        progressColor.value = LinearColor;
      } else {
        progressColor.value = props.color;
      }
      context.beginPath();
      context.arc(position, strokeWidth / 4, strokeWidth / 4, 0, PERIMETER);
      context.setFillStyle(progressColor.value);
      context.fill();
    }
    function drawCircle(currentValue2) {
      const canvasSize2 = props.size * pixel.value;
      getContext().then((context) => {
        context.clearRect(0, 0, canvasSize2, canvasSize2);
        renderLayerCircle(context);
        const formatValue = format(currentValue2);
        if (formatValue !== 0) {
          renderHoverCircle(context, formatValue);
        } else {
          renderDot(context);
        }
        context.draw();
      });
    }
    function reRender() {
      if (props.speed <= 0 || props.speed > 1e3) {
        drawCircle(props.modelValue);
        return;
      }
      clearTimeInterval();
      currentValue.value = currentValue.value || 0;
      const run = () => {
        interval.value = setTimeout(() => {
          if (currentValue.value !== props.modelValue) {
            if (Math.abs(currentValue.value - props.modelValue) < STEP) {
              currentValue.value = props.modelValue;
            } else if (currentValue.value < props.modelValue) {
              currentValue.value += STEP;
            } else {
              currentValue.value -= STEP;
            }
            drawCircle(currentValue.value);
            run();
          } else {
            clearTimeInterval();
          }
        }, 1e3 / props.speed);
      };
      run();
    }
    function clearTimeInterval() {
      interval.value && clearTimeout(interval.value);
    }
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: canvasSize.value,
        b: canvasSize.value,
        c: common_vendor.s(style.value),
        d: canvasId.value,
        e: canvasId.value,
        f: !_ctx.text
      }, !_ctx.text ? {} : {
        g: common_vendor.t(_ctx.text)
      }, {
        h: common_vendor.n(`wd-circle ${_ctx.customClass}`),
        i: common_vendor.s(_ctx.customStyle)
      });
    };
  }
});
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-e4cb10ec"]]);
wx.createComponent(Component);
