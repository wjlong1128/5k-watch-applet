"use strict";
const common_vendor = require("../../../../common/vendor.js");
const uni_modules_wotDesignUni_components_common_util = require("../common/util.js");
const uni_modules_wotDesignUni_components_wdSticky_types = require("./types.js");
if (!Array) {
  const _easycom_wd_resize2 = common_vendor.resolveComponent("wd-resize");
  _easycom_wd_resize2();
}
const _easycom_wd_resize = () => "../wd-resize/wd-resize.js";
if (!Math) {
  _easycom_wd_resize();
}
const __default__ = {
  name: "wd-sticky",
  options: {
    addGlobalClass: true,
    virtualHost: true,
    styleIsolation: "shared"
  }
};
const _sfc_main = /* @__PURE__ */ common_vendor.defineComponent({
  ...__default__,
  props: uni_modules_wotDesignUni_components_wdSticky_types.stickyProps,
  setup(__props, { expose: __expose }) {
    const props = __props;
    const styckyId = common_vendor.ref(`wd-sticky${uni_modules_wotDesignUni_components_common_util.uuid()}`);
    const openBox = common_vendor.ref(false);
    const position = common_vendor.ref("absolute");
    const top = common_vendor.ref(0);
    const height = common_vendor.ref(0);
    const width = common_vendor.ref(0);
    const observerList = common_vendor.ref([]);
    const state = common_vendor.ref("");
    const boxHeight = common_vendor.inject("box-height", null) || common_vendor.ref(0);
    const observerForChild = common_vendor.inject("observerForChild", null);
    const { proxy } = common_vendor.getCurrentInstance();
    const instance = common_vendor.getCurrentInstance();
    const rootStyle = common_vendor.computed(() => {
      const style = {
        "z-index": props.zIndex,
        height: uni_modules_wotDesignUni_components_common_util.addUnit(height.value),
        width: uni_modules_wotDesignUni_components_common_util.addUnit(width.value)
      };
      if (!openBox.value) {
        style["position"] = "relative";
      }
      return `${uni_modules_wotDesignUni_components_common_util.objToStyle(style)};${props.customStyle}`;
    });
    const stickyStyle = common_vendor.computed(() => {
      const style = {
        "z-index": props.zIndex,
        height: uni_modules_wotDesignUni_components_common_util.addUnit(height.value),
        width: uni_modules_wotDesignUni_components_common_util.addUnit(width.value)
      };
      if (!openBox.value) {
        style["position"] = "relative";
      }
      return `${uni_modules_wotDesignUni_components_common_util.objToStyle(style)};`;
    });
    const containerStyle = common_vendor.computed(() => {
      const style = {
        position: position.value,
        top: uni_modules_wotDesignUni_components_common_util.addUnit(top.value)
      };
      return uni_modules_wotDesignUni_components_common_util.objToStyle(style);
    });
    const innerOffsetTop = common_vendor.computed(() => {
      let top2 = 0;
      return top2 + props.offsetTop;
    });
    function clearObserver() {
      while (observerList.value.length !== 0) {
        observerList.value.pop().disconnect();
      }
    }
    function createObserver() {
      const observer = common_vendor.index.createIntersectionObserver(instance);
      observerList.value.push(observer);
      return observer;
    }
    function resizeHandler(detail) {
      width.value = detail.width;
      height.value = detail.height;
      uni_modules_wotDesignUni_components_common_util.requestAnimationFrame(() => {
        observerContentScroll();
        if (!observerForChild)
          return;
        observerForChild(proxy);
      });
    }
    function observerContentScroll() {
      if (height.value === 0 && width.value === 0)
        return;
      const offset = innerOffsetTop.value + height.value;
      clearObserver();
      createObserver().relativeToViewport({
        top: -offset
        // viewport上边界往下拉
      }).observe(`#${styckyId.value}`, (result) => {
        scrollHandler(result);
      });
      uni_modules_wotDesignUni_components_common_util.getRect(`#${styckyId.value}`, false, proxy).then((res) => {
        if (Number(res.bottom) <= offset)
          scrollHandler({ boundingClientRect: res });
      });
    }
    function scrollHandler({ boundingClientRect }) {
      if (observerForChild && height.value >= boxHeight.value) {
        position.value = "absolute";
        top.value = 0;
        return;
      }
      if (boundingClientRect.top <= innerOffsetTop.value) {
        state.value = "sticky";
        openBox.value = false;
        position.value = "fixed";
        top.value = innerOffsetTop.value;
      } else if (boundingClientRect.top > innerOffsetTop.value) {
        state.value = "normal";
        openBox.value = false;
        position.value = "absolute";
        top.value = 0;
      }
    }
    function setPosition(setOpenBox, setPosition2, setTop) {
      openBox.value = setOpenBox;
      position.value = setPosition2;
      top.value = setTop;
    }
    __expose({
      setPosition,
      openBox,
      position,
      top,
      height,
      width,
      state,
      offsetTop: innerOffsetTop.value
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.o(resizeHandler),
        b: common_vendor.p({
          ["custom-style"]: "display: inline-block;"
        }),
        c: common_vendor.s(containerStyle.value),
        d: common_vendor.n(`wd-sticky ${_ctx.customClass}`),
        e: common_vendor.s(stickyStyle.value),
        f: styckyId.value,
        g: common_vendor.s(`${rootStyle.value};display: inline-block;`)
      };
    };
  }
});
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-2722b5fd"]]);
wx.createComponent(Component);
