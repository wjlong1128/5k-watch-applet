"use strict";
const common_vendor = require("../../../../common/vendor.js");
const uni_modules_wotDesignUni_components_common_util = require("../common/util.js");
const uni_modules_wotDesignUni_components_common_props = require("../common/props.js");
if (!Array) {
  const _easycom_wd_resize2 = common_vendor.resolveComponent("wd-resize");
  _easycom_wd_resize2();
}
const _easycom_wd_resize = () => "../wd-resize/wd-resize.js";
if (!Math) {
  _easycom_wd_resize();
}
const __default__ = {
  name: "wd-sticky-box",
  options: {
    addGlobalClass: true,
    // virtualHost: true,
    styleIsolation: "shared"
  }
};
const _sfc_main = /* @__PURE__ */ common_vendor.defineComponent({
  ...__default__,
  props: uni_modules_wotDesignUni_components_common_props.baseProps,
  setup(__props) {
    const props = __props;
    const styckyBoxId = common_vendor.ref(`wd-sticky-box${uni_modules_wotDesignUni_components_common_util.uuid()}`);
    const observerMap = common_vendor.ref(/* @__PURE__ */ new Map());
    const height = common_vendor.ref(0);
    const width = common_vendor.ref(0);
    const stickyList = [];
    const { proxy } = common_vendor.getCurrentInstance();
    const instance = common_vendor.getCurrentInstance();
    common_vendor.provide("box-height", height);
    common_vendor.provide("box-width", width);
    common_vendor.provide("observerForChild", observerForChild);
    common_vendor.onBeforeMount(() => {
      observerMap.value = /* @__PURE__ */ new Map();
    });
    function resizeHandler(detail) {
      width.value = detail.width;
      height.value = detail.height;
      const temp = observerMap.value;
      observerMap.value = /* @__PURE__ */ new Map();
      for (const [uid] of temp) {
        const child = stickyList.find((sticky) => {
          return sticky.$.uid === uid;
        });
        observerForChild(child);
      }
      temp.forEach((observer) => {
        observer.disconnect();
      });
      temp.clear();
    }
    function deleteObserver(child) {
      const observer = observerMap.value.get(child.$.uid);
      if (!observer)
        return;
      observer.disconnect();
      observerMap.value.delete(child.$.uid);
    }
    function createObserver(child) {
      const observer = common_vendor.index.createIntersectionObserver(instance);
      observerMap.value.set(child.$.uid, observer);
      return observer;
    }
    function observerForChild(child) {
      const hasChild = stickyList.find((sticky) => {
        return sticky.$.uid === child.$.uid;
      });
      if (!hasChild) {
        stickyList.push(child);
      }
      deleteObserver(child);
      const observer = createObserver(child);
      const exposed = child.$.exposed;
      const offset = exposed.height.value + exposed.offsetTop;
      if (height.value <= exposed.height.value) {
        exposed.setPosition(false, "absolute", 0);
      }
      observer.relativeToViewport({ top: -offset }).observe(`#${styckyBoxId.value}`, (result) => {
        scrollHandler(exposed, result);
      });
      uni_modules_wotDesignUni_components_common_util.getRect(`#${styckyBoxId.value}`, false, proxy).then((res) => {
        if (Number(res.bottom) <= offset)
          scrollHandler(exposed, { boundingClientRect: res });
      }).catch((res) => {
        console.log(res);
      });
    }
    function scrollHandler(exposed, { boundingClientRect }) {
      const offset = exposed.height.value + exposed.offsetTop;
      if (boundingClientRect.bottom <= offset) {
        exposed.setPosition(true, "absolute", boundingClientRect.height - exposed.height.value);
      } else if (boundingClientRect.top <= offset && boundingClientRect.bottom > offset) {
        if (exposed.state.value === "normal")
          return;
        exposed.setPosition(false, "fixed", exposed.offsetTop);
      }
    }
    return (_ctx, _cache) => {
      return {
        a: common_vendor.o(resizeHandler),
        b: common_vendor.n(`wd-sticky-box ${props.customClass}`),
        c: common_vendor.s(_ctx.customStyle),
        d: styckyBoxId.value
      };
    };
  }
});
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-0667b36f"]]);
wx.createComponent(Component);
