"use strict";
const common_vendor = require("../../../../common/vendor.js");
const uni_modules_wotDesignUni_components_common_util = require("../common/util.js");
const uni_modules_wotDesignUni_components_composables_useCell = require("../composables/useCell.js");
const uni_modules_wotDesignUni_components_wdForm_types = require("../wd-form/types.js");
const uni_modules_wotDesignUni_components_composables_useParent = require("../composables/useParent.js");
const uni_modules_wotDesignUni_components_composables_useTranslate = require("../composables/useTranslate.js");
const uni_modules_wotDesignUni_components_wdTextarea_types = require("./types.js");
if (!Array) {
  const _easycom_wd_icon2 = common_vendor.resolveComponent("wd-icon");
  _easycom_wd_icon2();
}
const _easycom_wd_icon = () => "../wd-icon/wd-icon.js";
if (!Math) {
  _easycom_wd_icon();
}
const __default__ = {
  name: "wd-textarea",
  options: {
    virtualHost: true,
    addGlobalClass: true,
    styleIsolation: "shared"
  }
};
const _sfc_main = /* @__PURE__ */ common_vendor.defineComponent({
  ...__default__,
  props: uni_modules_wotDesignUni_components_wdTextarea_types.textareaProps,
  emits: [
    "update:modelValue",
    "clear",
    "change",
    "blur",
    "focus",
    "input",
    "keyboardheightchange",
    "confirm",
    "linechange",
    "clickprefixicon",
    "click"
  ],
  setup(__props, { emit: __emit }) {
    const { translate } = uni_modules_wotDesignUni_components_composables_useTranslate.useTranslate("textarea");
    const props = __props;
    const emit = __emit;
    const showClear = common_vendor.ref(false);
    const showWordCount = common_vendor.ref(false);
    const clearing = common_vendor.ref(false);
    const isFocus = common_vendor.ref(false);
    const inputValue = common_vendor.ref("");
    const cell = uni_modules_wotDesignUni_components_composables_useCell.useCell();
    common_vendor.watch(
      () => props.focus,
      (newValue) => {
        isFocus.value = newValue;
      },
      { immediate: true, deep: true }
    );
    common_vendor.watch(
      () => props.modelValue,
      (newValue) => {
        const { disabled, readonly, clearable } = props;
        if (newValue === null || newValue === void 0) {
          newValue = "";
          console.warn("[wot-design] warning(wd-textarea): value can not be null or undefined.");
        }
        inputValue.value = newValue;
        showClear.value = Boolean(clearable && !disabled && !readonly && newValue);
      },
      { immediate: true, deep: true }
    );
    const { parent: form } = uni_modules_wotDesignUni_components_composables_useParent.useParent(uni_modules_wotDesignUni_components_wdForm_types.FORM_KEY);
    const errorMessage = common_vendor.computed(() => {
      if (form && props.prop && form.errorMessages && form.errorMessages[props.prop]) {
        return form.errorMessages[props.prop];
      } else {
        return "";
      }
    });
    const isRequired = common_vendor.computed(() => {
      let formRequired = false;
      if (form && form.props.rules) {
        const rules = form.props.rules;
        for (const key in rules) {
          if (Object.prototype.hasOwnProperty.call(rules, key) && key === props.prop && Array.isArray(rules[key])) {
            formRequired = rules[key].some((rule) => rule.required);
          }
        }
      }
      return props.required || props.rules.some((rule) => rule.required) || formRequired;
    });
    const currentLength = common_vendor.computed(() => {
      return String(props.modelValue || "").length;
    });
    const rootClass = common_vendor.computed(() => {
      return `wd-textarea   ${props.label || props.useLabelSlot ? "is-cell" : ""} ${props.center ? "is-center" : ""} ${cell.border.value ? "is-border" : ""} ${props.size ? "is-" + props.size : ""} ${props.error ? "is-error" : ""} ${props.disabled ? "is-disabled" : ""} ${props.autoHeight ? "is-auto-height" : ""} ${currentLength.value > 0 ? "is-not-empty" : ""}  ${props.noBorder ? "is-no-border" : ""} ${props.customClass}`;
    });
    const labelClass = common_vendor.computed(() => {
      return `wd-textarea__label ${props.customLabelClass} ${isRequired.value ? "is-required" : ""}`;
    });
    const inputPlaceholderClass = common_vendor.computed(() => {
      return `wd-textarea__placeholder  ${props.placeholderClass}`;
    });
    const countClass = common_vendor.computed(() => {
      return `${currentLength.value > 0 ? "wd-textarea__count-current" : ""} ${currentLength.value > props.maxlength ? "is-error" : ""}`;
    });
    const labelStyle = common_vendor.computed(() => {
      return props.labelWidth ? uni_modules_wotDesignUni_components_common_util.objToStyle({
        "min-width": props.labelWidth,
        "max-width": props.labelWidth
      }) : "";
    });
    common_vendor.onBeforeMount(() => {
      initState();
    });
    function initState() {
      const { disabled, readonly, clearable, maxlength, showWordLimit } = props;
      showClear.value = Boolean(!disabled && !readonly && clearable && inputValue.value);
      showWordCount.value = Boolean(!disabled && !readonly && maxlength && showWordLimit);
      inputValue.value = formatValue(inputValue.value);
      emit("update:modelValue", inputValue.value);
    }
    function formatValue(value) {
      const { maxlength, showWordLimit } = props;
      if (showWordLimit && maxlength !== -1 && value.length > maxlength) {
        return value.toString().substring(0, maxlength);
      }
      return value;
    }
    function clear() {
      inputValue.value = "";
      uni_modules_wotDesignUni_components_common_util.requestAnimationFrame().then(() => uni_modules_wotDesignUni_components_common_util.requestAnimationFrame()).then(() => uni_modules_wotDesignUni_components_common_util.requestAnimationFrame()).then(() => {
        emit("change", {
          value: ""
        });
        emit("update:modelValue", inputValue.value);
        emit("clear");
        uni_modules_wotDesignUni_components_common_util.requestAnimationFrame().then(() => {
          isFocus.value = true;
        });
      });
    }
    function handleBlur({ detail }) {
      isFocus.value = false;
      emit("change", {
        value: inputValue.value
      });
      emit("update:modelValue", inputValue.value);
      emit("blur", {
        value: inputValue.value,
        // textarea 有 cursor
        cursor: detail.cursor ? detail.cursor : null
      });
    }
    function handleFocus({ detail }) {
      if (clearing.value) {
        clearing.value = false;
        return;
      }
      isFocus.value = true;
      emit("focus", detail);
    }
    function handleInput() {
      inputValue.value = formatValue(inputValue.value);
      emit("update:modelValue", inputValue.value);
      emit("input", inputValue.value);
    }
    function handleKeyboardheightchange({ detail }) {
      emit("keyboardheightchange", detail);
    }
    function handleConfirm({ detail }) {
      emit("confirm", detail);
    }
    function handleLineChange({ detail }) {
      emit("linechange", detail);
    }
    function onClickPrefixIcon() {
      emit("clickprefixicon");
    }
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: _ctx.label || _ctx.useLabelSlot
      }, _ctx.label || _ctx.useLabelSlot ? common_vendor.e({
        b: _ctx.prefixIcon || _ctx.usePrefixSlot
      }, _ctx.prefixIcon || _ctx.usePrefixSlot ? common_vendor.e({
        c: _ctx.prefixIcon && !_ctx.usePrefixSlot
      }, _ctx.prefixIcon && !_ctx.usePrefixSlot ? {
        d: common_vendor.o(onClickPrefixIcon),
        e: common_vendor.p({
          ["custom-class"]: "wd-textarea__icon",
          name: _ctx.prefixIcon
        })
      } : {}) : {}, {
        f: _ctx.label
      }, _ctx.label ? {
        g: common_vendor.t(_ctx.label)
      } : {}, {
        h: common_vendor.n(labelClass.value),
        i: common_vendor.s(labelStyle.value)
      }) : {}, {
        j: common_vendor.n(`wd-textarea__inner ${_ctx.customTextareaClass}`),
        k: _ctx.placeholder || common_vendor.unref(translate)("placeholder"),
        l: _ctx.disabled,
        m: _ctx.maxlength,
        n: isFocus.value,
        o: _ctx.autoFocus,
        p: _ctx.placeholderStyle,
        q: inputPlaceholderClass.value,
        r: _ctx.autoHeight,
        s: _ctx.cursorSpacing,
        t: _ctx.fixed,
        v: _ctx.cursor,
        w: _ctx.showConfirmBar,
        x: _ctx.selectionStart,
        y: _ctx.selectionEnd,
        z: _ctx.adjustPosition,
        A: _ctx.holdKeyboard,
        B: _ctx.confirmType,
        C: _ctx.confirmHold,
        D: _ctx.disableDefaultPadding,
        E: common_vendor.o([($event) => inputValue.value = $event.detail.value, handleInput]),
        F: common_vendor.o(handleFocus),
        G: common_vendor.o(handleBlur),
        H: common_vendor.o(handleConfirm),
        I: common_vendor.o(handleLineChange),
        J: common_vendor.o(handleKeyboardheightchange),
        K: inputValue.value,
        L: errorMessage.value
      }, errorMessage.value ? {
        M: common_vendor.t(errorMessage.value)
      } : {}, {
        N: _ctx.readonly
      }, _ctx.readonly ? {} : {}, {
        O: showClear.value
      }, showClear.value ? {
        P: common_vendor.o(clear),
        Q: common_vendor.p({
          ["custom-class"]: "wd-textarea__clear",
          name: "error-fill"
        })
      } : {}, {
        R: showWordCount.value
      }, showWordCount.value ? {
        S: common_vendor.t(currentLength.value),
        T: common_vendor.n(countClass.value),
        U: common_vendor.t(_ctx.maxlength)
      } : {}, {
        V: common_vendor.n(`wd-textarea__value ${showClear.value ? "is-suffix" : ""} ${_ctx.customTextareaContainerClass} ${showWordCount.value ? "is-show-limit" : ""}`),
        W: common_vendor.n(rootClass.value),
        X: common_vendor.s(_ctx.customStyle)
      });
    };
  }
});
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-7d71e04e"]]);
wx.createComponent(Component);
