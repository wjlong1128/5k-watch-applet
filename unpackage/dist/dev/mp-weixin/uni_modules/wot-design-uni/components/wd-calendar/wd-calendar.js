"use strict";
const common_vendor = require("../../../../common/vendor.js");
const uni_modules_wotDesignUni_components_common_dayjs = require("../common/dayjs.js");
const uni_modules_wotDesignUni_components_common_util = require("../common/util.js");
const uni_modules_wotDesignUni_components_wdCalendarView_utils = require("../wd-calendar-view/utils.js");
const uni_modules_wotDesignUni_components_composables_useCell = require("../composables/useCell.js");
const uni_modules_wotDesignUni_components_wdForm_types = require("../wd-form/types.js");
const uni_modules_wotDesignUni_components_composables_useParent = require("../composables/useParent.js");
const uni_modules_wotDesignUni_components_composables_useTranslate = require("../composables/useTranslate.js");
const uni_modules_wotDesignUni_components_wdCalendar_types = require("./types.js");
if (!Array) {
  const _easycom_wd_icon2 = common_vendor.resolveComponent("wd-icon");
  const _easycom_wd_tab2 = common_vendor.resolveComponent("wd-tab");
  const _easycom_wd_tabs2 = common_vendor.resolveComponent("wd-tabs");
  const _easycom_wd_tag2 = common_vendor.resolveComponent("wd-tag");
  const _easycom_wd_calendar_view2 = common_vendor.resolveComponent("wd-calendar-view");
  const _easycom_wd_button2 = common_vendor.resolveComponent("wd-button");
  const _easycom_wd_action_sheet2 = common_vendor.resolveComponent("wd-action-sheet");
  (_easycom_wd_icon2 + _easycom_wd_tab2 + _easycom_wd_tabs2 + _easycom_wd_tag2 + _easycom_wd_calendar_view2 + _easycom_wd_button2 + _easycom_wd_action_sheet2)();
}
const _easycom_wd_icon = () => "../wd-icon/wd-icon.js";
const _easycom_wd_tab = () => "../wd-tab/wd-tab.js";
const _easycom_wd_tabs = () => "../wd-tabs/wd-tabs.js";
const _easycom_wd_tag = () => "../wd-tag/wd-tag.js";
const _easycom_wd_calendar_view = () => "../wd-calendar-view/wd-calendar-view.js";
const _easycom_wd_button = () => "../wd-button/wd-button.js";
const _easycom_wd_action_sheet = () => "../wd-action-sheet/wd-action-sheet.js";
if (!Math) {
  (_easycom_wd_icon + _easycom_wd_tab + _easycom_wd_tabs + _easycom_wd_tag + _easycom_wd_calendar_view + _easycom_wd_button + _easycom_wd_action_sheet)();
}
const __default__ = {
  name: "wd-calendar",
  options: {
    addGlobalClass: true,
    virtualHost: true,
    styleIsolation: "shared"
  }
};
const _sfc_main = /* @__PURE__ */ common_vendor.defineComponent({
  ...__default__,
  props: uni_modules_wotDesignUni_components_wdCalendar_types.calendarProps,
  emits: ["cancel", "change", "update:modelValue", "confirm"],
  setup(__props, { expose: __expose, emit: __emit }) {
    const { translate } = uni_modules_wotDesignUni_components_composables_useTranslate.useTranslate("calendar");
    const defaultDisplayFormat = (value, type) => {
      switch (type) {
        case "date":
          return uni_modules_wotDesignUni_components_common_dayjs.dayjs(value).format("YYYY-MM-DD");
        case "dates":
          return value.map((item) => {
            return uni_modules_wotDesignUni_components_common_dayjs.dayjs(item).format("YYYY-MM-DD");
          }).join(", ");
        case "daterange":
          return `${value[0] ? uni_modules_wotDesignUni_components_common_dayjs.dayjs(value[0]).format("YYYY-MM-DD") : translate("startTime")} ${translate("to")} ${value[1] ? uni_modules_wotDesignUni_components_common_dayjs.dayjs(value[1]).format("YYYY-MM-DD") : translate("endTime")}`;
        case "datetime":
          return uni_modules_wotDesignUni_components_common_dayjs.dayjs(value).format("YYYY-MM-DD HH:mm:ss");
        case "datetimerange":
          return `${value[0] ? uni_modules_wotDesignUni_components_common_dayjs.dayjs(value[0]).format(translate("timeFormat")) : translate("startTime")} ${translate(
            "to"
          )}
${value[1] ? uni_modules_wotDesignUni_components_common_dayjs.dayjs(value[1]).format(translate("timeFormat")) : translate("endTime")}`;
        case "week": {
          const year = new Date(value).getFullYear();
          const week = uni_modules_wotDesignUni_components_wdCalendarView_utils.getWeekNumber(value);
          return translate("weekFormat", year, uni_modules_wotDesignUni_components_common_util.padZero(week));
        }
        case "weekrange": {
          const year1 = new Date(value[0]).getFullYear();
          const week1 = uni_modules_wotDesignUni_components_wdCalendarView_utils.getWeekNumber(value[0]);
          const year2 = new Date(value[1]).getFullYear();
          const week2 = uni_modules_wotDesignUni_components_wdCalendarView_utils.getWeekNumber(value[1]);
          return `${value[0] ? translate("weekFormat", year1, uni_modules_wotDesignUni_components_common_util.padZero(week1)) : translate("startWeek")} - ${value[1] ? translate("weekFormat", year2, uni_modules_wotDesignUni_components_common_util.padZero(week2)) : translate("endWeek")}`;
        }
        case "month":
          return uni_modules_wotDesignUni_components_common_dayjs.dayjs(value).format("YYYY / MM");
        case "monthrange":
          return `${value[0] ? uni_modules_wotDesignUni_components_common_dayjs.dayjs(value[0]).format("YYYY / MM") : translate("startMonth")} ${translate("to")} ${value[1] ? uni_modules_wotDesignUni_components_common_dayjs.dayjs(value[1]).format("YYYY / MM") : translate("endMonth")}`;
      }
    };
    const formatRange = (value, rangeType, type) => {
      switch (type) {
        case "daterange":
          if (!value) {
            return rangeType === "end" ? translate("endTime") : translate("startTime");
          }
          return uni_modules_wotDesignUni_components_common_dayjs.dayjs(value).format(translate("dateFormat"));
        case "datetimerange":
          if (!value) {
            return rangeType === "end" ? translate("endTime") : translate("startTime");
          }
          return uni_modules_wotDesignUni_components_common_dayjs.dayjs(value).format(translate("timeFormat"));
        case "weekrange": {
          if (!value) {
            return rangeType === "end" ? translate("endWeek") : translate("startWeek");
          }
          const date = new Date(value);
          const year = date.getFullYear();
          const week = uni_modules_wotDesignUni_components_wdCalendarView_utils.getWeekNumber(value);
          return translate("weekFormat", year, uni_modules_wotDesignUni_components_common_util.padZero(week));
        }
        case "monthrange":
          if (!value) {
            return rangeType === "end" ? translate("endMonth") : translate("startMonth");
          }
          return uni_modules_wotDesignUni_components_common_dayjs.dayjs(value).format(translate("monthFormat"));
      }
    };
    const props = __props;
    const emit = __emit;
    const pickerShow = common_vendor.ref(false);
    const calendarValue = common_vendor.ref(null);
    const lastCalendarValue = common_vendor.ref(null);
    const panelHeight = common_vendor.ref(338);
    const confirmBtnDisabled = common_vendor.ref(true);
    const currentTab = common_vendor.ref(0);
    const lastTab = common_vendor.ref(0);
    const currentType = common_vendor.ref("date");
    const lastCurrentType = common_vendor.ref();
    const inited = common_vendor.ref(false);
    const cell = uni_modules_wotDesignUni_components_composables_useCell.useCell();
    const calendarView = common_vendor.ref();
    const calendarTabs = common_vendor.ref();
    const rangeLabel = common_vendor.computed(() => {
      const [start, end] = uni_modules_wotDesignUni_components_common_util.deepClone(uni_modules_wotDesignUni_components_common_util.isArray(calendarValue.value) ? calendarValue.value : []);
      return [start, end].map((item, index) => {
        return (props.innerDisplayFormat || formatRange)(item, index === 0 ? "start" : "end", currentType.value);
      });
    });
    const showValue = common_vendor.computed(() => {
      if (!uni_modules_wotDesignUni_components_common_util.isArray(props.modelValue) && props.modelValue || uni_modules_wotDesignUni_components_common_util.isArray(props.modelValue) && props.modelValue.length) {
        return (props.displayFormat || defaultDisplayFormat)(props.modelValue, lastCurrentType.value || currentType.value);
      } else {
        return "";
      }
    });
    common_vendor.watch(
      () => props.modelValue,
      (val, oldVal) => {
        if (uni_modules_wotDesignUni_components_common_util.isEqual(val, oldVal))
          return;
        calendarValue.value = uni_modules_wotDesignUni_components_common_util.deepClone(val);
        confirmBtnDisabled.value = getConfirmBtnStatus(val);
      },
      {
        immediate: true
      }
    );
    common_vendor.watch(
      () => props.type,
      (newValue, oldValue) => {
        if (props.showTypeSwitch) {
          const tabs = ["date", "week", "month"];
          const rangeTabs = ["daterange", "weekrange", "monthrange"];
          const index = newValue.indexOf("range") > -1 ? rangeTabs.indexOf(newValue) || 0 : tabs.indexOf(newValue);
          currentTab.value = index;
        }
        panelHeight.value = props.showConfirm ? 338 : 400;
        currentType.value = uni_modules_wotDesignUni_components_common_util.deepClone(newValue);
      },
      {
        deep: true,
        immediate: true
      }
    );
    common_vendor.watch(
      () => props.showConfirm,
      (val) => {
        panelHeight.value = val ? 338 : 400;
      },
      {
        deep: true,
        immediate: true
      }
    );
    const { parent: form } = uni_modules_wotDesignUni_components_composables_useParent.useParent(uni_modules_wotDesignUni_components_wdForm_types.FORM_KEY);
    const errorMessage = common_vendor.computed(() => {
      if (form && props.prop && form.errorMessages && form.errorMessages[props.prop]) {
        return form.errorMessages[props.prop];
      } else {
        return "";
      }
    });
    const isRequired = common_vendor.computed(() => {
      let formRequired = false;
      if (form && form.props.rules) {
        const rules = form.props.rules;
        for (const key in rules) {
          if (Object.prototype.hasOwnProperty.call(rules, key) && key === props.prop && Array.isArray(rules[key])) {
            formRequired = rules[key].some((rule) => rule.required);
          }
        }
      }
      return props.required || props.rules.some((rule) => rule.required) || formRequired;
    });
    const range = common_vendor.computed(() => {
      return (type) => {
        return uni_modules_wotDesignUni_components_wdCalendarView_utils.isRange(type);
      };
    });
    function scrollIntoView() {
      calendarView.value && calendarView.value && calendarView.value.$.exposed.scrollIntoView();
    }
    function open() {
      const { disabled, readonly } = props;
      if (disabled || readonly)
        return;
      inited.value = true;
      pickerShow.value = true;
      lastCalendarValue.value = uni_modules_wotDesignUni_components_common_util.deepClone(calendarValue.value);
      lastTab.value = currentTab.value;
      lastCurrentType.value = currentType.value;
      uni_modules_wotDesignUni_components_common_util.requestAnimationFrame(() => {
        scrollIntoView();
      });
      setTimeout(() => {
        if (props.showTypeSwitch) {
          calendarTabs.value.scrollIntoView();
          calendarTabs.value.updateLineStyle(false);
        }
      }, 250);
    }
    function close() {
      pickerShow.value = false;
      setTimeout(() => {
        calendarValue.value = uni_modules_wotDesignUni_components_common_util.deepClone(lastCalendarValue.value);
        currentTab.value = lastTab.value;
        currentType.value = lastCurrentType.value || "date";
        confirmBtnDisabled.value = getConfirmBtnStatus(lastCalendarValue.value);
      }, 250);
      emit("cancel");
    }
    function handleTypeChange({ index }) {
      const tabs = ["date", "week", "month"];
      const rangeTabs = ["daterange", "weekrange", "monthrange"];
      const type = props.type.indexOf("range") > -1 ? rangeTabs[index] : tabs[index];
      currentTab.value = index;
      currentType.value = type;
    }
    function getConfirmBtnStatus(value) {
      let confirmBtnDisabled2 = false;
      if (props.type.indexOf("range") > -1 && (!uni_modules_wotDesignUni_components_common_util.isArray(value) || !value[0] || !value[1] || !value) || props.type === "dates" && (!uni_modules_wotDesignUni_components_common_util.isArray(value) || value.length === 0 || !value) || !value) {
        confirmBtnDisabled2 = true;
      }
      return confirmBtnDisabled2;
    }
    function handleChange({ value }) {
      calendarValue.value = uni_modules_wotDesignUni_components_common_util.deepClone(value);
      confirmBtnDisabled.value = getConfirmBtnStatus(value);
      emit("change", {
        value
      });
      if (!props.showConfirm && !confirmBtnDisabled.value) {
        handleConfirm();
      }
    }
    function handleConfirm() {
      if (props.beforeConfirm) {
        props.beforeConfirm({
          value: calendarValue.value,
          resolve: (isPass) => {
            isPass && onConfirm();
          }
        });
      } else {
        onConfirm();
      }
    }
    function onConfirm() {
      pickerShow.value = false;
      lastCurrentType.value = currentType.value;
      emit("update:modelValue", calendarValue.value);
      emit("confirm", {
        value: calendarValue.value
      });
    }
    function handleShortcutClick(index) {
      if (props.onShortcutsClick && typeof props.onShortcutsClick === "function") {
        calendarValue.value = uni_modules_wotDesignUni_components_common_util.deepClone(
          props.onShortcutsClick({
            item: props.shortcuts[index],
            index
          })
        );
        confirmBtnDisabled.value = getConfirmBtnStatus(calendarValue.value);
      }
      if (!props.showConfirm) {
        handleConfirm();
      }
    }
    __expose({
      close,
      open
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: _ctx.useDefaultSlot
      }, _ctx.useDefaultSlot ? {} : common_vendor.e({
        b: _ctx.label || _ctx.useLabelSlot
      }, _ctx.label || _ctx.useLabelSlot ? common_vendor.e({
        c: _ctx.label
      }, _ctx.label ? {
        d: common_vendor.t(_ctx.label)
      } : {}, {
        e: common_vendor.n(`wd-calendar__label ${isRequired.value ? "is-required" : ""} ${_ctx.customLabelClass}`),
        f: common_vendor.s(_ctx.labelWidth ? "min-width:" + _ctx.labelWidth + ";max-width:" + _ctx.labelWidth + ";" : "")
      }) : {}, {
        g: common_vendor.t(showValue.value || _ctx.placeholder || common_vendor.unref(translate)("placeholder")),
        h: common_vendor.n(`wd-calendar__value ${_ctx.ellipsis ? "is-ellipsis" : ""} ${_ctx.customValueClass} ${showValue.value ? "" : "wd-calendar__value--placeholder"}`),
        i: !_ctx.disabled && !_ctx.readonly
      }, !_ctx.disabled && !_ctx.readonly ? {
        j: common_vendor.p({
          ["custom-class"]: "wd-calendar__arrow",
          name: "arrow-right"
        })
      } : {}, {
        k: errorMessage.value
      }, errorMessage.value ? {
        l: common_vendor.t(errorMessage.value)
      } : {}, {
        m: common_vendor.n(`wd-calendar__cell ${_ctx.disabled ? "is-disabled" : ""} ${_ctx.readonly ? "is-readonly" : ""} ${_ctx.alignRight ? "is-align-right" : ""} ${_ctx.error ? "is-error" : ""} ${_ctx.size ? "is-" + _ctx.size : ""} ${_ctx.center ? "is-center" : ""}`)
      }), {
        n: common_vendor.o(open),
        o: !_ctx.showTypeSwitch && _ctx.shortcuts.length === 0
      }, !_ctx.showTypeSwitch && _ctx.shortcuts.length === 0 ? {
        p: common_vendor.t(_ctx.title || common_vendor.unref(translate)("title"))
      } : {}, {
        q: _ctx.showTypeSwitch
      }, _ctx.showTypeSwitch ? {
        r: common_vendor.p({
          title: common_vendor.unref(translate)("day"),
          name: common_vendor.unref(translate)("day")
        }),
        s: common_vendor.p({
          title: common_vendor.unref(translate)("week"),
          name: common_vendor.unref(translate)("week")
        }),
        t: common_vendor.p({
          title: common_vendor.unref(translate)("month"),
          name: common_vendor.unref(translate)("month")
        }),
        v: common_vendor.sr(calendarTabs, "4602e815-2,4602e815-1", {
          "k": "calendarTabs"
        }),
        w: common_vendor.o(handleTypeChange),
        x: common_vendor.o(($event) => currentTab.value = $event),
        y: common_vendor.p({
          modelValue: currentTab.value
        })
      } : {}, {
        z: _ctx.shortcuts.length > 0
      }, _ctx.shortcuts.length > 0 ? {
        A: common_vendor.f(_ctx.shortcuts, (item, index, i0) => {
          return {
            a: common_vendor.t(item.text),
            b: index,
            c: common_vendor.o(($event) => handleShortcutClick(index), index),
            d: "4602e815-6-" + i0 + ",4602e815-1"
          };
        }),
        B: common_vendor.p({
          ["custom-class"]: "wd-calendar__tag",
          type: "primary",
          plain: true,
          round: true
        })
      } : {}, {
        C: common_vendor.o(close),
        D: common_vendor.p({
          ["custom-class"]: "wd-calendar__close",
          name: "add"
        }),
        E: inited.value
      }, inited.value ? common_vendor.e({
        F: range.value(_ctx.type)
      }, range.value(_ctx.type) ? {
        G: common_vendor.t(rangeLabel.value[0]),
        H: common_vendor.n(`wd-calendar__range-label-item ${!calendarValue.value || !common_vendor.unref(uni_modules_wotDesignUni_components_common_util.isArray)(calendarValue.value) || !calendarValue.value[0] ? "is-placeholder" : ""}`),
        I: common_vendor.t(rangeLabel.value[1]),
        J: common_vendor.n(`wd-calendar__range-label-item ${!calendarValue.value || !common_vendor.unref(uni_modules_wotDesignUni_components_common_util.isArray)(calendarValue.value) || !calendarValue.value[1] ? "is-placeholder" : ""}`),
        K: common_vendor.n(`wd-calendar__range-label ${_ctx.type === "monthrange" ? "is-monthrange" : ""}`)
      } : {}, {
        L: common_vendor.sr(calendarView, "4602e815-8,4602e815-1", {
          "k": "calendarView"
        }),
        M: common_vendor.o(handleChange),
        N: common_vendor.o(($event) => calendarValue.value = $event),
        O: common_vendor.p({
          type: currentType.value,
          ["min-date"]: _ctx.minDate,
          ["max-date"]: _ctx.maxDate,
          ["first-day-of-week"]: _ctx.firstDayOfWeek,
          formatter: _ctx.formatter,
          ["panel-height"]: panelHeight.value,
          ["max-range"]: _ctx.maxRange,
          ["range-prompt"]: _ctx.rangePrompt,
          ["allow-same-day"]: _ctx.allowSameDay,
          ["default-time"]: _ctx.defaultTime,
          ["time-filter"]: _ctx.timeFilter,
          ["hide-second"]: _ctx.hideSecond,
          ["show-panel-title"]: !range.value(_ctx.type),
          modelValue: calendarValue.value
        }),
        P: common_vendor.n(`wd-calendar__view  ${currentType.value.indexOf("range") > -1 ? "is-range" : ""} ${_ctx.showConfirm ? "is-show-confirm" : ""}`)
      }) : {}, {
        Q: _ctx.showConfirm
      }, _ctx.showConfirm ? {
        R: common_vendor.t(_ctx.confirmText || common_vendor.unref(translate)("confirm")),
        S: common_vendor.o(handleConfirm),
        T: common_vendor.p({
          block: true,
          disabled: confirmBtnDisabled.value
        })
      } : {}, {
        U: common_vendor.o(close),
        V: common_vendor.o(($event) => pickerShow.value = $event),
        W: common_vendor.p({
          duration: 250,
          ["close-on-click-modal"]: _ctx.closeOnClickModal,
          ["safe-area-inset-bottom"]: _ctx.safeAreaInsetBottom,
          ["z-index"]: _ctx.zIndex,
          modelValue: pickerShow.value
        }),
        X: common_vendor.n(`wd-calendar ${common_vendor.unref(cell).border.value ? "is-border" : ""} ${_ctx.customClass}`)
      });
    };
  }
});
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-4602e815"]]);
wx.createComponent(Component);
