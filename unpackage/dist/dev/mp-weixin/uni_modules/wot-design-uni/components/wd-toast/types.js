"use strict";
const uni_modules_wotDesignUni_components_common_props = require("../common/props.js");
const toastProps = {
  ...uni_modules_wotDesignUni_components_common_props.baseProps,
  customIconClass: uni_modules_wotDesignUni_components_common_props.makeStringProp(""),
  selector: uni_modules_wotDesignUni_components_common_props.makeStringProp("")
};
exports.toastProps = toastProps;
