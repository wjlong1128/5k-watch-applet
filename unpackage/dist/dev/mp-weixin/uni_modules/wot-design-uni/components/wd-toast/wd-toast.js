"use strict";
const common_vendor = require("../../../../common/vendor.js");
const uni_modules_wotDesignUni_components_common_base64 = require("../common/base64.js");
const uni_modules_wotDesignUni_components_wdToast_index = require("./index.js");
const uni_modules_wotDesignUni_components_wdToast_types = require("./types.js");
const uni_modules_wotDesignUni_components_common_util = require("../common/util.js");
if (!Array) {
  const _easycom_wd_overlay2 = common_vendor.resolveComponent("wd-overlay");
  const _easycom_wd_loading2 = common_vendor.resolveComponent("wd-loading");
  const _easycom_wd_transition2 = common_vendor.resolveComponent("wd-transition");
  (_easycom_wd_overlay2 + _easycom_wd_loading2 + _easycom_wd_transition2)();
}
const _easycom_wd_overlay = () => "../wd-overlay/wd-overlay.js";
const _easycom_wd_loading = () => "../wd-loading/wd-loading.js";
const _easycom_wd_transition = () => "../wd-transition/wd-transition.js";
if (!Math) {
  (_easycom_wd_overlay + _easycom_wd_loading + _easycom_wd_transition)();
}
const __default__ = {
  name: "wd-toast",
  options: {
    addGlobalClass: true,
    virtualHost: true,
    styleIsolation: "shared"
  }
};
const _sfc_main = /* @__PURE__ */ common_vendor.defineComponent({
  ...__default__,
  props: uni_modules_wotDesignUni_components_wdToast_types.toastProps,
  setup(__props) {
    const props = __props;
    const iconName = common_vendor.ref("");
    const customIcon = common_vendor.ref(false);
    const msg = common_vendor.ref("");
    const position = common_vendor.ref("middle");
    const show = common_vendor.ref(false);
    const zIndex = common_vendor.ref(100);
    const loadingType = common_vendor.ref("outline");
    const loadingColor = common_vendor.ref("#4D80F0");
    const iconSize = common_vendor.ref(42);
    const svgStr = common_vendor.ref("");
    const cover = common_vendor.ref(false);
    let opened = null;
    let closed = null;
    const toastOptionKey = props.selector ? uni_modules_wotDesignUni_components_wdToast_index.toastDefaultOptionKey + props.selector : uni_modules_wotDesignUni_components_wdToast_index.toastDefaultOptionKey;
    const toastOption = common_vendor.inject(toastOptionKey, common_vendor.ref(uni_modules_wotDesignUni_components_wdToast_index.defaultOptions));
    common_vendor.watch(
      () => toastOption.value,
      (newVal) => {
        reset(newVal);
      },
      {
        deep: true,
        immediate: true
      }
    );
    common_vendor.watch(
      () => iconName.value,
      () => {
        buildSvg();
      },
      {
        deep: true,
        immediate: true
      }
    );
    const transitionStyle = common_vendor.computed(() => {
      const style = {
        "z-index": zIndex.value,
        position: "fixed",
        top: "50%",
        left: 0,
        width: "100%",
        transform: "translate(0, -50%)",
        "text-align": "center"
      };
      return uni_modules_wotDesignUni_components_common_util.objToStyle(style);
    });
    const loadingStyle = common_vendor.computed(() => {
      const style = {
        display: "inline-block",
        "margin-right": "16px"
      };
      return uni_modules_wotDesignUni_components_common_util.objToStyle(style);
    });
    const rootClass = common_vendor.computed(() => {
      return `wd-toast ${props.customClass} wd-toast--${position.value} ${(iconName.value !== "loading" || msg.value) && (iconName.value || customIcon.value) ? "wd-toast--with-icon" : ""} ${iconName.value === "loading" && !msg.value ? "wd-toast--loading" : ""}`;
    });
    common_vendor.onBeforeMount(() => {
      buildSvg();
    });
    function handleAfterEnter() {
      if (uni_modules_wotDesignUni_components_common_util.isFunction(opened)) {
        opened();
      }
    }
    function handleAfterLeave() {
      if (uni_modules_wotDesignUni_components_common_util.isFunction(closed)) {
        closed();
      }
    }
    function buildSvg() {
      if (iconName.value !== "success" && iconName.value !== "warning" && iconName.value !== "info" && iconName.value !== "error")
        return;
      const iconSvg = uni_modules_wotDesignUni_components_wdToast_index.toastIcon[iconName.value]();
      const iconSvgStr = `"data:image/svg+xml;base64,${uni_modules_wotDesignUni_components_common_base64.encode(iconSvg)}"`;
      svgStr.value = iconSvgStr;
    }
    function reset(option) {
      if (option) {
        show.value = uni_modules_wotDesignUni_components_common_util.isDef(option.show) ? option.show : false;
        if (show.value) {
          iconName.value = uni_modules_wotDesignUni_components_common_util.isDef(option.iconName) ? option.iconName : "";
          customIcon.value = uni_modules_wotDesignUni_components_common_util.isDef(option.customIcon) ? option.customIcon : false;
          msg.value = uni_modules_wotDesignUni_components_common_util.isDef(option.msg) ? option.msg : "";
          position.value = uni_modules_wotDesignUni_components_common_util.isDef(option.position) ? option.position : "middle";
          zIndex.value = uni_modules_wotDesignUni_components_common_util.isDef(option.zIndex) ? option.zIndex : 100;
          loadingType.value = uni_modules_wotDesignUni_components_common_util.isDef(option.loadingType) ? option.loadingType : "outline";
          loadingColor.value = uni_modules_wotDesignUni_components_common_util.isDef(option.loadingColor) ? option.loadingColor : "#4D80F0";
          iconSize.value = uni_modules_wotDesignUni_components_common_util.isDef(option.iconSize) ? option.iconSize : 42;
          cover.value = uni_modules_wotDesignUni_components_common_util.isDef(option.cover) ? option.cover : false;
          closed = uni_modules_wotDesignUni_components_common_util.isFunction(option.closed) ? option.closed : null;
          opened = uni_modules_wotDesignUni_components_common_util.isFunction(option.opened) ? option.opened : null;
        }
      }
    }
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: cover.value
      }, cover.value ? {
        b: common_vendor.p({
          ["z-index"]: zIndex.value,
          ["lock-scroll"]: true,
          show: show.value,
          ["custom-style"]: "background-color: transparent;pointer-events: auto;"
        })
      } : {}, {
        c: iconName.value === "loading"
      }, iconName.value === "loading" ? {
        d: common_vendor.p({
          type: loadingType.value,
          color: loadingColor.value,
          ["custom-class"]: "wd-toast__icon",
          customStyle: loadingStyle.value
        })
      } : iconName.value === "success" || iconName.value === "warning" || iconName.value === "info" || iconName.value === "error" ? {
        f: common_vendor.s(`background-image: url(${svgStr.value}); width:${iconSize.value}px; height:${iconSize.value}px`),
        g: common_vendor.s(`width:${iconSize.value}px; height:${iconSize.value}px`)
      } : customIcon.value ? {} : {}, {
        e: iconName.value === "success" || iconName.value === "warning" || iconName.value === "info" || iconName.value === "error",
        h: customIcon.value,
        i: msg.value
      }, msg.value ? {
        j: common_vendor.t(msg.value)
      } : {}, {
        k: common_vendor.n(rootClass.value),
        l: common_vendor.o(handleAfterEnter),
        m: common_vendor.o(handleAfterLeave),
        n: common_vendor.p({
          name: "fade",
          show: show.value,
          ["custom-style"]: transitionStyle.value
        })
      });
    };
  }
});
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-fce8c80a"]]);
wx.createComponent(Component);
