"use strict";
const common_vendor = require("../../common/vendor.js");
const lib_hooks_lang = require("../../lib/hooks/lang.js");
const _sfc_main = {
  __name: "change-lang",
  emits: ["changeAfter"],
  setup(__props, { emit: __emit }) {
    const emits = __emit;
    const langArray = common_vendor.ref([
      {
        title: "中文",
        key: "zh-Hans"
      },
      {
        title: "English",
        key: "en"
      }
    ]);
    const {
      changeLang
    } = lib_hooks_lang.useChangeLang();
    const showLangChange = () => {
      common_vendor.index.showActionSheet({
        itemList: langArray.value.map((item) => item.title),
        success({
          tapIndex
        }) {
          const lang = langArray.value[tapIndex].key;
          changeLang(lang);
          emits("changeAfter");
        }
      });
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.t(_ctx.$t("local.ChangeLanguage")),
        b: common_vendor.o(showLangChange)
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-2f5a81a9"]]);
wx.createComponent(Component);
