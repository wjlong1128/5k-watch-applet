"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  name: "DeviceElectricity",
  props: {
    num: {
      type: Number,
      default: 0
    }
  },
  data() {
    return {
      // 电量数值
      eleNum: 0,
      // 电量宽度
      electricityWidth: "0px"
    };
  },
  watch: {
    // 监听电量变化
    num: {
      immediate: true,
      handler(newValue) {
        this.eleNum = this.num;
        this.electricityWidth = `calc(${this.num}% - 4rpx)`;
      }
    }
  },
  mounted() {
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: $data.electricityWidth,
    b: $data.eleNum < 20 ? "red" : "#5f5f5f",
    c: common_vendor.t($data.eleNum)
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-0a01347e"]]);
wx.createComponent(Component);
