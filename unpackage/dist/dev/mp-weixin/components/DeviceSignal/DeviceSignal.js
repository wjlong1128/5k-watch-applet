"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  name: "DeviceSignal",
  props: {
    num: {
      type: Number,
      default: 0
    }
  },
  data() {
    return {
      signalNum: 0
    };
  },
  watch: {
    // 监听信号变化
    num: {
      immediate: true,
      handler(newValue) {
        this.signalNum = this.num;
      }
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: $data.signalNum >= 1 ? "#5f5f5f" : "rgba(255,255,256,0)",
    b: $data.signalNum >= 2 ? "#5f5f5f" : "rgba(255,255,256,0)",
    c: $data.signalNum >= 3 ? "#5f5f5f" : "rgba(255,255,256,0)",
    d: $data.signalNum >= 4 ? "#5f5f5f" : "rgba(255,255,256,0)",
    e: $data.signalNum >= 5 ? "#5f5f5f" : "rgba(255,255,256,0)"
  };
}
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__scopeId", "data-v-ac28fa71"]]);
wx.createComponent(Component);
