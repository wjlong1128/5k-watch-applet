"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  __name: "custom-tabbar",
  props: {
    currentPage: Number
  },
  setup(__props) {
    const props = __props;
    const data = common_vendor.reactive({
      tabbarList: [
        {
          "text": "page.index",
          "iconPath": "../../static/images/tabbar/index.png",
          "selectedIconPath": "../../static/images/tabbar/index-selected.png",
          "pagePath": "/pages/index/index"
        },
        {
          "text": "page.report",
          "iconPath": "../../static/images/tabbar/report.png",
          "selectedIconPath": "../../static/images/tabbar/report-selected.png",
          "pagePath": "/pages/report/report"
        },
        {
          "text": "page.my",
          "iconPath": "../../static/images/tabbar/my.png",
          "selectedIconPath": "../../static/images/tabbar/my-selected.png",
          "pagePath": "/pages/my/my"
        }
      ]
    });
    const changeTabbar = function(item) {
      common_vendor.index.switchTab({
        url: item.pagePath
      });
    };
    common_vendor.onMounted(() => {
      common_vendor.index.hideTabBar();
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.f(data.tabbarList, (item, index, i0) => {
          return {
            a: index == props.currentPage ? item.selectedIconPath : item.iconPath,
            b: common_vendor.t(_ctx.$t(item.text)),
            c: common_vendor.s(index == props.currentPage ? {
              color: "#2186f0"
            } : ""),
            d: index,
            e: common_vendor.o(($event) => changeTabbar(item), index)
          };
        })
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-51c48e3c"]]);
wx.createComponent(Component);
