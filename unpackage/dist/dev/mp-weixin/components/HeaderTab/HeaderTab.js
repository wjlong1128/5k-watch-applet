"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  __name: "HeaderTab",
  props: {
    tabs: {
      type: Array,
      default: []
    },
    modelValue: String,
    modelKey: {
      type: String,
      default: "name"
    }
  },
  emits: ["update:modelValue", "click"],
  setup(__props, { emit: __emit }) {
    common_vendor.useCssVars((_ctx) => ({
      "28f6444e": props.tabs.length
    }));
    const emits = __emit;
    const props = __props;
    const change = (item, index) => {
      common_vendor.index.vibrateShort();
      emits("update:modelValue", item[props.modelKey]);
      props.nameKey || "name";
      emits("click", item, index);
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.f(_ctx.$props.tabs, (item, index, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.o(($event) => change(item, index), index),
            c: index,
            d: common_vendor.n({
              "is-active": _ctx.$props.modelValue == item[_ctx.$props.modelKey]
            })
          };
        }),
        b: common_vendor.s(_ctx.__cssVars())
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-4e2589e9"]]);
wx.createComponent(Component);
