"use strict";
const common_vendor = require("../../common/vendor.js");
const lib_constants = require("../../lib/constants.js");
const _sfc_main = {
  __name: "privacy-authoriza",
  setup(__props) {
    const {
      t
    } = common_vendor.useI18n();
    const authorizationFun = common_vendor.ref(null);
    const show = common_vendor.ref(false);
    const contractName = common_vendor.ref("");
    const isRead = common_vendor.ref(false);
    common_vendor.onMounted(() => {
      if (common_vendor.index.onNeedPrivacyAuthorization) {
        common_vendor.index.onNeedPrivacyAuthorization((resolve) => {
          console.log("uni.onNeedPrivacyAuthorization.resolve", resolve);
          authorizationFun.value = resolve;
        });
      }
      if (common_vendor.index.getPrivacySetting) {
        common_vendor.index.showLoading({
          title: t("privact.authoriza.loading")
        });
        common_vendor.index.getPrivacySetting({
          // 是否需要用户授权隐私协议，隐私授权协议的名称
          success: ({
            needAuthorization,
            privacyContractName
          }) => {
            contractName.value = privacyContractName;
            show.value = needAuthorization;
            common_vendor.index.hideLoading();
          }
        });
      }
    });
    const openPrivacyContract = () => {
      common_vendor.index.openPrivacyContract({
        success: () => {
          isRead.value = true;
        },
        fail: () => {
          common_vendor.index.showToast({
            title: "遇到错误",
            icon: "error"
          });
        }
      });
    };
    const handleAgreePrivacyAuthorization = () => {
      if (typeof authorizationFun.value == "function") {
        authorizationFun.value({
          buttonId: "agree-btn",
          event: "agree"
        });
      }
      common_vendor.index.setStorageSync(lib_constants.storeKeys.privateAgreement, "yes");
      show.value = false;
    };
    const reject = () => {
      common_vendor.wx$1.exitMiniProgram();
    };
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: show.value
      }, show.value ? {
        b: common_vendor.t(_ctx.$t("UserPrivacyProtectionGuidelines")),
        c: common_vendor.t(_ctx.$t("PrivacyTextOne")),
        d: common_vendor.t(contractName.value),
        e: common_vendor.o(openPrivacyContract),
        f: common_vendor.t(_ctx.$t("PrivacyTextTow")),
        g: common_vendor.t(_ctx.$t("universal.refuse")),
        h: common_vendor.o(reject),
        i: common_vendor.t(_ctx.$t("universal.agree")),
        j: common_vendor.o(handleAgreePrivacyAuthorization)
      } : {});
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-9277e411"]]);
wx.createComponent(Component);
