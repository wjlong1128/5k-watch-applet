"use strict";
const common_vendor = require("../../common/vendor.js");
const lib_constants = require("../constants.js");
function useChangeLang(callback) {
  const {
    proxy
  } = common_vendor.getCurrentInstance();
  const changeLang = (lang) => {
    common_vendor.index.setLocale(lang);
    proxy.$i18n.locale = lang;
    common_vendor.index.setStorageSync(lib_constants.storeKeys.langKey, lang);
    callback && callback();
  };
  return {
    changeLang
  };
}
exports.useChangeLang = useChangeLang;
