"use strict";
const storeKeys = {
  langKey: "locale-lang",
  token: "5k-token",
  openId: "openId",
  wechatInfo: "c-wechatInfo",
  userInfo: "c-userInfo",
  imei: "imei",
  selectAgreement: "c-selectAgreement",
  privateAgreement: "c-privateAgreement"
};
const selectAgreementStatus = [
  "y",
  // 未选中
  "n"
  // 选中
];
exports.selectAgreementStatus = selectAgreementStatus;
exports.storeKeys = storeKeys;
