"use strict";
function getNumberArray(start, end) {
  const result = [];
  for (let i = start; i <= end; i++) {
    result.push(i);
  }
  return result;
}
function getStartDate(year, month = 1, day = 1) {
  month = month - 1;
  return new Date(year, month, day);
}
function formatTimestampToStr(timestamp) {
  let date = new Date(timestamp);
  let year = date.getFullYear();
  let month = String(date.getMonth() + 1).padStart(2, "0");
  let day = String(date.getDate()).padStart(2, "0");
  let hours = String(date.getHours()).padStart(2, "0");
  let minutes = String(date.getMinutes()).padStart(2, "0");
  let seconds = String(date.getSeconds()).padStart(2, "0");
  return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}
function isNumber(input) {
  if (!isNaN(input)) {
    if (parseFloat(input) === Number(input)) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}
exports.formatTimestampToStr = formatTimestampToStr;
exports.getNumberArray = getNumberArray;
exports.getStartDate = getStartDate;
exports.isNumber = isNumber;
