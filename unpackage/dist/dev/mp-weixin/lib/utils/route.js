"use strict";
const common_vendor = require("../../common/vendor.js");
const lib_constants = require("../constants.js");
const whiteList = [
  "/",
  // 注意入口页必须直接写 '/'
  "/pages/index/index",
  "/pages/report/report",
  "/pages/my/my",
  "/pages/login/login",
  "/sub_my_page/userAgreement/userAgreement",
  "/sub_my_page/bluetooth-connect/bluetooth-connect"
];
async function route() {
  const list = ["navigateTo", "redirectTo", "reLaunch", "switchTab"];
  list.forEach((item) => {
    common_vendor.index.addInterceptor(item, {
      invoke(e) {
        const url = e.url.split("?")[0];
        let pass;
        if (whiteList) {
          pass = whiteList.some((item2) => {
            if (typeof item2 === "object" && item2.pattern) {
              return item2.pattern.test(url);
            }
            return url === item2;
          });
        }
        if (!pass && !common_vendor.index.getStorageSync(lib_constants.storeKeys.token)) {
          common_vendor.index.navigateTo({
            url: "/pages/login/login",
            success: () => {
              common_vendor.index.showToast({
                title: common_vendor.index.getLocale() == "zh-Hans" ? "请先登入" : "please log in first",
                icon: "none"
              });
            }
          });
          return false;
        }
        return e;
      },
      fail(err) {
        console.log(err);
      }
    });
  });
}
exports.route = route;
