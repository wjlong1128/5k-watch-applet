"use strict";
const common_vendor = require("../../common/vendor.js");
require("../../store/user.js");
const lib_api_user = require("../api/user.js");
const lib_constants = require("../constants.js");
const defauls = {
  method: "POST",
  loading: true
};
function logoutAfter() {
  common_vendor.index.removeStorageSync(lib_constants.storeKeys.langKey);
  common_vendor.index.removeStorageSync(lib_constants.storeKeys.wechatInfo);
  common_vendor.index.removeStorageSync(lib_constants.storeKeys.token);
  common_vendor.index.removeStorageSync(lib_constants.storeKeys.openId);
  common_vendor.index.removeStorageSync(lib_constants.storeKeys.imei);
}
const BASE_URL = "https://prod.sx5k.com";
const request = (path, data = {}, config = defauls, reqconfig = {}) => {
  const token = common_vendor.index.getStorageSync(lib_constants.storeKeys.token);
  const Authorization = token ? `Bearer ${token}` : "";
  if (config.loading) {
    common_vendor.index.showLoading({
      title: common_vendor.index.getLocale().startsWith("zh") ? "加载中" : "loading",
      mask: true
    });
  }
  return new Promise((resolve, reject) => {
    common_vendor.index.request({
      ...reqconfig,
      header: {
        Authorization
      },
      url: BASE_URL + path,
      method: config.method,
      data,
      async success(response) {
        if (response.data.code === 3e3 || response.data.code === 403 || response.data.code === 1009) {
          await lib_api_user.logOutApi();
          logoutAfter();
          common_vendor.index.reLaunch({
            url: `/pages/my/my?msg=${common_vendor.index.getLocale().startsWith("zh") ? "登录状态已过期或失效，请重新登录" : "Login status has expired or expired, please log in again"}`
          });
          reject(response.data.msg);
        } else if (response.data.code === 1037) {
          resolve(response.data);
        } else if (response.data.code === 1038) {
          setTimeout(() => {
            common_vendor.index.showToast({
              icon: "none",
              duration: 4e3,
              title: common_vendor.index.getLocale().startsWith("zh") ? "绑定手表失败，请扫描正确的二维码" : "Binding watch failed, please scan the correct QR code"
            });
          }, 500);
          reject(response.data);
        } else if (response.data.code === 201) {
          resolve(response.data);
        } else if (response.data.code == 500) {
          setTimeout(() => {
            common_vendor.index.showToast({
              icon: "none",
              duration: 5e3,
              title: response.data.msg === "绑定失败,二维码有误" && common_vendor.index.getLocale().startsWith("zh") ? response.data.msg : "Binding watch failed, please scan the correct QR code"
            });
          }, 500);
          reject(response.data.msg);
        } else if (response.data.code !== 200) {
          common_vendor.index.showToast({
            icon: "none",
            duration: 4e3,
            title: response.data.msg
          });
          reject(response.data.msg);
        }
        resolve(response.data);
      },
      fail(err) {
        common_vendor.index.showToast({
          icon: "none",
          title: "服务响应失败"
        });
        console.error("服务器响应失败", err);
        reject(err);
      },
      complete() {
        if (config.loading) {
          common_vendor.index.hideLoading();
        }
      }
    });
  });
};
exports.BASE_URL = BASE_URL;
exports.logoutAfter = logoutAfter;
exports.request = request;
