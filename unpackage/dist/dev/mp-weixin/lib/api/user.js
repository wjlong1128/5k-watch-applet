"use strict";
const lib_utils_request = require("../utils/request.js");
function loginApi(data) {
  return lib_utils_request.request("/watch/login", data);
}
function getUserInfoApi(data) {
  return lib_utils_request.request("/personalCenter/getPersonalMsg", {}, { method: "GET", loading: true });
}
function logOutApi() {
  return lib_utils_request.request("/watch/logout", {}, { method: "POST", loading: false });
}
function updateUserInfoApi(data) {
  return lib_utils_request.request(`/personalCenter/modifyPersonalMsg`, data, { method: "POST", loading: true });
}
function bindDeviceApi(imei) {
  return lib_utils_request.request(`/personalCenter/bindWatch`, { imei }, { method: "POST", loading: true });
}
function unBindApi() {
  return lib_utils_request.request(`/personalCenter/unWatchImei`);
}
function getDeviceInfoApi(imei) {
  return lib_utils_request.request(`/MainFeatures/device/info/${imei}`, {}, { method: "GET", loading: true });
}
function getDevicePowerApi(imei) {
  return lib_utils_request.request(`/MainFeatures/device/power/${imei}`, {}, { method: "GET", loading: false });
}
function getDeviceLoactionApi() {
  return lib_utils_request.request("/MainFeatures/device/location", {}, { method: "GET", loading: true });
}
exports.bindDeviceApi = bindDeviceApi;
exports.getDeviceInfoApi = getDeviceInfoApi;
exports.getDeviceLoactionApi = getDeviceLoactionApi;
exports.getDevicePowerApi = getDevicePowerApi;
exports.getUserInfoApi = getUserInfoApi;
exports.logOutApi = logOutApi;
exports.loginApi = loginApi;
exports.unBindApi = unBindApi;
exports.updateUserInfoApi = updateUserInfoApi;
