"use strict";
const common_vendor = require("../../common/vendor.js");
const sub_my_page_lib_api = require("../lib/api.js");
if (!Array) {
  const _easycom_wd_picker2 = common_vendor.resolveComponent("wd-picker");
  const _easycom_wd_input2 = common_vendor.resolveComponent("wd-input");
  const _easycom_wd_textarea2 = common_vendor.resolveComponent("wd-textarea");
  const _easycom_wd_button2 = common_vendor.resolveComponent("wd-button");
  (_easycom_wd_picker2 + _easycom_wd_input2 + _easycom_wd_textarea2 + _easycom_wd_button2)();
}
const _easycom_wd_picker = () => "../../uni_modules/wot-design-uni/components/wd-picker/wd-picker.js";
const _easycom_wd_input = () => "../../uni_modules/wot-design-uni/components/wd-input/wd-input.js";
const _easycom_wd_textarea = () => "../../uni_modules/wot-design-uni/components/wd-textarea/wd-textarea.js";
const _easycom_wd_button = () => "../../uni_modules/wot-design-uni/components/wd-button/wd-button.js";
if (!Math) {
  (_easycom_wd_picker + _easycom_wd_input + _easycom_wd_textarea + _easycom_wd_button)();
}
const __default__ = {
  options: {
    // 微信小程序中 options 选项  
    options: {
      styleIsolation: "shared"
    }
  }
};
const _sfc_main = /* @__PURE__ */ Object.assign(__default__, {
  __name: "sendMsg",
  setup(__props) {
    const {
      t
    } = common_vendor.useI18n();
    const changeTitle = () => {
      common_vendor.index.setNavigationBarTitle({
        title: t("my.sendMsg")
      });
    };
    common_vendor.onLoad(() => {
      changeTitle();
    });
    const columns = common_vendor.ref([
      t("const.custom"),
      t("const.dad"),
      t("const.mom"),
      t("const.olderBrother"),
      t("const.youngerBrother"),
      t("const.olderSister"),
      t("const.youngerSister")
    ]);
    const msg = common_vendor.ref("");
    const sender = common_vendor.ref("");
    const isCustomSender = common_vendor.ref(false);
    const customSender = common_vendor.ref("");
    common_vendor.watch(sender, (newValue) => {
      if (newValue == columns.value[0]) {
        isCustomSender.value = true;
      } else {
        isCustomSender.value = false;
        customSender.value = "";
      }
    });
    const sendBtn = common_vendor.ref(false);
    const sendMsg = async () => {
      let sendUser, msgData;
      if (isCustomSender.value) {
        sendUser = customSender.value;
      } else {
        sendUser = sender.value;
      }
      if (!sendUser) {
        common_vendor.index.showToast({
          title: t("sendMsg.senderNotEmpty"),
          duration: 2e3
        });
        return;
      }
      msgData = msg.value;
      if (!msgData) {
        common_vendor.index.showToast({
          title: t("sendMsg.messageNotEmpty"),
          duration: 2e3
        });
        return;
      }
      try {
        sendBtn.value = true;
        await sub_my_page_lib_api.sendMsgApi({
          msgData,
          sendUser
        });
        common_vendor.index.showToast({
          title: t("sendMsg.sendSuccess"),
          duration: 2e3,
          complete() {
            setTimeout(() => {
              common_vendor.index.navigateBack();
            }, 2e3);
          }
        });
      } catch (e) {
        common_vendor.index.showToast({
          title: t("sendMsg.sendFail"),
          duration: 2e3,
          complete() {
            common_vendor.index.navigateBack();
          }
        });
      } finally {
        sendBtn.value = false;
      }
    };
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.o(($event) => sender.value = $event),
        b: common_vendor.p({
          placeholder: _ctx.$t("sendMsg.pleaseSelect"),
          label: _ctx.$t("sendMsg.sender"),
          error: true,
          columns: columns.value,
          required: true,
          modelValue: sender.value
        }),
        c: isCustomSender.value
      }, isCustomSender.value ? {
        d: common_vendor.o(($event) => customSender.value = $event),
        e: common_vendor.p({
          placeholder: _ctx.$t("sendMsg.pleaseInput"),
          label: _ctx.$t("sendMsg.sender"),
          required: true,
          modelValue: customSender.value
        })
      } : {}, {
        f: common_vendor.o(($event) => msg.value = $event),
        g: common_vendor.p({
          ["show-word-limit"]: true,
          maxlength: 50,
          size: "large",
          clearable: true,
          ["auto-focus"]: true,
          placeholder: _ctx.$t("sendMsg.pleaseInputMsg"),
          modelValue: msg.value
        }),
        h: common_vendor.o(sendMsg),
        i: common_vendor.p({
          disabled: sendBtn.value,
          size: "large"
        })
      });
    };
  }
});
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-cb52eeed"]]);
wx.createPage(MiniProgramPage);
