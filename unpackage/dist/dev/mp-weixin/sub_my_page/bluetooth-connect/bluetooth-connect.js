"use strict";
const common_vendor = require("../../common/vendor.js");
const sub_my_page_lib_bluethooth = require("../lib/bluethooth.js");
if (!Array) {
  const _easycom_wd_button2 = common_vendor.resolveComponent("wd-button");
  const _easycom_wd_icon2 = common_vendor.resolveComponent("wd-icon");
  (_easycom_wd_button2 + _easycom_wd_icon2)();
}
const _easycom_wd_button = () => "../../uni_modules/wot-design-uni/components/wd-button/wd-button.js";
const _easycom_wd_icon = () => "../../uni_modules/wot-design-uni/components/wd-icon/wd-icon.js";
if (!Math) {
  (_easycom_wd_button + _easycom_wd_icon + ConnectionView)();
}
const ConnectionView = () => "../lib/components/connection-view/connection-view.js";
const __default__ = {
  options: {
    // 微信小程序中 options 选项  
    options: {
      styleIsolation: "shared"
    }
  }
};
const _sfc_main = /* @__PURE__ */ Object.assign(__default__, {
  __name: "bluetooth-connect",
  setup(__props) {
    common_vendor.useI18n();
    const bluetoothStore = sub_my_page_lib_bluethooth.useBluetoothStore();
    const { isAuth, isEnableBluetooth, errTips, deviceList, isDiscovering, restartLoading, isActive, isConnection, currentDevice } = common_vendor.storeToRefs(bluetoothStore);
    const { authBluetooth, loadBluetooth, startScan, stopScan, closeBluetooth, connect, getDeviceServicesAndCharacteristics } = bluetoothStore;
    const unConnected = () => {
      return !(isActive.value && isConnection.value && currentDevice.value);
    };
    const startluetoothAndScan = () => {
      loadBluetooth(true).then(() => {
        common_vendor.index.getConnectedBluetoothDevices({
          success(res) {
            console.log("已经连接的设备", res);
            startScan().catch((e) => {
              if (e.errCode == 10008) {
                common_vendor.index.showToast({
                  icon: "none",
                  title: "请稍后再试"
                });
              }
            });
          }
        });
      });
    };
    const refresh = () => {
      startluetoothAndScan();
    };
    const connectionDevice = async (device) => {
      common_vendor.index.showLoading();
      connect(device).then(() => {
        common_vendor.index.hideLoading();
        common_vendor.index.showToast({
          icon: "none",
          title: "连接成功",
          duration: 2e3
        });
      }).catch((e) => {
        common_vendor.index.hideLoading();
        console.log("连接失败", e);
        common_vendor.index.showToast({
          icon: "none",
          title: "连接失败",
          duration: 2e3
        });
      }).finally(() => {
      });
    };
    common_vendor.onLoad(() => {
      if (unConnected()) {
        startluetoothAndScan();
        return;
      }
    });
    common_vendor.onUnload(() => {
      unConnected() && closeBluetooth();
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: !common_vendor.unref(isAuth) || !common_vendor.unref(isEnableBluetooth)
      }, !common_vendor.unref(isAuth) || !common_vendor.unref(isEnableBluetooth) ? common_vendor.e({
        b: common_vendor.t(_ctx.$t(common_vendor.unref(errTips))),
        c: !common_vendor.unref(isAuth)
      }, !common_vendor.unref(isAuth) ? {
        d: common_vendor.t(_ctx.$t("bt.auth")),
        e: common_vendor.o(common_vendor.unref(authBluetooth))
      } : {}, {
        f: common_vendor.unref(isAuth) && !common_vendor.unref(isEnableBluetooth)
      }, common_vendor.unref(isAuth) && !common_vendor.unref(isEnableBluetooth) ? {
        g: common_vendor.t(_ctx.$t("bt.refresh")),
        h: common_vendor.o(refresh)
      } : {}) : !common_vendor.unref(isConnection) && !common_vendor.unref(currentDevice) ? {
        j: common_vendor.t(common_vendor.unref(deviceList).length),
        k: common_vendor.p({
          name: "detection",
          size: "22px"
        }),
        l: common_vendor.unref(isDiscovering) ? 1 : "",
        m: common_vendor.f(common_vendor.unref(deviceList), (device, k0, i0) => {
          return {
            a: "499b0446-3-" + i0,
            b: common_vendor.t(device.name),
            c: common_vendor.t(device.deviceId),
            d: common_vendor.o(($event) => connectionDevice(device), device.deviceId),
            e: "499b0446-4-" + i0,
            f: device.deviceId
          };
        }),
        n: common_vendor.p({
          color: "blue",
          name: "bianjiliebiao",
          size: "22px"
        }),
        o: common_vendor.p({
          round: false,
          type: "info"
        })
      } : {}, {
        i: !common_vendor.unref(isConnection) && !common_vendor.unref(currentDevice)
      });
    };
  }
});
wx.createPage(_sfc_main);
