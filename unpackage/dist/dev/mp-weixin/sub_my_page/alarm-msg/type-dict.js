"use strict";
const typeDicts = {
  // 心率
  "0": {
    text: "data.HeartRate"
  },
  // 血压,
  "1": {
    text: "data.BloodPressure"
  },
  // 血氧
  "2": {
    text: "data.BloodOxygen"
  },
  // 体温
  "3": {
    text: "data.BodyTemperature"
  },
  // 低电量
  "4": {
    text: "data.LowBattery"
  },
  // sos求救
  "5": {
    text: "data.Sos"
  }
};
function getAlarmTypeName(type) {
  var _a;
  return (_a = typeDicts[type]) == null ? void 0 : _a.text;
}
exports.getAlarmTypeName = getAlarmTypeName;
