"use strict";
const common_vendor = require("../../common/vendor.js");
const sub_my_page_lib_api = require("../lib/api.js");
const sub_my_page_alarmMsg_typeDict = require("./type-dict.js");
if (!Array) {
  const _easycom_wd_status_tip2 = common_vendor.resolveComponent("wd-status-tip");
  const _easycom_wd_icon2 = common_vendor.resolveComponent("wd-icon");
  (_easycom_wd_status_tip2 + _easycom_wd_icon2)();
}
const _easycom_wd_status_tip = () => "../../uni_modules/wot-design-uni/components/wd-status-tip/wd-status-tip.js";
const _easycom_wd_icon = () => "../../uni_modules/wot-design-uni/components/wd-icon/wd-icon.js";
if (!Math) {
  (_easycom_wd_status_tip + _easycom_wd_icon)();
}
const _sfc_main = {
  __name: "alarm-msg",
  setup(__props) {
    const { t } = common_vendor.useI18n();
    const changeTitle = () => {
      common_vendor.index.setNavigationBarTitle({
        title: t("my.alarm")
      });
    };
    const dataList = common_vendor.ref([]);
    common_vendor.onLoad(async () => {
      changeTitle();
      const {
        data
      } = await sub_my_page_lib_api.getAlarmMsgApi();
      dataList.value = data.map((item) => {
        return {
          time: item.updateTime,
          value: item.value,
          typeName: sub_my_page_alarmMsg_typeDict.getAlarmTypeName(String(item.type))
        };
      });
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: !dataList.value || !dataList.value.length
      }, !dataList.value || !dataList.value.length ? {
        b: common_vendor.p({
          image: "content",
          tip: _ctx.$t("alarm.noalter")
        })
      } : {}, {
        c: common_vendor.f(dataList.value, (item, index, i0) => {
          return {
            a: "d9f6ac61-1-" + i0,
            b: common_vendor.t(_ctx.$t(item.typeName)),
            c: common_vendor.t(item.time),
            d: index
          };
        }),
        d: common_vendor.p({
          color: "#e25f58",
          name: "error-circle-filled",
          size: "22px"
        }),
        e: common_vendor.t(_ctx.$t("alarm.detected"))
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-d9f6ac61"]]);
wx.createPage(MiniProgramPage);
