"use strict";
const common_vendor = require("../../common/vendor.js");
const lib_utils_utils = require("../../lib/utils/utils.js");
const lib_api_user = require("../../lib/api/user.js");
if (!Array) {
  const _easycom_wd_picker2 = common_vendor.resolveComponent("wd-picker");
  const _easycom_wd_datetime_picker2 = common_vendor.resolveComponent("wd-datetime-picker");
  const _easycom_wd_cell_group2 = common_vendor.resolveComponent("wd-cell-group");
  const _easycom_wd_radio2 = common_vendor.resolveComponent("wd-radio");
  const _easycom_wd_radio_group2 = common_vendor.resolveComponent("wd-radio-group");
  const _easycom_wd_button2 = common_vendor.resolveComponent("wd-button");
  const _easycom_wd_form2 = common_vendor.resolveComponent("wd-form");
  (_easycom_wd_picker2 + _easycom_wd_datetime_picker2 + _easycom_wd_cell_group2 + _easycom_wd_radio2 + _easycom_wd_radio_group2 + _easycom_wd_button2 + _easycom_wd_form2)();
}
const _easycom_wd_picker = () => "../../uni_modules/wot-design-uni/components/wd-picker/wd-picker.js";
const _easycom_wd_datetime_picker = () => "../../uni_modules/wot-design-uni/components/wd-datetime-picker/wd-datetime-picker.js";
const _easycom_wd_cell_group = () => "../../uni_modules/wot-design-uni/components/wd-cell-group/wd-cell-group.js";
const _easycom_wd_radio = () => "../../uni_modules/wot-design-uni/components/wd-radio/wd-radio.js";
const _easycom_wd_radio_group = () => "../../uni_modules/wot-design-uni/components/wd-radio-group/wd-radio-group.js";
const _easycom_wd_button = () => "../../uni_modules/wot-design-uni/components/wd-button/wd-button.js";
const _easycom_wd_form = () => "../../uni_modules/wot-design-uni/components/wd-form/wd-form.js";
if (!Math) {
  (_easycom_wd_picker + _easycom_wd_datetime_picker + _easycom_wd_cell_group + _easycom_wd_radio + _easycom_wd_radio_group + _easycom_wd_button + _easycom_wd_form)();
}
const _sfc_main = {
  __name: "set-userinfo",
  setup(__props) {
    const { t } = common_vendor.useI18n();
    const genders = common_vendor.ref([
      {
        value: "0",
        label: t("Gender.Man")
      },
      {
        value: "1",
        label: t("Gender.Woman")
      }
    ]);
    const heights = common_vendor.ref(lib_utils_utils.getNumberArray(120, 220).map((item) => String(item)));
    const weights = common_vendor.ref(lib_utils_utils.getNumberArray(10, 149).map((item) => String(item)));
    const startDate = common_vendor.ref(lib_utils_utils.getStartDate((/* @__PURE__ */ new Date()).getFullYear() - 100).getTime());
    const endTime = common_vendor.ref((/* @__PURE__ */ new Date()).getTime());
    const time = common_vendor.ref(/* @__PURE__ */ new Date());
    const model = common_vendor.reactive({
      gender: "",
      height: "",
      weight: "",
      birthday: lib_utils_utils.formatTimestampToStr(/* @__PURE__ */ new Date()),
      hypertension: 0,
      //高血压
      hyperglycemia: 0,
      //高血糖
      hyperlipidemia: 0,
      //高血脂
      hyperuricemia: 0
      //高尿酸
    });
    const gender = common_vendor.ref("");
    const height = common_vendor.ref("");
    const weight = common_vendor.ref("");
    const birthday = common_vendor.ref(lib_utils_utils.formatTimestampToStr(/* @__PURE__ */ new Date()));
    const hypertension = common_vendor.ref(0);
    const hyperglycemia = common_vendor.ref(0);
    const hyperlipidemia = common_vendor.ref(0);
    const hyperuricemia = common_vendor.ref(0);
    common_vendor.watch(time, (newValue) => {
      birthday.value = lib_utils_utils.formatTimestampToStr(newValue);
    });
    const options = common_vendor.ref("1");
    const changeTitle = (title) => {
      common_vendor.index.setNavigationBarTitle({
        title: t(title)
      });
    };
    const reqInfo = async () => {
      await common_vendor.nextTick$1();
      try {
        const { data } = await lib_api_user.getUserInfoApi();
        data.birthday = data.birthday.replaceAll("-", "/");
        time.value = new Date(data.birthday);
        model.gender = data.gender;
        model.height = data.height;
        model.weight = data.weight;
        console.log("weights", weights.value);
        console.log("heights", heights.value);
        gender.value = String(data.gender);
        height.value = String(data.height);
        weight.value = String(data.weight);
      } catch (e) {
      } finally {
      }
    };
    common_vendor.onLoad(async (query) => {
      options.value = query["opt"] || "1";
      const title = query["title"] || "me.EditProfile";
      changeTitle(title);
      await reqInfo();
    });
    common_vendor.onUnload(() => {
      if (options.value == "1") {
        common_vendor.index.switchTab({
          url: "/pages/my/my"
        });
      }
    });
    const form = common_vendor.ref();
    const err = common_vendor.ref(null);
    async function handleSubmit() {
      if (!gender.value === "" || !gender.value === null || !gender.value === void 0 || !height.value || !height.value || !birthday.value) {
        common_vendor.index.showToast({
          title: t("userinfo.tips"),
          duration: 2e3,
          icon: "error"
        });
        return;
      }
      try {
        const reqData = {
          sex: parseInt(gender.value),
          weight: parseInt(weight.value),
          birthday: birthday.value,
          height: parseInt(height.value),
          hypertension: hypertension.value,
          hyperglycemia: hyperglycemia.value,
          hyperlipidemia: hyperlipidemia.value,
          hyperuricemia: hyperuricemia.value
        };
        await lib_api_user.updateUserInfoApi(reqData);
        err.value = "aaa";
        common_vendor.index.switchTab({
          url: "/pages/my/my"
        });
      } catch (e) {
        err.value = e;
      } finally {
      }
    }
    return (_ctx, _cache) => {
      return {
        a: common_vendor.t(_ctx.$t("userinfo.basicMsg")),
        b: common_vendor.o(($event) => gender.value = $event),
        c: common_vendor.p({
          columns: genders.value,
          label: _ctx.$t("me.Gender"),
          modelValue: gender.value
        }),
        d: common_vendor.o(($event) => height.value = $event),
        e: common_vendor.p({
          columns: heights.value,
          label: _ctx.$t("me.Height") + "(cm)",
          modelValue: height.value
        }),
        f: common_vendor.o(($event) => weight.value = $event),
        g: common_vendor.p({
          columns: weights.value,
          label: _ctx.$t("me.Weight") + "(kg)",
          modelValue: weight.value
        }),
        h: common_vendor.o(($event) => time.value = $event),
        i: common_vendor.p({
          minDate: startDate.value,
          maxDate: endTime.value,
          type: "date",
          label: _ctx.$t("me.DateOfBirth"),
          modelValue: time.value
        }),
        j: common_vendor.p({
          border: true
        }),
        k: common_vendor.t(_ctx.$t("userinfo.history")),
        l: common_vendor.t(_ctx.$t("userinfo.hypertension")),
        m: common_vendor.t(_ctx.$t("common.yes")),
        n: common_vendor.p({
          value: 1
        }),
        o: common_vendor.t(_ctx.$t("common.no")),
        p: common_vendor.p({
          value: 0
        }),
        q: common_vendor.o(($event) => hypertension.value = $event),
        r: common_vendor.p({
          shape: "button",
          modelValue: hypertension.value
        }),
        s: common_vendor.t(_ctx.$t("userinfo.hyperglycemia")),
        t: common_vendor.t(_ctx.$t("common.yes")),
        v: common_vendor.p({
          value: 1
        }),
        w: common_vendor.t(_ctx.$t("common.no")),
        x: common_vendor.p({
          value: 0
        }),
        y: common_vendor.o(($event) => hyperglycemia.value = $event),
        z: common_vendor.p({
          shape: "button",
          modelValue: hyperglycemia.value
        }),
        A: common_vendor.t(_ctx.$t("userinfo.hyperlipidemia")),
        B: common_vendor.t(_ctx.$t("common.yes")),
        C: common_vendor.p({
          value: 1
        }),
        D: common_vendor.t(_ctx.$t("common.no")),
        E: common_vendor.p({
          value: 0
        }),
        F: common_vendor.o(($event) => hyperlipidemia.value = $event),
        G: common_vendor.p({
          shape: "button",
          modelValue: hyperlipidemia.value
        }),
        H: common_vendor.t(_ctx.$t("userinfo.hyperuricemia")),
        I: common_vendor.t(_ctx.$t("common.yes")),
        J: common_vendor.p({
          value: 1
        }),
        K: common_vendor.t(_ctx.$t("common.no")),
        L: common_vendor.p({
          value: 0
        }),
        M: common_vendor.o(($event) => hyperuricemia.value = $event),
        N: common_vendor.p({
          shape: "button",
          modelValue: hyperuricemia.value
        }),
        O: common_vendor.t(_ctx.$t("universal.OK")),
        P: common_vendor.o(handleSubmit),
        Q: common_vendor.p({
          type: "primary",
          size: "large",
          block: true
        }),
        R: common_vendor.sr(form, "864ed964-0", {
          "k": "form"
        }),
        S: common_vendor.p({
          model
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-864ed964"]]);
wx.createPage(MiniProgramPage);
