"use strict";
const common_vendor = require("../../common/vendor.js");
const lib_api_user = require("../../lib/api/user.js");
if (!Array) {
  const _easycom_wd_status_tip2 = common_vendor.resolveComponent("wd-status-tip");
  const _easycom_wd_cell2 = common_vendor.resolveComponent("wd-cell");
  const _easycom_wd_cell_group2 = common_vendor.resolveComponent("wd-cell-group");
  (_easycom_wd_status_tip2 + _easycom_wd_cell2 + _easycom_wd_cell_group2)();
}
const _easycom_wd_status_tip = () => "../../uni_modules/wot-design-uni/components/wd-status-tip/wd-status-tip.js";
const _easycom_wd_cell = () => "../../uni_modules/wot-design-uni/components/wd-cell/wd-cell.js";
const _easycom_wd_cell_group = () => "../../uni_modules/wot-design-uni/components/wd-cell-group/wd-cell-group.js";
if (!Math) {
  (_easycom_wd_status_tip + _easycom_wd_cell + _easycom_wd_cell_group)();
}
const _sfc_main = {
  __name: "device-info",
  setup(__props) {
    const { t } = common_vendor.useI18n();
    const changeTitle = () => {
      common_vendor.index.setNavigationBarTitle({
        title: t("me.DeviceInformation")
      });
    };
    const deviceInfo = common_vendor.ref(null);
    common_vendor.onLoad(async (query) => {
      changeTitle();
      const { data } = await lib_api_user.getDeviceInfoApi(query.imei);
      deviceInfo.value = data;
    });
    return (_ctx, _cache) => {
      var _a, _b, _c, _d, _e;
      return common_vendor.e({
        a: !deviceInfo.value
      }, !deviceInfo.value ? {
        b: common_vendor.p({
          image: "content",
          tip: _ctx.$t("device.noReport")
        })
      } : {
        c: common_vendor.p({
          title: _ctx.$t("deviceInfo.DeviceModel"),
          value: (_a = deviceInfo.value) == null ? void 0 : _a.devName
        }),
        d: common_vendor.p({
          title: "ICCID",
          value: (_b = deviceInfo.value) == null ? void 0 : _b.devIccid
        }),
        e: common_vendor.p({
          title: _ctx.$t("deviceInfo.DeviceSerialNumber"),
          value: (_c = deviceInfo.value) == null ? void 0 : _c.devSn
        }),
        f: common_vendor.p({
          title: _ctx.$t("deviceInfo.SoftwareVersion"),
          value: (_d = deviceInfo.value) == null ? void 0 : _d.devVersion
        }),
        g: common_vendor.p({
          title: _ctx.$t("deviceInfo.HardwareVersion"),
          value: (_e = deviceInfo.value) == null ? void 0 : _e.mcuVersion
        }),
        h: common_vendor.p({
          border: true
        })
      });
    };
  }
};
wx.createPage(_sfc_main);
