"use strict";
const lib_utils_request = require("../../lib/utils/request.js");
function getAlarmMsgApi() {
  return lib_utils_request.request("/MainFeatures/Alarm/getAlarmMsg", {}, { method: "GET", loading: true });
}
function sendMsgApi(data) {
  return lib_utils_request.request("/watch/send/down/msg", data, { method: "POST", loading: true });
}
exports.getAlarmMsgApi = getAlarmMsgApi;
exports.sendMsgApi = sendMsgApi;
