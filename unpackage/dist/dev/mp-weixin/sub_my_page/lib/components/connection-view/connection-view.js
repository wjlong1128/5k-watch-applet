"use strict";
const common_vendor = require("../../../../common/vendor.js");
const sub_my_page_lib_bluethooth = require("../../bluethooth.js");
if (!Array) {
  const _easycom_wd_button2 = common_vendor.resolveComponent("wd-button");
  _easycom_wd_button2();
}
const _easycom_wd_button = () => "../../../../uni_modules/wot-design-uni/components/wd-button/wd-button.js";
if (!Math) {
  _easycom_wd_button();
}
const _sfc_main = {
  __name: "connection-view",
  setup(__props) {
    const bluetoothStore = sub_my_page_lib_bluethooth.useBluetoothStore();
    common_vendor.storeToRefs(bluetoothStore);
    const { closeConnection } = bluetoothStore;
    const close = async () => {
      common_vendor.index.showLoading();
      try {
        await closeConnection();
        common_vendor.index.hideLoading();
      } catch (e) {
        common_vendor.index.hideLoading();
      }
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.o(close),
        b: common_vendor.p({
          type: "error"
        })
      };
    };
  }
};
wx.createComponent(_sfc_main);
