"use strict";
const common_vendor = require("../../common/vendor.js");
const useBluetoothStore = common_vendor.defineStore("bluetoothStore", () => {
  const isAuth = common_vendor.ref(true);
  const isEnableBluetooth = common_vendor.ref(false);
  const errTips = common_vendor.ref("bt.authTips");
  const deviceList = common_vendor.ref([]);
  const isDiscovering = common_vendor.ref(false);
  const isRegisterMethod = common_vendor.ref(false);
  const isActive = common_vendor.ref(false);
  const restartLoading = common_vendor.ref(false);
  const isConnection = common_vendor.ref(false);
  const currentDevice = common_vendor.ref(null);
  function registerDeviceFound() {
    common_vendor.index.onBluetoothDeviceFound((rd) => {
      rd.devices.forEach((d) => {
        if (d.name) {
          const filterArr = deviceList.value.filter((e) => e.deviceId == d.deviceId);
          if (!filterArr || filterArr.length <= 0) {
            deviceList.value.push(d);
          }
        }
      });
    });
  }
  const closeBluetooth = () => {
    common_vendor.index.stopBluetoothDevicesDiscovery({
      success() {
        deviceList.value = [];
      }
    });
    common_vendor.index.offBluetoothDeviceFound();
    common_vendor.index.offBluetoothAdapterStateChange();
    common_vendor.index.closeBluetoothAdapter();
    common_vendor.index.offBLECharacteristicValueChange();
    common_vendor.index.offBLEConnectionStateChange();
    common_vendor.index.offBLEMTUChange();
    isActive.value = false;
    deviceList.value = [];
    isRegisterMethod.value = false;
    if (isAuth.value) {
      errTips.value = "bt.enableBt";
    } else {
      errTips.value = "bt.authTips";
    }
    isAuth.value = true;
    isEnableBluetooth.value = false;
    isDiscovering.value = false;
    isConnection.value = false;
    currentDevice.value = null;
  };
  function listenerAdapterStateChange() {
    common_vendor.index.onBluetoothAdapterStateChange(({ available, discovering }) => {
      if (isActive.value) {
        isEnableBluetooth.value = available;
      }
      if (isActive.value && !available) {
        errTips.value = "bt.enableBt";
        closeBluetooth();
      }
    });
  }
  function loadFail(isShow, res, rejectFun) {
    console.log("error", res);
    isShow && common_vendor.index.hideLoading();
    if (res.errno === 103 || res.errMsg == "openBluetoothAdapter:fail auth deny" || res.errMsg == "openBluetoothAdapter:fail:auth denied") {
      errTips.value = "bt.authTips";
      isAuth.value = false;
      rejectFun();
      return;
    }
    isAuth.value = true;
    switch (res.errCode) {
      case 10001: {
        isEnableBluetooth.value = false;
        errTips.value = "bt.enableBt";
        break;
      }
    }
    rejectFun(res);
  }
  function loadSuccess(isShow, resolveFun) {
    isActive.value = true;
    isShow && common_vendor.index.hideLoading();
    isAuth.value = true;
    isEnableBluetooth.value = true;
    resolveFun();
  }
  const loadBluetooth = (isShow = false) => {
    return new Promise((resolve, reject) => {
      if (isActive.value) {
        resolve();
        return;
      }
      isShow && common_vendor.index.showLoading();
      common_vendor.index.openBluetoothAdapter({
        mode: "central",
        success() {
          if (!isRegisterMethod.value) {
            registerDeviceFound();
            listenerAdapterStateChange();
            isRegisterMethod.value = true;
          }
          loadSuccess(isShow, resolve);
        },
        fail(res) {
          loadFail(isShow, res, reject);
        }
      });
    });
  };
  const authBluetooth = () => {
    common_vendor.index.openSetting({
      success(authRes) {
        if (authRes.authSetting["scope.bluetooth"]) {
          isAuth.value = true;
          loadBluetooth().then(() => {
            startScan();
          });
        }
      }
    });
  };
  const startScan = () => {
    return new Promise((resolve, reject) => {
      common_vendor.index.startBluetoothDevicesDiscovery({
        allowDuplicatesKey: false,
        interval: 0,
        success(res) {
          console.log("开启扫描", res);
          isDiscovering.value = true;
          resolve(res);
        },
        fail(e) {
          console.log("开启扫描失败！", e);
          isDiscovering.value = false;
          reject(e);
        }
      });
    });
  };
  const stopScan = () => {
    return new Promise((resolve, reject) => {
      if (!isDiscovering.value) {
        resolve();
        return;
      }
      isDiscovering.value = false;
      common_vendor.index.stopBluetoothDevicesDiscovery({
        fail(e) {
          reject(e);
        },
        success(res) {
          resolve(res);
        }
      });
    });
  };
  const restartScan = () => {
    restartLoading.value = true;
    stopScan();
  };
  const getDeviceServices = (deviceId) => {
    return new Promise((resolve, reject) => {
      common_vendor.index.getBLEDeviceServices({
        deviceId,
        success(res) {
          console.log("getBLEDeviceServices ok", res);
          resolve(res);
        },
        fail(e) {
          console.log("getBLEDeviceServices error", e);
          reject(e);
        }
      });
    });
  };
  const getDeviceCharacteristics = (deviceId, serviceId) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        common_vendor.index.getBLEDeviceCharacteristics({
          deviceId,
          serviceId,
          success(res) {
            console.log("getDeviceCharacteristics, ok", deviceId, serviceId, res);
            resolve(res);
          },
          fail(e) {
            if (e.errCode == 10005) {
              resolve({ ...e, characteristics: [] });
              return;
            }
            console.log("getDeviceCharacteristics, error", deviceId, serviceId, e);
            reject(e);
          }
        });
      }, 500);
    });
  };
  const registerNotifyCharacteristicValueChange = (deviceId, serviceId, characteristic) => {
    return new Promise((resolve, reject) => {
      if (characteristic.properties.notify || characteristic.properties.indicate) {
        common_vendor.index.notifyBLECharacteristicValueChange({
          deviceId,
          serviceId,
          characteristicId: characteristic.uuid,
          state: true,
          success(r) {
            console.log("registerNotifyCharacteristicValueChange ok", r, deviceId, serviceId, characteristic);
            resolve({ result: r, characteristic });
          },
          fail(e) {
            console.log("registerNotifyCharacteristicValueChange error", e, deviceId, serviceId, characteristic);
            reject(e);
          }
        });
      } else {
        resolve({});
      }
    });
  };
  const getDeviceServicesAndCharacteristics = async (deviceId) => {
    try {
      const { services } = await getDeviceServices(deviceId);
      for (let i = 0; i < services.length; i++) {
        const s = services[i];
        const { characteristics } = await getDeviceCharacteristics(deviceId, s.uuid);
        for (let j = 0; j < characteristics.length; j++) {
          await registerNotifyCharacteristicValueChange(deviceId, s.uuid, characteristics[j]);
        }
        s.characteristics = characteristics;
      }
      common_vendor.index.onBLECharacteristicValueChange(({ deviceId: did, serviceId, characteristicId, value }) => {
        console.log("onBLECharacteristicValueChange", did, serviceId, characteristicId, value);
      });
      return services;
    } catch (e) {
      throw e;
    }
  };
  const connectSuccess = (connectionResult, device) => {
    return new Promise((resolve, reject) => {
      getDeviceServicesAndCharacteristics(device.deviceId).then((services) => {
        isConnection.value = true;
        currentDevice.value = device;
        common_vendor.index.onBLEConnectionStateChange(({ deviceId, connected }) => {
          var _a;
          console.log("onBLEConnectionStateChange", deviceId, connected);
          if (deviceId == ((_a = currentDevice.value) == null ? void 0 : _a.deviceId)) {
            isConnection.value = connected;
            if (isActive.value && !connected) {
              closeBluetooth();
            }
          }
        });
        resolve({ result: connectionResult, services });
      }).catch((e) => {
        common_vendor.index.closeBLEConnection({
          deviceId: device.deviceId,
          complete() {
            isConnection.value = false;
            currentDevice.value = null;
            reject(e);
          }
        });
      });
    });
  };
  const connect = (device) => {
    return new Promise((resolve, reject) => {
      common_vendor.index.createBLEConnection({
        deviceId: device.deviceId,
        success(res) {
          connectSuccess(res, device).then(resolve).catch(reject);
        },
        fail(res) {
          console.log("连接失败", device, res);
          if (res.errCode == -1) {
            connectSuccess(res, device).then(resolve).catch(reject);
          } else {
            isConnection.value = false;
            currentDevice.value = null;
            reject(res);
          }
        }
      });
    });
  };
  const closeConnection = () => {
    return new Promise((resolve, reject) => {
      var _a;
      common_vendor.index.closeBLEConnection({
        deviceId: (_a = currentDevice.value) == null ? void 0 : _a.deviceId,
        success(r) {
          console.log("关闭连接成功", r);
        },
        fail(e) {
          console.log("关闭连接失败", e);
        },
        complete() {
          closeBluetooth();
          resolve();
        }
      });
    });
  };
  return {
    isAuth,
    isEnableBluetooth,
    errTips,
    isActive,
    isDiscovering,
    deviceList,
    restartLoading,
    isConnection,
    currentDevice,
    loadBluetooth,
    authBluetooth,
    startScan,
    stopScan,
    restartScan,
    closeBluetooth,
    connect,
    closeConnection,
    getDeviceServicesAndCharacteristics
  };
});
exports.useBluetoothStore = useBluetoothStore;
