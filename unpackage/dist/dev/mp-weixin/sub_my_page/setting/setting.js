"use strict";
const common_vendor = require("../../common/vendor.js");
const uni_modules_wotDesignUni_components_wdMessageBox_index = require("../../uni_modules/wot-design-uni/components/wd-message-box/index.js");
require("../../uni_modules/wot-design-uni/locale/index.js");
const lib_api_user = require("../../lib/api/user.js");
const lib_utils_request = require("../../lib/utils/request.js");
const store_user = require("../../store/user.js");
if (!Array) {
  const _easycom_wd_cell2 = common_vendor.resolveComponent("wd-cell");
  const _easycom_wd_message_box2 = common_vendor.resolveComponent("wd-message-box");
  (_easycom_wd_cell2 + _easycom_wd_message_box2)();
}
const _easycom_wd_cell = () => "../../uni_modules/wot-design-uni/components/wd-cell/wd-cell.js";
const _easycom_wd_message_box = () => "../../uni_modules/wot-design-uni/components/wd-message-box/wd-message-box.js";
if (!Math) {
  (_easycom_wd_cell + _easycom_wd_message_box)();
}
const _sfc_main = {
  __name: "setting",
  setup(__props) {
    const { clearCurUser } = store_user.useUserStore();
    const { t } = common_vendor.useI18n();
    const message = uni_modules_wotDesignUni_components_wdMessageBox_index.useMessage();
    const changeTitle = () => {
      common_vendor.index.setNavigationBarTitle({
        title: t("me.Settings")
      });
    };
    const appVersion = common_vendor.ref("V1.5.8");
    common_vendor.onLoad(() => {
      const accountInfo = common_vendor.index.getAccountInfoSync();
      if (accountInfo.miniProgram.version) {
        appVersion.value = accountInfo.miniProgram.version;
      }
      changeTitle();
    });
    const logout = () => {
      message.confirm({
        title: t("me.checkLogOut"),
        confirmButtonText: t("universal.OK"),
        cancelButtonText: t("universal.Cancel")
      }).then(async () => {
        await lib_api_user.logOutApi();
        lib_utils_request.logoutAfter();
        clearCurUser();
        common_vendor.index.switchTab({
          url: "/pages/index/index"
        });
      }).catch(() => {
      });
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.p({
          title: _ctx.$t("app.version")
        }),
        b: common_vendor.t(_ctx.$t("me.logout")),
        c: common_vendor.o(logout)
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-2fc665e7"]]);
wx.createPage(MiniProgramPage);
