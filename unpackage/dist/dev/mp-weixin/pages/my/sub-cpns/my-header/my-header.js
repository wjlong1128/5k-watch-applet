"use strict";
const common_vendor = require("../../../../common/vendor.js");
const _sfc_main = {
  __name: "my-header",
  props: {
    username: String,
    avatar: String,
    nickname: String,
    isLogin: Boolean
  },
  emits: ["headerClick"],
  setup(__props) {
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: _ctx.$props.avatar,
        b: common_vendor.o(($event) => _ctx.$emit("headerClick")),
        c: !_ctx.$props.isLogin
      }, !_ctx.$props.isLogin ? {
        d: common_vendor.t(_ctx.$props.username),
        e: common_vendor.o(($event) => _ctx.$emit("headerClick"))
      } : {
        f: common_vendor.t(_ctx.$props.nickname),
        g: common_vendor.o(($event) => _ctx.$emit("headerClick")),
        h: common_vendor.t(_ctx.$props.username),
        i: common_vendor.o(($event) => _ctx.$emit("headerClick"))
      });
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-5e3cbd6b"]]);
wx.createComponent(Component);
