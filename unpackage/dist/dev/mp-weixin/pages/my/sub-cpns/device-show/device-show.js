"use strict";
const common_vendor = require("../../../../common/vendor.js");
if (!Array) {
  const _easycom_wd_button2 = common_vendor.resolveComponent("wd-button");
  _easycom_wd_button2();
}
const _easycom_wd_button = () => "../../../../uni_modules/wot-design-uni/components/wd-button/wd-button.js";
if (!Math) {
  (_easycom_wd_button + DeviceSignal + DeviceElectricity)();
}
const DeviceElectricity = () => "../../../../components/DeviceElectricity/DeviceElectricity.js";
const DeviceSignal = () => "../../../../components/DeviceSignal/DeviceSignal.js";
const _sfc_main = {
  __name: "device-show",
  props: {
    signal: Number,
    battery: Number
  },
  emits: ["refresh"],
  setup(__props) {
    const props = __props;
    common_vendor.computed(() => {
      return props.signal / 5 * 100;
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.o(($event) => _ctx.$emit("refresh")),
        b: common_vendor.p({
          type: "text",
          icon: "refresh"
        }),
        c: common_vendor.p({
          num: _ctx.$props.signal
        }),
        d: common_vendor.p({
          num: _ctx.$props.battery
        })
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-647c9ec8"]]);
wx.createComponent(Component);
