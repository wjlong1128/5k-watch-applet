"use strict";
const common_vendor = require("../../../../common/vendor.js");
const uni_modules_wotDesignUni_components_wdMessageBox_index = require("../../../../uni_modules/wot-design-uni/components/wd-message-box/index.js");
require("../../../../uni_modules/wot-design-uni/locale/index.js");
const store_user = require("../../../../store/user.js");
const lib_api_user = require("../../../../lib/api/user.js");
if (!Array) {
  const _easycom_wd_message_box2 = common_vendor.resolveComponent("wd-message-box");
  const _easycom_wd_icon2 = common_vendor.resolveComponent("wd-icon");
  const _easycom_wd_grid_item2 = common_vendor.resolveComponent("wd-grid-item");
  const _easycom_wd_grid2 = common_vendor.resolveComponent("wd-grid");
  (_easycom_wd_message_box2 + _easycom_wd_icon2 + _easycom_wd_grid_item2 + _easycom_wd_grid2)();
}
const _easycom_wd_message_box = () => "../../../../uni_modules/wot-design-uni/components/wd-message-box/wd-message-box.js";
const _easycom_wd_icon = () => "../../../../uni_modules/wot-design-uni/components/wd-icon/wd-icon.js";
const _easycom_wd_grid_item = () => "../../../../uni_modules/wot-design-uni/components/wd-grid-item/wd-grid-item.js";
const _easycom_wd_grid = () => "../../../../uni_modules/wot-design-uni/components/wd-grid/wd-grid.js";
if (!Math) {
  (_easycom_wd_message_box + _easycom_wd_icon + _easycom_wd_grid_item + _easycom_wd_grid)();
}
const _sfc_main = {
  __name: "options-grid",
  emits: ["showMsgBox", "hideMsgBox", "bind", "unbind"],
  setup(__props, { emit: __emit }) {
    const {
      t
    } = common_vendor.useI18n();
    const userStore = store_user.useUserStore();
    const {
      userInfo,
      imeiRef
    } = common_vendor.storeToRefs(userStore);
    const message = uni_modules_wotDesignUni_components_wdMessageBox_index.useMessage();
    common_vendor.onLoad(() => {
    });
    const emits = __emit;
    const vetifyUserLogin = () => {
      if (!userInfo.value) {
        common_vendor.index.showToast({
          icon: "none",
          title: t("PleaseLogInFirst")
        });
        return false;
      }
      return true;
    };
    const vetifyUserBind = () => {
      var _a;
      if (!vetifyUserLogin()) {
        return false;
      }
      if (!((_a = userInfo == null ? void 0 : userInfo.value) == null ? void 0 : _a.imei)) {
        common_vendor.index.showToast({
          icon: "none",
          title: t("data.UnboundDevice")
        });
        return false;
      }
      return true;
    };
    const gridList = common_vendor.ref([
      {
        icon: "list",
        text: "me.Profile",
        // 个人资料
        click() {
          common_vendor.index.navigateTo({
            url: "/sub_my_page/set-userinfo/set-userinfo?opt=2&title=me.EditProfile"
          });
        }
      },
      {
        icon: "setting",
        text: "me.Settings",
        // 设置
        click() {
          common_vendor.index.navigateTo({
            url: "/sub_my_page/setting/setting"
          });
        }
      }
      // {
      // 	icon: 'service',
      // 	text: 'my.feedback', // 客服反馈
      // 	click() {
      // 		uni.showToast({
      // 			icon: 'none',
      // 			title: '正在开发中',
      // 			duration: 2000
      // 		})
      // 	}
      // },
      // {
      // 	icon: 'star',
      // 	text: 'my.about5k', // 关于5k
      // 	click() {
      // 		uni.showToast({
      // 			icon: 'none',
      // 			title: '正在开发中',
      // 			duration: 2000
      // 		})
      // 	}
      // },
    ]);
    const deviceGridList = common_vendor.ref([
      {
        icon: "scan",
        // 当前不存在设备才能绑定设备
        text: "me.BindDevice",
        // 绑定设备
        click() {
          var _a;
          if (!vetifyUserLogin()) {
            return;
          }
          if ((_a = userInfo.value) == null ? void 0 : _a.imei) {
            common_vendor.index.showToast({
              icon: "none",
              title: t("me.PleaseUnBind")
            });
            return;
          }
          common_vendor.index.scanCode({
            // scanType: 'qrCode',
            async success({
              result
            }) {
              await lib_api_user.bindDeviceApi(result);
              await userStore.getUserInfo();
              emits("bind");
            }
          });
        }
      },
      {
        icon: "error-fill",
        // 当前存在设备才能接解绑
        text: "me.UnbindDevice",
        // 解绑
        click() {
          if (!vetifyUserBind()) {
            return;
          }
          emits("showMsgBox");
          message.confirm({
            zIndex: 9999999,
            title: t("universal.DoYouWantToUnbind"),
            confirmButtonText: t("universal.OK"),
            cancelButtonText: t("universal.Cancel")
          }).then(async () => {
            await lib_api_user.unBindApi();
            await userStore.getUserInfo();
            emits("unbind");
          }).catch(() => {
          }).finally(() => {
            emits("hideMsgBox");
          });
        }
      },
      {
        icon: "info-circle",
        text: "me.deviceInformation",
        // 设备信息
        click() {
          var _a;
          if (!vetifyUserBind()) {
            return;
          }
          common_vendor.index.navigateTo({
            url: `/sub_my_page/device-info/device-info?imei=${(_a = userInfo == null ? void 0 : userInfo.value) == null ? void 0 : _a.imei}`
          });
        }
      },
      {
        icon: "chat",
        text: "my.alarm",
        // 告警信息
        click() {
          if (!vetifyUserBind()) {
            return;
          }
          common_vendor.index.navigateTo({
            url: "/sub_my_page/alarm-msg/alarm-msg"
          });
        }
      },
      {
        icon: "setting1",
        text: "my.watchSettings",
        // 手表设置
        click() {
          if (!vetifyUserBind()) {
            return;
          }
          common_vendor.index.showToast({
            icon: "none",
            title: t("repost.dev"),
            duration: 2e3
          });
        }
      },
      {
        icon: "location",
        text: "my.deviceLocation",
        // 设备位置
        async click() {
          if (!vetifyUserBind()) {
            return;
          }
          const { data } = await lib_api_user.getDeviceLoactionApi();
          if (data && data.longitude && data.latitude) {
            common_vendor.index.openLocation({
              longitude: parseFloat(data.longitude),
              latitude: parseFloat(data.latitude),
              scale: 14,
              name: t("localtion.updTime"),
              address: data.positioningTime
            });
          } else {
            common_vendor.index.showToast({
              icon: "none",
              title: t("localtion.noLocation"),
              duration: 2e3
            });
          }
        }
      },
      {
        icon: "send",
        iconPrefix: "my-icon",
        text: "my.sendMsg",
        // 发送消息
        async click() {
          if (!vetifyUserBind()) {
            return;
          }
          common_vendor.index.navigateTo({
            url: "/sub_my_page/sendMsg/sendMsg"
          });
        }
      }
      // {
      // 	icon: '蓝牙',
      // 	iconPrefix: 'my-icon',
      // 	text: 'my.sendMsg', // 发送消息
      // 	async click() {
      // 		// if(!vetifyUserBind()){
      // 		// 	return;
      // 		// }
      // 		uni.navigateTo({
      // 			url:'/sub_my_page/bluetooth-connect/bluetooth-connect'
      // 		})
      // 	}
      // },
    ]);
    return (_ctx, _cache) => {
      return {
        a: common_vendor.t(_ctx.$t("my.title3")),
        b: common_vendor.f(deviceGridList.value, (option, index, i0) => {
          return common_vendor.e({
            a: option.iconPrefix
          }, option.iconPrefix ? {
            b: "8d085ee1-3-" + i0 + "," + ("8d085ee1-2-" + i0),
            c: common_vendor.p({
              ["class-prefix"]: option.iconPrefix,
              name: option.icon,
              size: "26px"
            })
          } : {
            d: "8d085ee1-4-" + i0 + "," + ("8d085ee1-2-" + i0),
            e: common_vendor.p({
              name: option.icon,
              size: "26px"
            })
          }, {
            f: common_vendor.t(_ctx.$t(option.text)),
            g: index,
            h: common_vendor.o(($event) => option.click(index), index),
            i: "8d085ee1-2-" + i0 + ",8d085ee1-1",
            j: common_vendor.p({
              text: _ctx.$t(option.text),
              ["use-text-slot"]: true,
              ["use-icon-slot"]: true
            })
          });
        }),
        c: common_vendor.p({
          column: 4,
          clickable: true
        }),
        d: common_vendor.t(_ctx.$t("my.title2")),
        e: common_vendor.f(gridList.value, (option, index, i0) => {
          return {
            a: common_vendor.t(_ctx.$t(option.text)),
            b: index,
            c: common_vendor.o(($event) => option.click(index, _ctx.i), index),
            d: "8d085ee1-6-" + i0 + ",8d085ee1-5",
            e: common_vendor.p({
              icon: option.icon,
              text: _ctx.$t(option.text),
              ["use-text-slot"]: true
            })
          };
        }),
        f: common_vendor.p({
          column: 4,
          clickable: true
        }),
        g: common_vendor.t(_ctx.$t("my.title1"))
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-8d085ee1"]]);
wx.createComponent(Component);
