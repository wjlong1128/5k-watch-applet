"use strict";
const common_vendor = require("../../common/vendor.js");
const store_user = require("../../store/user.js");
const common_assets = require("../../common/assets.js");
const lib_constants = require("../../lib/constants.js");
const lib_api_user = require("../../lib/api/user.js");
if (!Array) {
  const _easycom_wd_badge2 = common_vendor.resolveComponent("wd-badge");
  _easycom_wd_badge2();
}
const _easycom_wd_badge = () => "../../uni_modules/wot-design-uni/components/wd-badge/wd-badge.js";
if (!Math) {
  (MyHeader + _easycom_wd_badge + DeviceShow + OptionsGrid + ChangeLang + CustomTabbar)();
}
const CustomTabbar = () => "../../components/custom-tabbar/custom-tabbar.js";
const ChangeLang = () => "../../components/change-lang/change-lang.js";
const MyHeader = () => "./sub-cpns/my-header/my-header.js";
const DeviceShow = () => "./sub-cpns/device-show/device-show.js";
const OptionsGrid = () => "./sub-cpns/options-grid/options-grid.js";
const __default__ = {
  options: {
    // 微信小程序中 options 选项  
    options: {
      styleIsolation: "shared"
    }
  }
};
const _sfc_main = /* @__PURE__ */ Object.assign(__default__, {
  __name: "my",
  setup(__props) {
    const {
      t
    } = common_vendor.useI18n();
    const userStore = store_user.useUserStore();
    const {
      userInfo,
      deviceInfo
    } = common_vendor.storeToRefs(userStore);
    const changeTitle = () => {
      common_vendor.index.setNavigationBarTitle({
        title: t("page.my")
      });
    };
    const showDeviceShow = common_vendor.ref(true);
    const reqDeviceMsg = async () => {
      var _a;
      if ((_a = userInfo == null ? void 0 : userInfo.value) == null ? void 0 : _a.imei) {
        const {
          data
        } = await lib_api_user.getDevicePowerApi(userInfo.value.imei);
        userStore.setDeviceInfo(data);
      }
    };
    const unbindHandler = () => {
      userStore.clearDeviceInfo();
      common_vendor.index.showToast({
        title: t("me.UnbindDeviceSuccess"),
        duration: 2e3
      });
    };
    const bindHandler = async () => {
      await reqDeviceMsg();
      common_vendor.index.showToast({
        title: t("universal.BindingSuccessful"),
        duration: 2e3
      });
    };
    common_vendor.onLoad(async (options) => {
      var _a;
      changeTitle();
      if (options.msg) {
        userStore.clearCurUser();
        common_vendor.index.showToast({
          icon: "none",
          title: options.msg,
          duration: 3e3
        });
      } else {
        if (common_vendor.index.getStorageSync(lib_constants.storeKeys.token)) {
          await userStore.getUserInfo();
          if ((_a = userInfo.value) == null ? void 0 : _a.imei) {
            await reqDeviceMsg();
          }
        }
      }
    });
    common_vendor.onShow(async () => {
      var _a;
      if ((_a = userInfo.value) == null ? void 0 : _a.imei) {
        await reqDeviceMsg();
      }
    });
    const username = common_vendor.computed(() => {
      var _a;
      return ((_a = userInfo == null ? void 0 : userInfo.value) == null ? void 0 : _a.mobile) || t("my.login");
    });
    const avatar = common_vendor.computed(() => {
      var _a;
      return ((_a = userInfo == null ? void 0 : userInfo.value) == null ? void 0 : _a.avatarUrl) || common_assets.defaultAvatar;
    });
    const nickname = common_vendor.computed(() => {
      var _a;
      return ((_a = userInfo == null ? void 0 : userInfo.value) == null ? void 0 : _a.nickname) || t("wechat.user");
    });
    const isLogin = common_vendor.computed(() => {
      return !!userInfo.value;
    });
    const clickHeaderHandler = () => {
      let url;
      if (userInfo == null ? void 0 : userInfo.value) {
        url = "/sub_my_page/set-userinfo/set-userinfo?opt=2&title=me.EditProfile";
      } else {
        url = "/pages/login/login";
      }
      common_vendor.index.navigateTo({
        url
      });
    };
    return (_ctx, _cache) => {
      var _a, _b, _c;
      return common_vendor.e({
        a: common_vendor.o(clickHeaderHandler),
        b: common_vendor.p({
          isLogin: isLogin.value,
          username: username.value,
          avatar: avatar.value,
          nickname: nickname.value
        }),
        c: (_a = common_vendor.unref(userInfo)) == null ? void 0 : _a.imei
      }, ((_b = common_vendor.unref(userInfo)) == null ? void 0 : _b.imei) ? {
        d: common_vendor.t((_c = common_vendor.unref(userInfo)) == null ? void 0 : _c.imei),
        e: common_vendor.p({
          ["bg-color"]: common_vendor.unref(deviceInfo).deviceStatus == 0 ? "green" : "red",
          ["is-dot"]: true
        })
      } : {
        f: common_vendor.t(_ctx.$t("me.UnbounDevice"))
      }, {
        g: common_vendor.o(reqDeviceMsg),
        h: common_vendor.p({
          signal: common_vendor.unref(deviceInfo).signal,
          battery: common_vendor.unref(deviceInfo).battery
        }),
        i: common_vendor.o(bindHandler),
        j: common_vendor.o(unbindHandler),
        k: common_vendor.o(($event) => showDeviceShow.value = false),
        l: common_vendor.o(($event) => showDeviceShow.value = true),
        m: common_vendor.o(changeTitle),
        n: common_vendor.p({
          currentPage: 2
        })
      });
    };
  }
});
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-2f1ef635"]]);
wx.createPage(MiniProgramPage);
