"use strict";
const common_vendor = require("../../common/vendor.js");
const lib_constants = require("../../lib/constants.js");
const store_user = require("../../store/user.js");
if (!Math) {
  (PrivacyAuthoriza + Dashboard + ItemList + CustomTabbar)();
}
const CustomTabbar = () => "../../components/custom-tabbar/custom-tabbar.js";
const PrivacyAuthoriza = () => "../../components/privacy-authoriza/privacy-authoriza.js";
const Dashboard = () => "./sub-cpns/Dashboard/Dashboard.js";
const ItemList = () => "./sub-cpns/ItemList/ItemList.js";
const _sfc_main = {
  __name: "index",
  setup(__props) {
    const {
      t
    } = common_vendor.useI18n();
    const userStore = store_user.useUserStore();
    const needPrivacy = common_vendor.ref(false);
    common_vendor.ref(0);
    common_vendor.ref(0);
    const changeTitle = () => {
      common_vendor.index.setNavigationBarTitle({
        title: t("page.index")
      });
    };
    common_vendor.onShow(() => {
      changeTitle();
    });
    common_vendor.onLoad(async () => {
      changeTitle();
      if (common_vendor.index.getStorageSync(lib_constants.storeKeys.token)) {
        needPrivacy.value = false;
        await userStore.getUserInfo();
      } else if (common_vendor.index.getStorageSync(lib_constants.storeKeys.privateAgreement)) {
        needPrivacy.value = false;
      } else {
        needPrivacy.value = true;
      }
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: needPrivacy.value
      }, needPrivacy.value ? {} : {}, {
        b: common_vendor.p({
          currentPage: 0
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-1cf27b2a"]]);
wx.createPage(MiniProgramPage);
