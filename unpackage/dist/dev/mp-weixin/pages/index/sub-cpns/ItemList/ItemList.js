"use strict";
const common_vendor = require("../../../../common/vendor.js");
const pages_index_subCpns_ItemList_indexDict = require("./index-dict.js");
const lib_constants = require("../../../../lib/constants.js");
const _sfc_main = {
  __name: "ItemList",
  setup(__props) {
    const {
      t
    } = common_vendor.useI18n();
    const itemList = common_vendor.ref(pages_index_subCpns_ItemList_indexDict.itemDicts);
    common_vendor.onMounted(() => {
    });
    const handleClick = (item) => {
      if (!common_vendor.index.getStorageSync(lib_constants.storeKeys.token)) {
        common_vendor.index.navigateTo({
          url: "/pages/login/login",
          success() {
            common_vendor.index.showToast({
              icon: "none",
              duration: 1500,
              title: t("PleaseLogInFirst")
            });
          }
        });
        return;
      }
      if (!common_vendor.index.getStorageSync(lib_constants.storeKeys.imei)) {
        common_vendor.index.showToast({
          icon: "error",
          duration: 1500,
          title: t("me.UnbounDevice")
        });
        return;
      }
      item.click();
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.f(itemList.value, (item, k0, i0) => {
          return {
            a: common_vendor.t(_ctx.$t(item.text)),
            b: `url(${item.bgImg})`,
            c: common_vendor.o(($event) => handleClick(item), item.text),
            d: common_vendor.n(`item ${item.class}`),
            e: item.text
          };
        })
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-cdc19e1b"]]);
wx.createComponent(Component);
