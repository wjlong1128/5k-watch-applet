"use strict";
const common_vendor = require("../../../../common/vendor.js");
const lib_utils_request = require("../../../../lib/utils/request.js");
const bgUrl = lib_utils_request.BASE_URL + "/images/getImage?imageName=";
const url = "../../../../static/images/";
const pathPrefix = "/sub_index_page/";
const itemDicts = [
  {
    class: "heartRate",
    text: "data.HeartRate",
    // url: "/pages/index/heartRate/heartRate",
    // img: url  + '心率.jpg'
    img: url + "心率.png",
    bgImg: bgUrl + "心率.jpg",
    click() {
      common_vendor.index.navigateTo({
        url: pathPrefix + this.class + "/" + this.class
      });
    }
  },
  {
    class: "bloodPressure",
    text: "data.BloodPressure",
    // url: "/pages/index/bloodPressure/bloodPressure",
    // img: url  + '血压.jpg'
    img: url + "血压.png",
    bgImg: bgUrl + "血压.jpg",
    click() {
      common_vendor.index.navigateTo({
        url: pathPrefix + this.class + "/" + this.class
      });
    }
  },
  {
    class: "temperature",
    text: "data.BodyTemperature",
    // url: "/pages/index/temperature/temperature",
    // img: url  + '体温.jpg"'
    img: url + "体温.png",
    bgImg: bgUrl + "体温.jpg",
    click() {
      common_vendor.index.navigateTo({
        url: pathPrefix + this.class + "/" + this.class
      });
    }
  },
  {
    class: "bloodOxygen",
    text: "data.BloodOxygen",
    // url: "/pages/index/bloodOxygen/bloodOxygen",
    // img: url  + '血氧.jpg'
    img: url + "血氧.png",
    bgImg: bgUrl + "血氧.jpg",
    click() {
      common_vendor.index.navigateTo({
        url: pathPrefix + this.class + "/" + this.class
      });
    }
  },
  {
    class: "step",
    text: "data.NumberOfSteps",
    // url: "/pages/index/Step/Step",
    // img: url  + '运动步数.jpg'
    img: url + "步数.png",
    bgImg: bgUrl + "运动步数.jpg",
    click() {
      common_vendor.index.navigateTo({
        url: pathPrefix + this.class + "/" + this.class
      });
    }
  },
  {
    class: "bloodSugar",
    text: "data.BloodSugar",
    // url: "/pages/index/bloodSugar/bloodSugar",
    // img: url  + '血糖.png'
    img: url + "血糖.png",
    bgImg: bgUrl + "血糖.png",
    click() {
      common_vendor.index.navigateTo({
        url: pathPrefix + this.class + "/" + this.class
      });
    }
  }
];
exports.itemDicts = itemDicts;
