"use strict";
const common_vendor = require("../../../../common/vendor.js");
const common_assets = require("../../../../common/assets.js");
const store_user = require("../../../../store/user.js");
const lib_api_user = require("../../../../lib/api/user.js");
const _sfc_main = {
  __name: "Dashboard",
  setup(__props) {
    const userStore = store_user.useUserStore();
    const { userInfo } = common_vendor.storeToRefs(userStore);
    const { t } = common_vendor.useI18n();
    const bindHandler = async () => {
      var _a, _b;
      if ((_a = userInfo == null ? void 0 : userInfo.value) == null ? void 0 : _a.imei) {
        return;
      }
      if (!userInfo.value) {
        common_vendor.index.navigateTo({
          url: "/pages/login/login",
          complete() {
            common_vendor.index.showToast({
              icon: "none",
              title: t("PleaseLogInFirst"),
              duration: 2e3
            });
          }
        });
        return;
      }
      if (userInfo.value && !((_b = userInfo == null ? void 0 : userInfo.value) == null ? void 0 : _b.imei)) {
        common_vendor.index.scanCode({
          // scanType: 'qrCode',
          async success({ result }) {
            await lib_api_user.bindDeviceApi(result);
            await userStore.getUserInfo();
            common_vendor.index.showToast({
              mask: true,
              icon: "success",
              title: t("universal.BindingSuccessful"),
              duration: 2e3
            });
          }
        });
      }
    };
    return (_ctx, _cache) => {
      var _a, _b, _c;
      return {
        a: common_assets._imports_0,
        b: common_vendor.t(((_a = common_vendor.unref(userInfo)) == null ? void 0 : _a.imei) ? _ctx.$t("data.OkBing") : _ctx.$t("data.GoToBing")),
        c: common_vendor.o(bindHandler),
        d: common_vendor.t(((_b = common_vendor.unref(userInfo)) == null ? void 0 : _b.imei) ? _ctx.$t("index.dashboard.titleOnBind") : _ctx.$t("index.dashboard.title")),
        e: common_vendor.t(((_c = common_vendor.unref(userInfo)) == null ? void 0 : _c.imei) ? _ctx.$t("index.dashboard.descOnBind") : _ctx.$t("index.dashboard.desc"))
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-5123ffbe"]]);
wx.createComponent(Component);
