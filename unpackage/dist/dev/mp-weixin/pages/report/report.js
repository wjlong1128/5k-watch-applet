"use strict";
const common_vendor = require("../../common/vendor.js");
if (!Array) {
  const _easycom_wd_tab2 = common_vendor.resolveComponent("wd-tab");
  const _easycom_wd_tabs2 = common_vendor.resolveComponent("wd-tabs");
  (_easycom_wd_tab2 + _easycom_wd_tabs2)();
}
const _easycom_wd_tab = () => "../../uni_modules/wot-design-uni/components/wd-tab/wd-tab.js";
const _easycom_wd_tabs = () => "../../uni_modules/wot-design-uni/components/wd-tabs/wd-tabs.js";
if (!Math) {
  (_easycom_wd_tab + _easycom_wd_tabs + CustomTabbar)();
}
const CustomTabbar = () => "../../components/custom-tabbar/custom-tabbar.js";
const _sfc_main = {
  __name: "report",
  setup(__props) {
    const {
      t
    } = common_vendor.useI18n();
    const tab = common_vendor.ref("BloodPressureReport");
    const changeTitle = () => {
      common_vendor.index.setNavigationBarTitle({
        title: t("page.report")
      });
    };
    common_vendor.ref(0);
    common_vendor.ref(0);
    common_vendor.onLoad(() => {
      changeTitle();
    });
    common_vendor.onShow(() => changeTitle());
    return (_ctx, _cache) => {
      return {
        a: common_vendor.f(["BloodPressureReport", "BloodSugarReport"], (item, index, i0) => {
          return {
            a: "12a8021c-1-" + i0 + ",12a8021c-0",
            b: common_vendor.p({
              title: _ctx.$t(item)
            }),
            c: index
          };
        }),
        b: common_vendor.t(_ctx.$t("repost.dev")),
        c: common_vendor.o(($event) => tab.value = $event),
        d: common_vendor.p({
          modelValue: tab.value
        }),
        e: common_vendor.p({
          currentPage: 1
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-12a8021c"]]);
wx.createPage(MiniProgramPage);
