"use strict";
const common_vendor = require("../../common/vendor.js");
const lib_api_user = require("../../lib/api/user.js");
const lib_constants = require("../../lib/constants.js");
const store_user = require("../../store/user.js");
if (!Array) {
  const _easycom_wd_icon2 = common_vendor.resolveComponent("wd-icon");
  const _easycom_wd_button2 = common_vendor.resolveComponent("wd-button");
  const _easycom_wd_checkbox2 = common_vendor.resolveComponent("wd-checkbox");
  (_easycom_wd_icon2 + _easycom_wd_button2 + _easycom_wd_checkbox2)();
}
const _easycom_wd_icon = () => "../../uni_modules/wot-design-uni/components/wd-icon/wd-icon.js";
const _easycom_wd_button = () => "../../uni_modules/wot-design-uni/components/wd-button/wd-button.js";
const _easycom_wd_checkbox = () => "../../uni_modules/wot-design-uni/components/wd-checkbox/wd-checkbox.js";
if (!Math) {
  (_easycom_wd_icon + _easycom_wd_button + _easycom_wd_checkbox)();
}
const _sfc_main = {
  __name: "login",
  setup(__props) {
    const userStore = store_user.useUserStore();
    const {
      t
    } = common_vendor.useI18n();
    const height = common_vendor.ref(0);
    const maginTop = common_vendor.ref(0);
    const changeTitle = () => {
      common_vendor.index.setNavigationBarTitle({
        title: t("page.login")
      });
    };
    const flag = common_vendor.ref(false);
    common_vendor.onLoad(() => {
      changeTitle();
      let selectAgreement = common_vendor.index.getStorageSync(lib_constants.storeKeys.selectAgreement);
      if (!selectAgreement) {
        selectAgreement = lib_constants.selectAgreementStatus[1];
      }
      flag.value = selectAgreement == lib_constants.selectAgreementStatus[0];
      const barInfo = common_vendor.index.createSelectorQuery().select(".bar");
      barInfo.boundingClientRect().exec(([res]) => {
        const rect = common_vendor.index.getMenuButtonBoundingClientRect();
        maginTop.value = rect.top;
        height.value = rect.height;
      });
    });
    common_vendor.watch(flag, (newFlag) => {
      const selectAgreementIndex = newFlag ? 0 : 1;
      common_vendor.index.setStorageSync(lib_constants.storeKeys.selectAgreement, lib_constants.selectAgreementStatus[selectAgreementIndex]);
    });
    const cancel = () => {
      common_vendor.index.switchTab({
        url: "/pages/index/index"
      });
    };
    const loginHandler = (phoneCode) => {
      const lang = common_vendor.index.getStorageSync(lib_constants.storeKeys.langKey).startsWith("zh") ? "zh-CN" : "en";
      common_vendor.index.getUserInfo({
        lang,
        desc: t("login.info.desc"),
        success: (wechatInfo) => {
          common_vendor.index.login({
            provider: "weixin",
            success: async ({
              code
            }) => {
              const {
                data
              } = await lib_api_user.loginApi({
                phoneCode,
                loginCode: code
              });
              common_vendor.index.setStorageSync(lib_constants.storeKeys.token, data.token);
              common_vendor.index.setStorageSync(lib_constants.storeKeys.openId, data.openId);
              userStore.setWechatInfo(wechatInfo.userInfo);
              await userStore.getUserInfo();
              if (data.isFirstLogin) {
                common_vendor.index.navigateTo({
                  url: "/sub_my_page/set-userinfo/set-userinfo?opt=1&title=me.CompleteProfile"
                });
              } else {
                common_vendor.index.switchTab({
                  url: `/pages/my/my`
                });
              }
            }
          });
        }
      });
    };
    const getphonenumber = (e) => {
      console.log("flag", flag.value);
      if (!flag.value) {
        common_vendor.index.vibrateLong();
        common_vendor.index.showToast({
          icon: "none",
          title: t("login.check"),
          duration: 1500
        });
        return;
      }
      if (e.encryptedData) {
        loginHandler(e == null ? void 0 : e.code);
        return;
      }
      common_vendor.index.showToast({
        title: t("Canceled"),
        icon: "none",
        duration: 4e3
      });
    };
    const back = () => {
      common_vendor.index.navigateBack();
    };
    const toHome = () => {
      common_vendor.index.switchTab({
        url: "/pages/index/index"
      });
    };
    const toPrivate = () => {
      common_vendor.index.openPrivacyContract({
        success: () => {
        },
        fail: () => {
          common_vendor.index.showToast({
            title: "遇到错误",
            icon: "error"
          });
        }
      });
    };
    const toUser = () => {
      common_vendor.index.navigateTo({
        url: "/sub_my_page/userAgreement/userAgreement"
      });
    };
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.o(back),
        b: common_vendor.p({
          color: "#fff",
          name: "arrow-left",
          size: "22px"
        }),
        c: common_vendor.o(toHome),
        d: common_vendor.p({
          color: "#fff",
          name: "home1",
          size: "22px"
        }),
        e: `${maginTop.value}px`,
        f: `${height.value}px`,
        g: common_vendor.t(_ctx.$t("login.back")),
        h: common_vendor.t(_ctx.$t("login.backTips")),
        i: flag.value
      }, flag.value ? {
        j: common_vendor.t(_ctx.$t("login.login")),
        k: common_vendor.o(getphonenumber),
        l: common_vendor.p({
          size: "large",
          ["open-type"]: "getPhoneNumber",
          width: "100%",
          block: true
        })
      } : {
        m: common_vendor.t(_ctx.$t("login.login")),
        n: common_vendor.o(getphonenumber),
        o: common_vendor.p({
          size: "large",
          width: "100%",
          block: true
        })
      }, {
        p: common_vendor.t(_ctx.$t("login.cancel")),
        q: common_vendor.o(cancel),
        r: common_vendor.p({
          size: "large",
          type: "info",
          block: true
        }),
        s: common_vendor.o(($event) => flag.value = $event),
        t: common_vendor.p({
          inline: true,
          modelValue: flag.value
        }),
        v: common_vendor.t(_ctx.$t("login.check-tips1")),
        w: common_vendor.o(($event) => flag.value = !flag.value),
        x: common_vendor.t(_ctx.$t("login.check-p1")),
        y: common_vendor.o(toPrivate),
        z: common_vendor.t(_ctx.$t("login.and")),
        A: common_vendor.t(_ctx.$t("login.check-p2")),
        B: common_vendor.o(toUser)
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-e4e4508d"]]);
wx.createPage(MiniProgramPage);
