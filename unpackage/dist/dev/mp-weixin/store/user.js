"use strict";
const common_vendor = require("../common/vendor.js");
const lib_api_user = require("../lib/api/user.js");
const lib_constants = require("../lib/constants.js");
const useUserStore = common_vendor.defineStore("user", () => {
  const { t } = common_vendor.useI18n();
  const userInfo = common_vendor.ref(null);
  const wechatInfo = common_vendor.ref(null);
  const imeiRef = common_vendor.ref(null);
  const deviceInfo = common_vendor.ref({
    signal: 0,
    battery: 0,
    deviceStatus: 1
  });
  const setDeviceInfo = (deviceData) => {
    deviceInfo.value.signal = parseInt((deviceData == null ? void 0 : deviceData.signal) || 0);
    deviceInfo.value.battery = parseInt((deviceData == null ? void 0 : deviceData.battery) || 0);
    deviceInfo.value.deviceStatus = parseInt((deviceData == null ? void 0 : deviceData.status) || 1);
  };
  const refreshUserInfo = () => {
    if (!common_vendor.index.getStorageSync(lib_constants.storeKeys.token)) {
      userInfo.value = null;
      wechatInfo.value = null;
    }
    return {
      userInfo: userInfo.value,
      wechatInfo: wechatInfo.value
    };
  };
  const getImei = async () => {
    const imei = common_vendor.index.getStorageSync(lib_constants.storeKeys.imei);
    if (imei) {
      return imei;
    }
    return null;
  };
  const setWechatInfo = (v) => {
    wechatInfo.value = v;
    common_vendor.index.setStorageSync(lib_constants.storeKeys.wechatInfo, JSON.stringify(v));
  };
  const getUserInfo = async () => {
    const token = common_vendor.index.getStorageSync(lib_constants.storeKeys.token);
    if (token) {
      const {
        data
      } = await lib_api_user.getUserInfoApi();
      userInfo.value = data;
      wechatInfo.value = common_vendor.index.getStorageSync(lib_constants.storeKeys.wechatInfo);
      const imei = data.imei;
      imeiRef.value = imei;
      if (imei) {
        common_vendor.index.setStorageSync(lib_constants.storeKeys.imei, imei);
      } else {
        common_vendor.index.removeStorageSync(lib_constants.storeKeys.imei);
      }
    }
  };
  const getDeviceStatus = async () => {
    const imei = common_vendor.index.getStorageSync(lib_constants.storeKeys.imei);
    if (!imei) {
      common_vendor.index.showToast({
        title: t("data.UnboundDevice"),
        icon: "fail"
      });
      return;
    }
    const res = await lib_api_user.getDevicePowerApi(imei);
    if (!res.data) {
      common_vendor.index.showToast({
        title: t("me.deviceInfoNull"),
        icon: "none",
        duration: 4e3
      });
      return;
    }
  };
  const logout = async () => {
    await lib_api_user.logOutApi();
    userInfo.value = null;
    wechatInfo.value = null;
    clearDeviceInfo();
    common_vendor.index.removeStorageSync(lib_constants.storeKeys.langKey);
    common_vendor.index.removeStorageSync(lib_constants.storeKeys.wechatInfo);
    common_vendor.index.removeStorageSync(lib_constants.storeKeys.token);
    common_vendor.index.removeStorageSync(lib_constants.storeKeys.openId);
    common_vendor.index.removeStorageSync(lib_constants.storeKeys.imei);
    return new Promise((resolve, reject) => {
      common_vendor.index.reLaunch({
        url: "/pages/index/index",
        success: () => {
          resolve();
        },
        fail() {
          reject("跳转登录失败");
        }
      });
    });
  };
  const clearDeviceInfo = () => {
    deviceInfo.value = {
      signal: 0,
      battery: 0,
      deviceStatus: 1
    };
  };
  const clearCurUser = () => {
    clearDeviceInfo();
    const token = common_vendor.index.getStorageSync(lib_constants.storeKeys.token);
    if (!token) {
      userInfo.value = null;
      wechatInfo.value = null;
    }
  };
  return {
    imeiRef,
    userInfo,
    deviceInfo,
    refreshUserInfo,
    getUserInfo,
    setWechatInfo,
    setDeviceInfo,
    getDeviceStatus,
    getImei,
    logout,
    clearCurUser,
    clearDeviceInfo
  };
});
exports.useUserStore = useUserStore;
