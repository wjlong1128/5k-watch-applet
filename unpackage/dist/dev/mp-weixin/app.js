"use strict";
Object.defineProperty(exports, Symbol.toStringTag, { value: "Module" });
const common_vendor = require("./common/vendor.js");
const lib_utils_route = require("./lib/utils/route.js");
const lib_constants = require("./lib/constants.js");
const locale_index = require("./locale/index.js");
if (!Math) {
  "./pages/index/index.js";
  "./pages/report/report.js";
  "./pages/my/my.js";
  "./pages/login/login.js";
  "./sub_my_page/set-userinfo/set-userinfo.js";
  "./sub_my_page/device-info/device-info.js";
  "./sub_my_page/setting/setting.js";
  "./sub_my_page/userAgreement/userAgreement.js";
  "./sub_my_page/alarm-msg/alarm-msg.js";
  "./sub_my_page/sendMsg/sendMsg.js";
  "./sub_my_page/bluetooth-connect/bluetooth-connect.js";
  "./sub_index_page/bloodPressure/bloodPressure.js";
  "./sub_index_page/temperature/temperature.js";
  "./sub_index_page/bloodOxygen/bloodOxygen.js";
  "./sub_index_page/step/step.js";
  "./sub_index_page/bloodSugar/bloodSugar.js";
  "./sub_index_page/heartRate/heartRate.js";
  "./sub_index_page/calibrateBloodSugar/calibrateBloodSugar.js";
}
const _sfc_main = {
  onLaunch: async function() {
    let lang = common_vendor.index.getStorageSync(lib_constants.storeKeys.langKey);
    if (!lang) {
      lang = common_vendor.index.getLocale();
    }
    this.$i18n.locale = lang;
    common_vendor.index.setLocale(lang);
    common_vendor.index.setStorageSync(lib_constants.storeKeys.langKey, lang);
    await lib_utils_route.route();
  },
  onShow: function() {
  },
  onHide: function() {
  }
};
function createApp() {
  const i18n = common_vendor.createI18n({
    locale: common_vendor.index.getLocale(),
    messages: locale_index.locale
  });
  const app = common_vendor.createSSRApp(_sfc_main);
  app.use(i18n);
  app.use(common_vendor.createPinia());
  return {
    app,
    Pinia: common_vendor.Pinia
  };
}
createApp().app.mount("#app");
exports.createApp = createApp;
