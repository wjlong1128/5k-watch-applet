"use strict";
const loading$1 = "loading";
const day$1 = "day";
const week$1 = "week";
const month$1 = "month";
const year$1 = "year";
const currentData$1 = "current data";
const reportCount$1 = "Number of reports";
const MaxP$1 = "highest blood pressure";
const MinP$1 = "lowest blood pressure";
const AvgP$1 = "Average blood pressure";
const MaxHr$1 = "Max HR";
const MinHr$1 = "Min HR";
const AvgHr$1 = "Avg HR";
const MaxHP$1 = "Max HP";
const MinLP$1 = "Min LP";
const HP$1 = "HP";
const LP$1 = "LP";
const AvgMaxHP$1 = "Avg MaxHP";
const AvgMinLP$1 = "Avg MinLP";
const MaxTemp$1 = "Max Temp";
const MinTemp$1 = "Min Temp";
const AvgTemp$1 = "Avg Temp";
const Temp$1 = "Temp";
const MaxSpO2$1 = "Max SpO2";
const MinSpO2$1 = "Min SpO2";
const AvgSpO2$1 = "Avg SpO2";
const SpO2$1 = "SpO2";
const StepTotalDist$1 = "Total Dist";
const StepTotalSteps$1 = "Total Steps";
const StepTotalCal$1 = "Total Cal";
const Km$1 = "km";
const kcal$1 = "kcal";
const Step$1 = "Step";
const MaxGlu$1 = "Max Glu";
const MinGlu$1 = "Min Glu";
const AvgGlu$1 = "Avg Glu";
const Glu$1 = "Glu";
const GluCalibration$1 = "Glu Calibration";
const AfterMealTime$1 = "After eating";
const PleaseEnterAfterMealTime$1 = "Enter post-meal time.";
const Minutes$1 = "minutes";
const BloodSugarValue$1 = "Blood sugar value";
const PleaseEnterBloodSugarValue$1 = "Please enter a valid blood sugar value.";
const DiabetesStatus$1 = "Are you diabetic:";
const PleaseClick$1 = "Please choose";
const Yes$1 = "yes";
const No$1 = "no";
const CalibratingTitle$1 = "Calibrating";
const CalibratingMsgHint$1 = "*Calibrating data can help us generate new data more accurately, ensuring that the presented information is more precise and reliable.";
const AddTips$1 = "You have added 0 items, and you can add up to 3 items.";
const PleaseChooseDiabetes$1 = "Please choose diabetes.";
const CurrentlyInDevelopment$1 = "Currently in development...";
const BloodPressureReport$1 = "Bp Report";
const BloodSugarReport$1 = "Glu Report";
const PleaseBindWatchfirst$1 = "unbound";
const Canceled$1 = "Canceled";
const PleaseLogInFirst$1 = "please log in first";
const UserPrivacyProtectionGuidelines$1 = "User Privacy Protection Guidelines";
const PrivacyTextOne$1 = "Thank you for choosing to use the 5k Health Management Mini Program. We attach great importance to your personal information security and privacy protection. Before using our product, please read it carefully“";
const PrivacyTextTow$1 = "If you agree to this privacy protection guide, please click the agree button to start using this mini program. We will do our best to protect your personal information and legal rights. Thank you for your trust!";
const TotalCount$1 = "Total Count";
const Count$1 = "count";
const en = {
  "login.check": "Please check the user agreement",
  "login.check-tips1": "I have read and agreed to ",
  "login.check-tips2": "The phone number that is not registered with 5k Health Management will be automatically registered as a 5k Health Management user account upon successful login.",
  "login.check-p1": "《Privacy Policy》",
  "login.check-p2": "《User Agreement》",
  "login.and": "and",
  "wechat.user": "WeChat User",
  "page.index": "Index",
  "page.report": "Data",
  "page.my": "My",
  "page.heilth": "Heilth",
  "page.login": "Login page",
  "login.cancel": "Cancel",
  "privact.authoriza.loading": "Retrieving user privacy policy information...",
  "index.dashboard.title": "Bind Smart Health Watch",
  "index.dashboard.titleOnBind": "Real-time Health Monitoring",
  "index.dashboard.desc": "Understand your health condition and master device information",
  "index.dashboard.descOnBind": "You can immediately view and understand your health status",
  "login.login": "Quick Login",
  "login.info.desc": "Get your nickname, avatar, region, and gender",
  "login.success": "login success!",
  "login.back": "Welcome back",
  "login.backTips": "Welcome to 5K Health Management",
  "userinfo.tips": "Please complete personal information!",
  "userinfo.hypertension": "High blood pressure",
  "userinfo.hyperglycemia": "High blood sugar",
  "userinfo.hyperlipidemia": "High blood lipids",
  "userinfo.hyperuricemia": "High uric acid",
  "userinfo.basicMsg": "basic information",
  "userinfo.history": "medical history",
  "alarm.noalter": "Currently no alerts",
  "common.yes": "yes",
  "common.no": "no",
  "const.dad": "Dad",
  "const.test": "test",
  "const.mom": "Mom",
  "const.olderBrother": "Older Brother",
  "const.youngerBrother": "Younger Brother",
  "const.olderSister": "Older Sister",
  "const.youngerSister": "Younger Sister",
  "const.custom": "Custom",
  "localtion.updTime": "Location update time",
  "localtion.type": "Location type",
  "localtion.noLocation": "Device is not located",
  "device.noReport": "Device not reported",
  "repost.dev": "Under active development...",
  "app.version": "Wechat App Version",
  "my.title1": "My Device",
  "my.title3": "Device Management",
  "my.title2": "Common Tools",
  "my.login": "Quick Login",
  "my.alarm": "Alarm Message",
  "my.about5k": "About 5k",
  "my.feedback": "Feedback",
  "my.watchSettings": "Watch Settings",
  "my.deviceLocation": "Device Location",
  "my.sendMsg": "Send Message",
  "locale.auto": "System",
  "locale.en": "English",
  "locale.zh-hans": "简体中文",
  "index.scene": "Scene",
  "index.company": "Company Profile",
  "sendMsg.sender": "Sender",
  "sendMsg.sendSuccess": "Message sent successfully",
  "sendMsg.sendFail": "Failed to send message",
  "sendMsg.senderNotEmpty": "Sender cannot be empty",
  "sendMsg.messageNotEmpty": "Message cannot be empty",
  "sendMsg.pleaseSelect": "Please select",
  "sendMsg.pleaseInput": "Please input",
  "sendMsg.pleaseInputMsg": "Please enter the message to send",
  "me.contact": "contact us",
  "me.user.name": "WeChat User",
  "me.logout": "Log out",
  "me.checkLogOut": "Do you want to Log out?",
  "me.LocateTab": "Quick Login",
  "me.name": "My",
  "me.UnbindDevice": "Unbind Device",
  "me.BindDevice": "Bind Device",
  "me.UnbounDevice": "Unbound Device",
  "me.CompleteProfile": "Complete Personal Profile",
  "me.PleaseUnBind": "Please unbind the current device",
  "me.EditProfile": "Edit Profile",
  "me.Profile": "My Profile",
  "me.DeviceInformation": "Device Information",
  "me.Settings": "Settings",
  "me.Gender": "Gender",
  "me.Height": "Height",
  "me.Weight": "Weight",
  "me.DateOfBirth": "Date of Birth",
  "me.deviceInformation": "Device Info",
  "me.deviceInfoNull": "Device information not obtained, please try again",
  "me.signal": "signal",
  "me.battery": "battery",
  "data.empty": "No data yet",
  "data.desc": "The above assessment and forecast results are for reference only and cannot be used as medical judgment standards. For further screening, please consult a local doctor.",
  "Gender.Man": "Man",
  "Gender.Woman": "Woman",
  "data.UnboundDevice": "Unbound Device",
  "me.UnbindDeviceSuccess": "Unbound Device Success!",
  "data.GoToBing": "Go Bind",
  "data.OkBing": "Bound",
  "data.HeartRate": "Heart Rate",
  "data.BloodPressure": "Blood Pressure",
  "data.BodyTemperature": "Body Temperature",
  "data.BloodOxygen": "Blood Oxygen",
  "data.NumberOfSteps": "Number of Steps",
  "data.BloodSugar": "Blood Sugar",
  "data.LowBattery": "Low battery",
  "data.Sos": " SOS for help",
  "data.show": "Data presentation",
  "alarm.detected": "Detected ",
  "data.showMonth": "Monthly Data",
  "data.showDay": "Daily Data",
  "data.showWeek": "Weekly Data",
  "data.showYear": "Yearly Data",
  "data.count.highest": "highest",
  "data.count.lowest": "lowest",
  "data.count.average": "average",
  "data.count.frequency": "frequency",
  "data.count.accumulated_count": "accumulated_count",
  "data.count.accumulated_duration": "accumulated_duration",
  "data.countTotalDistance": "Total Distance",
  "data.countTotalSteps": "Total Steps",
  "data.countTotalDuration": "Total Duration",
  "data.countTotalCalories": "Calories",
  "data.add": "add to",
  "data.name": "Data",
  "data.delete": "delete",
  "report.name": "Report",
  loading: loading$1,
  "universal.refuse": "refuse",
  "universal.agree": "agree",
  "universal.OK": "OK",
  "universal.Cancel": "Cancel",
  "universal.PleaseConfigure": "Please Set",
  "universal.SystemPrompt": "System Prompt",
  "universal.Submission": "Submission successful.",
  "universal.submit": "Submit",
  "universal.DoYouWantToUnbind": "Do you want to unbind",
  "universal.BindingSuccessful": "Binding Successful",
  "universal.SystemError": "System Error",
  "deviceInfo.DeviceModel": "Device Model",
  "deviceInfo.DeviceSerialNumber": "Device Serial Number",
  "deviceInfo.SoftwareVersion": "4G Version",
  "deviceInfo.HardwareVersion": "Software Version",
  day: day$1,
  week: week$1,
  month: month$1,
  year: year$1,
  currentData: currentData$1,
  reportCount: reportCount$1,
  MaxP: MaxP$1,
  MinP: MinP$1,
  AvgP: AvgP$1,
  MaxHr: MaxHr$1,
  MinHr: MinHr$1,
  AvgHr: AvgHr$1,
  MaxHP: MaxHP$1,
  MinLP: MinLP$1,
  HP: HP$1,
  LP: LP$1,
  AvgMaxHP: AvgMaxHP$1,
  AvgMinLP: AvgMinLP$1,
  MaxTemp: MaxTemp$1,
  MinTemp: MinTemp$1,
  AvgTemp: AvgTemp$1,
  Temp: Temp$1,
  MaxSpO2: MaxSpO2$1,
  MinSpO2: MinSpO2$1,
  AvgSpO2: AvgSpO2$1,
  SpO2: SpO2$1,
  StepTotalDist: StepTotalDist$1,
  StepTotalSteps: StepTotalSteps$1,
  StepTotalCal: StepTotalCal$1,
  Km: Km$1,
  kcal: kcal$1,
  Step: Step$1,
  "Step.msg": "step",
  "Step.compet": "completion rate",
  "Step.today": "today's step count",
  MaxGlu: MaxGlu$1,
  MinGlu: MinGlu$1,
  AvgGlu: AvgGlu$1,
  Glu: Glu$1,
  GluCalibration: GluCalibration$1,
  AfterMealTime: AfterMealTime$1,
  PleaseEnterAfterMealTime: PleaseEnterAfterMealTime$1,
  Minutes: Minutes$1,
  BloodSugarValue: BloodSugarValue$1,
  PleaseEnterBloodSugarValue: PleaseEnterBloodSugarValue$1,
  DiabetesStatus: DiabetesStatus$1,
  PleaseClick: PleaseClick$1,
  Yes: Yes$1,
  No: No$1,
  CalibratingTitle: CalibratingTitle$1,
  CalibratingMsgHint: CalibratingMsgHint$1,
  AddTips: AddTips$1,
  PleaseChooseDiabetes: PleaseChooseDiabetes$1,
  CurrentlyInDevelopment: CurrentlyInDevelopment$1,
  BloodPressureReport: BloodPressureReport$1,
  BloodSugarReport: BloodSugarReport$1,
  PleaseBindWatchfirst: PleaseBindWatchfirst$1,
  Canceled: Canceled$1,
  PleaseLogInFirst: PleaseLogInFirst$1,
  UserPrivacyProtectionGuidelines: UserPrivacyProtectionGuidelines$1,
  PrivacyTextOne: PrivacyTextOne$1,
  PrivacyTextTow: PrivacyTextTow$1,
  TotalCount: TotalCount$1,
  Count: Count$1,
  "local.ChangeLanguage": "语言/Language",
  "bt.authTips": "Please grant Bluetooth permission",
  "bt.auth": "Go to authorize",
  "bt.enableBt": "Please turn on your phone's Bluetooth and then click refresh",
  "bt.refresh": "Refresh"
};
const loading = "加载中";
const day = "日";
const week = "周";
const month = "月";
const year = "年";
const currentData = "当前数据";
const reportCount = "上报次数";
const MaxHr = "最高心率";
const MinHr = "最低心率";
const AvgHr = "平均心率";
const MaxHP = "最高高压";
const MinLP = "最高低压";
const HP = "高压";
const LP = "低压";
const MaxP = "最高血压";
const MinP = "最低血压";
const AvgP = "平均血压";
const AvgMaxHP = "平均高压";
const AvgMinLP = "平均低压";
const MaxTemp = "最高体温";
const MinTemp = "最低体温";
const AvgTemp = "平均体温";
const Temp = "体温";
const MaxSpO2 = "最高血氧";
const MinSpO2 = "最低血氧";
const AvgSpO2 = "平均血氧";
const SpO2 = "血氧";
const StepTotalDist = "总路程";
const StepTotalSteps = "总步数";
const StepTotalCal = "消耗总卡路里";
const Km = "公里";
const kcal = "千卡";
const Step = "步数";
const MaxGlu = "最高血糖";
const MinGlu = "最低血糖";
const AvgGlu = "平均血糖";
const Glu = "血糖";
const GluCalibration = "血糖校准";
const AfterMealTime = "饭后时间";
const PleaseEnterAfterMealTime = "请输入饭后时间.";
const Minutes = "分钟";
const BloodSugarValue = "测量血糖值";
const PleaseEnterBloodSugarValue = "请输入正确的血糖值.";
const DiabetesStatus = "是否糖尿病:";
const PleaseClick = "请点击选择";
const Yes = "是";
const No = "否";
const CalibratingTitle = "血糖校准";
const CalibratingMsgHint = "校准数据可以帮助我们更精准地生成新的数据，确保所呈现的信息更加准确和可靠.";
const AddTips = "您已经添加0条，最多可以添加3条";
const PleaseChooseDiabetes = "请选择是否有糖尿病";
const CurrentlyInDevelopment = "正在努力开发中...";
const BloodPressureReport = "血压报告";
const BloodSugarReport = "血糖报告";
const PleaseBindWatchfirst = "请先绑定手表";
const Canceled = "已取消授权";
const PleaseLogInFirst = "请先登录";
const UserPrivacyProtectionGuidelines = "用户隐私保护指引";
const PrivacyTextOne = "感谢您选择使用5k健康管理小程序，我们非常重视您的个人信息安全和隐私保护。使用我们的产品前，请您仔细阅读";
const PrivacyTextTow = "如您同意此隐私保护指引,请点击同意按钮,开始使用此小程序,我们将尽全力保护您的个人信息及合法权益，感谢您的信任！";
const TotalCount = "累计次数";
const Count = "次";
const zhHans = {
  "wechat.user": "微信用户",
  "login.check": "请勾选用户协议",
  "login.check-tips1": "我已阅读并同意",
  "login.check-tips2": "未注册5k健康管理账号的手机号码，登录成功将自动注册5k健康管理用户账号",
  "login.check-p1": "《隐私协议》",
  "login.check-p2": "《用户协议》",
  "login.and": "和",
  "page.index": "首页",
  "page.heilth": "健康",
  "page.report": "报告",
  "page.my": "我的",
  "page.login": "登录页",
  "privact.authoriza.loading": "正在获取用户隐私协议信息...",
  "index.dashboard.title": "绑定智能健康手表",
  "index.dashboard.titleOnBind": "健康数据实时监测",
  "index.dashboard.desc": "了解健康状况，掌握设备信息",
  "index.dashboard.descOnBind": "您可以立即查看并了解自己的健康状态",
  "login.login": "一键登录",
  "login.cancel": "取消登录",
  "login.success": "登录成功!",
  "login.back": "欢迎使用5k健康管理",
  "login.backTips": "登录查看您的健康数据",
  "login.info.desc": "获取你的昵称、头像、地区及性别",
  "userinfo.tips": "请完善个人信息!",
  "userinfo.hypertension": "高血压",
  "userinfo.hyperglycemia": "高血糖",
  "userinfo.hyperlipidemia": "高血脂",
  "userinfo.hyperuricemia": "高尿酸",
  "userinfo.basicMsg": "基础信息",
  "userinfo.history": "既往病史",
  "alarm.noalter": "暂无告警信息",
  "common.yes": "有",
  "common.no": "无",
  "const.dad": "爸爸",
  "const.test": "测试",
  "const.mom": "妈妈",
  "const.olderBrother": "哥哥",
  "const.youngerBrother": "弟弟",
  "const.olderSister": "姐姐",
  "const.youngerSister": "妹妹",
  "const.custom": "自定义",
  "my.title1": "我的设备",
  "my.title3": "设备管理",
  "my.title2": "常用工具",
  "my.login": "点击登录",
  "my.alarm": "告警信息",
  "my.about5k": "关于5k",
  "my.feedback": "意见反馈",
  "my.watchSettings": "手表设置",
  "my.deviceLocation": "设备位置",
  "my.sendMsg": "发送消息",
  "locale.auto": "系统",
  "locale.en": "English",
  "locale.zh-hans": "简体中文",
  "index.company": "公司介绍",
  "sendMsg.sender": "发送人",
  "sendMsg.sendSuccess": "发送成功",
  "sendMsg.sendFail": "发送失败",
  "sendMsg.senderNotEmpty": "发送人不能为空",
  "sendMsg.messageNotEmpty": "消息不能为空",
  "sendMsg.pleaseSelect": "请选择",
  "sendMsg.pleaseInput": "请输入",
  "sendMsg.pleaseInputMsg": "请填写要发送的消息",
  "localtion.updTime": "位置更新时间",
  "localtion.type": "定位类型",
  "localtion.noLocation": "设备未定位",
  "device.noReport": "设备未上报",
  "app.version": "版本号",
  "repost.dev": "正在积极开发中...",
  "me.user.name": "微信用户",
  "me.contact": "联系我们",
  "me.logout": "退出登录",
  "me.checkLogOut": "是否退出登入?",
  "me.name": "我的",
  "me.UnbindDevice": "解绑设备",
  "me.UnbindDeviceSuccess": "解绑成功",
  "me.BindDevice": "绑定设备",
  "me.PleaseUnBind": "请解绑当前设备",
  "me.UnbounDevice": "未绑定设备",
  "me.Profile": "个人资料",
  "me.EditProfile": "编辑个人资料",
  "me.CompleteProfile": "完善个人资料",
  "me.DeviceInformation": "设备信息",
  "me.Settings": "设置",
  "me.LocateTab": "手机号快捷登录",
  "me.Gender": "性别",
  "me.Height": "身高",
  "me.Weight": "体重",
  "me.DateOfBirth": "出生日期",
  "me.deviceInformation": "设备信息",
  "me.deviceInfoNull": "未获取到设备信息,请稍后重试",
  "me.signal": "信号",
  "me.battery": "电量",
  "Gender.Man": "男",
  "Gender.Woman": "女",
  loading,
  "data.UnboundDevice": "暂未绑定设备",
  "data.GoToBing": "去绑定",
  "data.OkBing": "已绑定",
  "data.HeartRate": "心率",
  "data.BloodPressure": "血压",
  "data.BodyTemperature": "体温",
  "data.BloodOxygen": "血氧",
  "data.NumberOfSteps": "步数",
  "data.BloodSugar": "血糖",
  "data.LowBattery": "低电量",
  "data.Sos": "SOS求救",
  "data.show": "数据展示",
  "alarm.detected": "监测到",
  "data.showMonth": "本月数据",
  "data.showDay": "本日数据",
  "data.showWeek": "本周数据",
  "data.showYear": "本年度数据",
  "data.count.highest": "最高",
  "data.count.lowest": "最低",
  "data.count.average": "平均",
  "data.count.frequency": "次",
  "data.count.accumulated_count": "累积次数",
  "data.count.accumulated_duration": "累计时长",
  "data.countTotalDistance": "总路程",
  "data.countTotalSteps": "总步数",
  "data.countTotalDuration": "累计时长",
  "data.countTotalCalories": "卡路里",
  "data.add": "添加",
  "data.name": "数据",
  "report.name": "报告",
  "data.delete": "删除",
  "universal.refuse": "拒绝",
  "data.empty": "暂无数据",
  "data.desc": "免责声明:以上评估及预测结果仅供客户参考，无法作为医疗级评判标准，如需进一步筛查请咨询当地医生",
  "universal.agree": "同意",
  "universal.OK": "确定",
  "universal.Cancel": "取消",
  "universal.PleaseConfigure": "请设置",
  "universal.SystemPrompt": "系统提示",
  "universal.Submission": "提交成功",
  "universal.submit": "提交",
  "universal.DoYouWantToUnbind": "是否解除绑定?",
  "universal.BindingSuccessful": "绑定成功",
  "universal.SystemError": "系统错误",
  "deviceInfo.DeviceModel": "设备型号",
  "deviceInfo.DeviceSerialNumber": "设备序列号",
  "deviceInfo.SoftwareVersion": "4G版本",
  "deviceInfo.HardwareVersion": "软件版本",
  day,
  week,
  month,
  year,
  currentData,
  reportCount,
  MaxHr,
  MinHr,
  AvgHr,
  MaxHP,
  MinLP,
  HP,
  LP,
  MaxP,
  MinP,
  AvgP,
  AvgMaxHP,
  AvgMinLP,
  MaxTemp,
  MinTemp,
  AvgTemp,
  Temp,
  MaxSpO2,
  MinSpO2,
  AvgSpO2,
  SpO2,
  StepTotalDist,
  StepTotalSteps,
  StepTotalCal,
  Km,
  kcal,
  Step,
  "Step.msg": "步",
  "Step.today": "今日步数",
  "Step.compet": "完成度",
  MaxGlu,
  MinGlu,
  AvgGlu,
  Glu,
  GluCalibration,
  AfterMealTime,
  PleaseEnterAfterMealTime,
  Minutes,
  BloodSugarValue,
  PleaseEnterBloodSugarValue,
  DiabetesStatus,
  PleaseClick,
  Yes,
  No,
  CalibratingTitle,
  CalibratingMsgHint,
  AddTips,
  PleaseChooseDiabetes,
  CurrentlyInDevelopment,
  BloodPressureReport,
  BloodSugarReport,
  PleaseBindWatchfirst,
  Canceled,
  PleaseLogInFirst,
  UserPrivacyProtectionGuidelines,
  PrivacyTextOne,
  PrivacyTextTow,
  TotalCount,
  Count,
  "local.ChangeLanguage": "语言/Language",
  "bt.authTips": "请授权蓝牙权限",
  "bt.auth": "去授权",
  "bt.enableBt": "请开启手机蓝牙后点击刷新",
  "bt.refresh": "刷新"
};
const locale = {
  en,
  "zh-Hans": zhHans
};
exports.locale = locale;
